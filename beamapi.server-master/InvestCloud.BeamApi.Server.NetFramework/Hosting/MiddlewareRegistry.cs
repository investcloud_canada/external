using System.Collections.Generic;
using System.Web.Configuration;
using InvestCloud.BeamApi.Server.NetFramework.Middleware;
using InvestCloud.BeamApi.Server.Shared;

namespace InvestCloud.BeamApi.Server.NetFramework.Hosting
{
    public class MiddlewareRegistry : IMiddlewareRegistry
    {
        private readonly IBeamApiOptions _options;
        private readonly IList<IBeamApiMiddleware> _middlewareList = new List<IBeamApiMiddleware>();

        public MiddlewareRegistry(IBeamApiOptions options)
        {
            _options = options;
        }
            
        public void Register<T>(T middleware) where T : IBeamApiMiddleware
        {
            _middlewareList.Add(middleware);
        }

        public void Initialise()
        {
            var compilationSection = (CompilationSection)System.Configuration.ConfigurationManager.GetSection(@"system.web/compilation");
            _options.Debug = compilationSection.Debug;
        }

        public IBeamApiContext GetNewContext()
        {
            return new BeamApiContext(_middlewareList);
        }
    }
}