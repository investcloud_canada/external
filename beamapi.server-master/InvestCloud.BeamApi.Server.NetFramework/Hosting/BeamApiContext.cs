using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using InvestCloud.BeamApi.Server.NetFramework.DependencyInjection;
using InvestCloud.BeamApi.Server.NetFramework.Middleware;
using InvestCloud.BeamApi.Server.Shared.Exception;

namespace InvestCloud.BeamApi.Server.NetFramework.Hosting
{
    public class BeamApiContext : IBeamApiContext
    {
        private readonly IList<IBeamApiMiddleware> _middlewareList;
        private int _count = 0;

        public BeamApiContext(IList<IBeamApiMiddleware> middlewareList)
        {
            _middlewareList = middlewareList;
        }

        public async Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider)
        {
            if (_count >= _middlewareList.Count)
                throw new BeamApiException($"Middleware {_middlewareList[_count-1].GetType().Name} called Invoke(...), but there are no further middleware registered to invoke");
            await _middlewareList[_count++].Invoke(next, context, dependencyProvider);
        }
    }
}