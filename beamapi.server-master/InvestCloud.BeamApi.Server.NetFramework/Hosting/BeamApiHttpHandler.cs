using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using InvestCloud.BeamApi.Server.NetFramework.DependencyInjection;

namespace InvestCloud.BeamApi.Server.NetFramework.Hosting
{
    public class BeamApiHttpHandler : HttpTaskAsyncHandler
    {
        private readonly IServiceProvider _dependencyProvider;

        public BeamApiHttpHandler()
        {
            _dependencyProvider = ServiceProviderInitialiser.GetDependencyProvider();
        }

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            if (_dependencyProvider == null)
            {
                LogAndOutputError(context,
                    HttpStatusCode.InternalServerError,
                    $"Could not instantiate an {nameof(IServiceProvider)} implementation; please implement this interface and register in the config file (see documentation)");
                return;
            }

            var beamApiContext = ((IMiddlewareRegistry)_dependencyProvider.GetService(typeof(IMiddlewareRegistry))).GetNewContext();
            try
            {
                await beamApiContext.Invoke(beamApiContext, context, _dependencyProvider);
            }
            catch (Exception e)
            {
                // TODO - not yet true; need to handle in BeamApiContext
                LogAndOutputError(context,
                    HttpStatusCode.InternalServerError,
                    "Unhandled exception in BeamApiHttpHandler; all exceptions should be handled at this point");
            }
        }

        public override bool IsReusable => true;

        private void LogAndOutputError(HttpContext context,
            HttpStatusCode statusCode,
            string message,
            Exception e = null)
        {
            context.Response.StatusCode = (int) statusCode;
            context.Response.Output.Write(message);
            
        }
    }
}