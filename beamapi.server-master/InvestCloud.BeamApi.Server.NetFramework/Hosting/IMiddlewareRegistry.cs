using InvestCloud.BeamApi.Server.NetFramework.Middleware;

namespace InvestCloud.BeamApi.Server.NetFramework.Hosting
{
    public interface IMiddlewareRegistry
    {
        void Register<T>(T middleware) where T : IBeamApiMiddleware;
        
        IBeamApiContext GetNewContext();
        void Initialise();
    }
}