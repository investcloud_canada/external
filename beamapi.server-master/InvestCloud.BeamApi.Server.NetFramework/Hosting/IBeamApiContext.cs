using System;
using System.Threading.Tasks;
using System.Web;

namespace InvestCloud.BeamApi.Server.NetFramework.Hosting
{
    public interface IBeamApiContext
    {
        Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider);
    }
}