using System;
using System.Configuration;
using System.Reflection;
using InvestCloud.BeamApi.Server.Shared.Exception;

namespace InvestCloud.BeamApi.Server.NetFramework.DependencyInjection
{
    public static class ServiceProviderInitialiser

    {
        private const string ConfigKey = "BeamApiDependencyProviderImplementation";
        private static bool _isInitialised;
        private static IServiceProvider _dependencyProvider;

        public static IServiceProvider GetDependencyProvider()
        {
            if (!_isInitialised)
                Initialise();
            return _dependencyProvider;
        }

        #region IDependencyProvider initialisation
        private static void Initialise()
        {
            // attempt 1: check the config file
            _dependencyProvider = InitialiseDependencyProviderFromConfigFile();
            // attempt 2: try to find an implementation through reflection
            if (_dependencyProvider == null)
                _dependencyProvider = InitialiseDependencyProviderUsingReflection();
            // try to initialise until DependencyProvider is found
            if (_dependencyProvider != null)
                _isInitialised = true;
        }     
        

        private static IServiceProvider InitialiseDependencyProviderUsingReflection()
        {
            var interfaceType = typeof(IServiceProvider);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                // skip obvious third party libraries
                if (assembly.FullName.StartsWith("System") || assembly.FullName.StartsWith("Microsoft"))
                    continue;
                try
                {
                    foreach (var type in assembly.GetTypes())
                    {
                        if (!type.IsInterface && interfaceType.IsAssignableFrom(type))
                            return Instantiate(type);
                    }
                }
                catch (ReflectionTypeLoadException)
                {
                    // couldn't load one of the types in the assembly; continue
                    continue;
                }
            }

            return null;
        }

        private static IServiceProvider InitialiseDependencyProviderFromConfigFile()
        {
            var configValue = ConfigurationManager.AppSettings[ConfigKey];
            if (!string.IsNullOrWhiteSpace(configValue))
            {
                Type t = null;
                try
                {
                    t = Type.GetType(configValue);
                }
                catch (Exception e)
                {
                    LogErrorAndThrow(
                        $"Couldn't find or load type of {nameof(IServiceProvider)} implementation '{configValue}'",
                        e);
                }

                if (!typeof(IServiceProvider).IsAssignableFrom(t))
                {
                    LogErrorAndThrow(
                        $"Type {t.FullName} specified in config key {ConfigKey} doesn't implement {nameof(IServiceProvider)}");
                }

                return Instantiate(t);
            }

            return null;
        }

        private static IServiceProvider Instantiate(Type t)
        {
            try
            {
                return (IServiceProvider) Activator.CreateInstance(t);
            }
            catch (Exception e)
            {
                LogErrorAndThrow($"Couldn't instantiate {nameof(IServiceProvider)} implementation '{t.FullName}'", e);
                return null;
            }
        }
        #endregion
        
        private static void LogErrorAndThrow(string message, Exception e = null)
        {
            throw new BeamApiException(message, e);
        }
    }
}