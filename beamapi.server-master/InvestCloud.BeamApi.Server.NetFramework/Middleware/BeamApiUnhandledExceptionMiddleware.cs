﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;
using InvestCloud.BeamApi.Server.Shared;
using InvestCloud.BeamApi.Server.Shared.Serialization;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    /// <summary>
    /// Simple handler for unhandled exceptions. This does the following:
    /// <ul>
    /// <li>catches any exception thrown by 'child' middleware</li>
    /// <li>builds up a short error message containing information about the call context (if available)</li>
    /// <li>logs the error (using <see cref="ILogger{TCategoryName}"/> with a key of <see cref="Constants.BeamApiLoggerName"/>)</li>
    /// <li>attempts to return a BeamApi response with this message</li>
    /// <li>if that fails, return the message as plain text (with a <see cref="HttpStatusCode.InternalServerError"/>).</li>
    /// </ul>
    /// </summary>
    public class BeamApiUnhandledExceptionMiddleware : BaseBeamApiMiddleware
    {
        public BeamApiUnhandledExceptionMiddleware(ILoggerFactory loggerFactory) : base(loggerFactory)
        {
        }

        public override async Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider)
        {
            try
            {
                await next.Invoke(next, context, dependencyProvider);
            }
            catch (Exception e)
            {
                var messageBuilder = new StringBuilder("Unhandled exception");
                // check whether the response exists in the context
                if (context.Items.Contains(Constants.BeamApiHttpContextRequestKey))
                {
                    var req = context.Items[Constants.BeamApiHttpContextRequestKey];
                    var request = (call2Request) req;
                    messageBuilder.Append( $" calling {request.arg0.control.app}-{request.arg0.control.function}-{request.arg0.control.verb}");
                }

                messageBuilder.Append(": ").Append(e.Message).Append(" See log for full detail.");
                var message = messageBuilder.ToString();
                Logger.LogError(message, e);
                try
                {
                    context.Response.ContentType = "text/xml;charset=utf-8";
                    // attempt to return a Beam API response
                    var response = new call2Response
                    {
                        @return = new messageEnvelope
                        {
                            Status = (ushort) MessageStatus.ApplicationError,
                            MainMessage = message
                        }
                    };
                    // not injected via DI to avoid errors being unreported if the DI is not set up correctly!
                    var serialization = new RequestResponseSerialization();
                    serialization.SerializeResponse(response, context.Response.OutputStream);
                }
                catch (Exception exception)
                {
                    Logger.LogError("Failed to return a BeamApi formatted error message when handling the above error", exception);
                    // if failing, return a plain-text response
                    context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                    context.Response.Write(message);
                }
            }
        }
    }
}