﻿using System;
using System.Threading.Tasks;
using System.Web;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    /// <summary>
    /// <p>
    /// A thin wrapper around <see cref="IBeamApiTransactionWrapper"/>. Guarantees that <see cref="IBeamApiTransactionWrapper.Commit"/> is called,
    /// regardless of whether the underlying controller call was handled successfully or an exception was thrown.
    /// </p>
    /// <p>
    /// Note that the expectation is that <see cref="IBeamApiTransactionWrapper.Commit"/> does not throw any errors relating to
    /// data correctness (i.e. the SQL operations have completed successfully before the data is committed; such that the
    /// call to <see cref="IBeamApiSecurity.RequestEnd"/> is always committed successfully, regardless of data issues in the
    /// controller. This may not be the case with ORMs for instance.
    /// </p>
    /// <p>
    /// If this is not guaranteed to be the case, a custom middleware should be used instead to handle this interaction between the
    /// <see cref="IBeamApiSecurity"/> implementation and the database transaction handling.
    /// </p>
    /// </summary>
    public class BeamApiDatabaseTransactionMiddleware : BaseBeamApiMiddleware
    {
        public BeamApiDatabaseTransactionMiddleware(ILoggerFactory loggerFactory) : base(loggerFactory)
        {
        }

        public override async Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider)
        {
            var transactionWrapper = (IBeamApiTransactionWrapper)dependencyProvider.GetService(typeof(IBeamApiTransactionWrapper));
            transactionWrapper.Initialise();
            try
            {
                await next.Invoke(next, context, dependencyProvider);
            }
            finally
            {
                try
                {
                    transactionWrapper.Commit();
                }
                finally
                {
                    transactionWrapper.Dispose();
                }
            }
        }
    }
}