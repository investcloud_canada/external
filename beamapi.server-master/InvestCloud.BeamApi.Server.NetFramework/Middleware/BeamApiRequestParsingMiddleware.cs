using System;
using System.Threading.Tasks;
using System.Web;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;
using InvestCloud.BeamApi.Server.Shared;
using InvestCloud.BeamApi.Server.Shared.Exception;
using InvestCloud.BeamApi.Server.Shared.Serialization;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    /// <summary>
    /// Performs the following tasks:
    /// <ul>
    /// <li>De-serialises the request and serialises the response (using <see cref="RequestResponseSerialization"/>)</li>
    /// <li>Adds the SecurityKey and the UserId from the request to the <see cref="HttpContext.Items"/> (using the <see cref="Constants.BeamApiSessionSecurityKey"/> and <see cref="Constants.BeamApiSessionUserId"/> keys respectively)</li>
    /// <li>Calls the next middleware in the chain (typically, authorisation / session handling).</li>
    /// </ul>
    /// </summary>
    public class BeamApiRequestParsingMiddleware : BaseBeamApiMiddleware
    {
        public BeamApiRequestParsingMiddleware(ILoggerFactory loggerFactory) : base(loggerFactory)
        {
        }

        public override async Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider)
        {
            var serialization = (IRequestResponseSerialization)dependencyProvider.GetService(typeof(IRequestResponseSerialization));
            call2Request request;
            // 1. parse request
            try
            {
                request = serialization.ParseRequest(context.Request.InputStream);
                context.Items[Constants.BeamApiHttpContextRequestKey] = request;
            }
            catch (Exception e)
            {
                throw new BeamApiException("Couldn't parse incoming SOAP message: " + e.Message, e);
            }

            context.Items[Constants.BeamApiSessionSecurityKey] = request.arg0.security.securitykey;
            context.Items[Constants.BeamApiSessionUserId] = request.arg0.security.userId;

            // 2. call service to handle the request
            await next.Invoke(next, context, dependencyProvider);
            
            // persist the session (if required) & set the correct content type for the output
            // NCU POC - comment out session handling
            // Rplan.Framework.Dependencies.Dependencies.Container.GetInstance<IWebSessionProvider>().Save();
            context.Response.ContentType = "text/xml;charset=utf-8";
            
            // 3. serialize the response to the output stream
            try
            {
                var response = (call2Response) context.Items[Constants.BeamApiHttpContextResponseKey];
                serialization.SerializeResponse(response, context.Response.OutputStream);
            }
            catch (Exception e)
            {
                throw new BeamApiException("Error serializing SOAP response: " + e.Message, e);
            }
        }
    }
}