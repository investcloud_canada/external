﻿using System;
using System.Threading.Tasks;
using System.Web;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    /// <summary>
    /// A thin .net Core Middleware wrapper around <see cref="Service"/>.
    /// </summary>
    public class BeamApiControllerInvokerMiddleware : BaseBeamApiMiddleware
    {
        private readonly Service _svc;

        public BeamApiControllerInvokerMiddleware(ILoggerFactory loggerFactory, Service svc) : base(loggerFactory)
        {
            _svc = svc;
        }

        public override async Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider)
        {
            var request = (call2Request) context.Items[Constants.BeamApiHttpContextRequestKey];
            var response = await _svc.call2(request);
            context.Items[Constants.BeamApiHttpContextResponseKey] = response;
        }
    }
}