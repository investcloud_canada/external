﻿using System;
using System.Threading.Tasks;
using System.Web;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    /// <summary>
    /// A thin wrapper around <see cref="IBeamApiSecurity"/>. Guarantees that <see cref="IBeamApiSecurity.RequestEnd"/> is called,
    /// regardless of whether the underlying controller call was handled successfully or an exception was thrown.
    /// </summary>
    public class BeamApiSecurityMiddleware : BaseBeamApiMiddleware
    {
        public BeamApiSecurityMiddleware(ILoggerFactory loggerFactory) : base(loggerFactory)
        {
        }

        public override async Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider)
        {
            var beamApiSecurity = (IBeamApiSecurity)dependencyProvider.GetService(typeof(IBeamApiSecurity));
            var securityKey = (string) context.Items[Constants.BeamApiSessionSecurityKey];
            var userId = (string) context.Items[Constants.BeamApiSessionUserId];
            beamApiSecurity.RequestStart(securityKey, userId);
            try
            {
                await next.Invoke(next, context, dependencyProvider);
            }
            finally
            {
                beamApiSecurity.RequestEnd();
            }
        }
    }
}