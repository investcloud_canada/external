﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    /// <summary>
    /// Simple validation of the incoming request:
    /// <ul>
    /// <li>must match one of the paths specified in <see cref="IBeamApiOptions.Paths"/></li>
    /// <li>must be POST</li>
    /// <li>must be to a valid path</li>
    /// <li>Content-Length must be set and greater than 0 (i.e. the request body must be set)</li>
    /// <li>must have the '' content-type header</li>
    /// </ul>
    /// </summary>
    public class BeamApiRequestValidatorMiddleware : BaseBeamApiMiddleware
    {
        private readonly IBeamApiOptions _options;

        public BeamApiRequestValidatorMiddleware(ILoggerFactory loggerFactory, IBeamApiOptions options) : base(loggerFactory)
        {
            _options = options;
        }

        public override async Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider)
        {
            var requestPath = context.Request.Path;
            if (!_options.Paths.Any(x => requestPath.Equals(x, StringComparison.InvariantCultureIgnoreCase)))
            {
                OutputValidationError(context, $"Invalid path '{context.Request.Path}'; supported paths are {string.Join(", ", _options.Paths)}", (int)HttpStatusCode.NotFound);
                return;
            }

            if (context.Request.HttpMethod != "POST")
            {
                OutputValidationError(context, $"Unexpected method {context.Request.HttpMethod}", (int)HttpStatusCode.BadRequest);
                return;
            }

            var contentLengthStr = context.Request.Headers["Content-Length"];
            if (string.IsNullOrWhiteSpace(contentLengthStr) || !decimal.TryParse(contentLengthStr, out var contentLength) || contentLength == 0m)
            {
                OutputValidationError(context, $"Missing request content (length 0)", (int)HttpStatusCode.BadRequest);
                return;
            }

            await next.Invoke(next, context, dependencyProvider);
        }

        private void OutputValidationError(HttpContext context, string message, int statusCode)
        {
            context.Response.StatusCode = statusCode;
            context.Response.Write(message);
        }
    }
}
