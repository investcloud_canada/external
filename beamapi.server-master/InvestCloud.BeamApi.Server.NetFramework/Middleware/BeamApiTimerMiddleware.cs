﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    /// <summary>
    /// Simple middleware to record the time taken to execute the request. The time is stored in <see cref="audit.timeInCall"/>,
    /// and logged using an <see cref="ILogger"/> (at <see cref="LogLevel.Trace"/>).
    /// </summary>
    public class BeamApiTimerMiddleware : BaseBeamApiMiddleware
    {
        public BeamApiTimerMiddleware(ILoggerFactory loggerFactory) : base(loggerFactory)
        {
        }

        public override async Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider)
        {
            var watch = new Stopwatch();
            watch.Start();
            try
            {
                await next.Invoke(next, context, dependencyProvider);
            }
            finally
            {
                watch.Stop();
                call2Response response = null;
                if (context.Items.Contains(Constants.BeamApiHttpContextResponseKey))
                {
                    var resp = context.Items[Constants.BeamApiHttpContextResponseKey];
                    response = (call2Response) resp;
                    if (response.@return == null) response.@return = new messageEnvelope();
                    if (response.@return.audit == null) response.@return.audit = new audit();
                    response.@return.audit.timeInCall = (int) watch.ElapsedMilliseconds;
                    response.@return.audit.timeInCallSpecified = true;
                }
                if (Logger.IsEnabled(LogLevel.Trace))
                    Logger.LogTrace($"Time taken to execute {response?.@return?.control?.app}-{response?.@return?.control?.function}-{response?.@return?.control?.verb}: {watch.ElapsedMilliseconds}ms");
            }
        }
    }
}