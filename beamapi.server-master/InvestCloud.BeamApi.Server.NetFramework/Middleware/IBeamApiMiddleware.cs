using System;
using System.Threading.Tasks;
using System.Web;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    public interface IBeamApiMiddleware
    {
        Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider);
    }
}