﻿using System;
using System.Threading.Tasks;
using System.Web;
using InvestCloud.BeamApi.Server.NetFramework.Hosting;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.Extensions.Logging;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace InvestCloud.BeamApi.Server.NetFramework.Middleware
{
    public abstract class BaseBeamApiMiddleware : IBeamApiMiddleware
    {
        protected static ILogger Logger;
        
        public BaseBeamApiMiddleware(ILoggerFactory loggerFactory)
        {
            if (Logger == null)
                Logger = loggerFactory.CreateLogger(Constants.BeamApiLoggerName);
        }

        public abstract Task Invoke(IBeamApiContext next, HttpContext context, IServiceProvider dependencyProvider);
    }
}