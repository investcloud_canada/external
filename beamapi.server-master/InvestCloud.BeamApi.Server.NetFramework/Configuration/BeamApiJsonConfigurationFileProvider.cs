﻿using System.Configuration;
using System.IO;
using InvestCloud.BeamApi.Server.Shared.Configuration;
using InvestCloud.BeamApi.Server.Shared.Exception;
using Newtonsoft.Json;

namespace InvestCloud.BeamApi.Server.NetFramework.Configuration
{
    /// <summary>
    /// Loads a file whose path is specified in the 'config.beamapijsonconfig' appSetting in web.config
    /// </summary>
    public class BeamApiJsonConfigurationFileProvider : IBeamApiJsonConfigurationFileProvider
    {
        public BeamServiceMappingConfig GetConfiguration()
        {
            var configValue = ConfigurationManager.AppSettings["config.beamapijsonconfig"];
            var fileInfo = new FileInfo(System.Web.Hosting.HostingEnvironment.MapPath("/") + configValue);
            if (!fileInfo.Exists)
                throw new BeamApiException($"File specified in 'config.beamapijsonconfig' does not exist (full path: '{fileInfo.FullName}')");
            var jsonConfigText = File.ReadAllText(fileInfo.FullName);
            return JsonConvert.DeserializeObject<BeamServiceMappingConfig>(jsonConfigText);
        }
    }
}