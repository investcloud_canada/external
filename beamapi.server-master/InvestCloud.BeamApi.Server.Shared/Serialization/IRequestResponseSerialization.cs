﻿using System.IO;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Serialization
{
    public interface IRequestResponseSerialization
    {
        call2Request ParseRequest(Stream input);
        void SerializeResponse(call2Response response, Stream output);
    }
}