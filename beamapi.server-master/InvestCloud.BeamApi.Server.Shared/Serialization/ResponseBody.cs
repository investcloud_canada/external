﻿using System.Xml.Serialization;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Serialization
{
    public class ResponseBody
    {
        [XmlElement(ElementName = "call2Response", Namespace = "http://service.beam.be.com/")]
        public call2Response call2Response { get; set; }
    }
}