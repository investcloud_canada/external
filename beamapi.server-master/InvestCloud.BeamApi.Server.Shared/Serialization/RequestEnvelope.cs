﻿using System.Xml.Serialization;

namespace InvestCloud.BeamApi.Server.Shared.Serialization
{
    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class RequestEnvelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public RequestBody Body { get; set; }
    }
}