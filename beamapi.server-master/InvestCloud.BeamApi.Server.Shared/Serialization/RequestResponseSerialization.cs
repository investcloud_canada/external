using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Serialization
{
    public class RequestResponseSerialization : IRequestResponseSerialization
    {
        public call2Request ParseRequest(Stream input)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(RequestEnvelope));

            var requestEnvelope = xmlSerializer.Deserialize(input) as RequestEnvelope;

            return requestEnvelope.Body.call2Request;
        }

        public void SerializeResponse(call2Response response, Stream output)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ResponseEnvelope));

            var xmlWriterSettings = new XmlWriterSettings
            {
                // If set to true XmlWriter would close MemoryStream automatically and using would then do double dispose
                // Code analysis does not understand that. That's why there is a suppress message.
                CloseOutput = false,
                Encoding = Encoding.UTF8,
                OmitXmlDeclaration = false,
                Indent = false
            };
            using (var xmlWriter = XmlWriter.Create(output, xmlWriterSettings))
            {
                xmlSerializer.Serialize(xmlWriter,
                    new ResponseEnvelope {Body = new ResponseBody {call2Response = response}});
            }
        }
    }
}