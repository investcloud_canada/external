﻿using System.Xml.Serialization;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Serialization
{
    public class RequestBody
    {
        [XmlElement(ElementName = "call2", Namespace = "http://service.beam.be.com/")]
        public call2Request call2Request { get; set; }
    }
}