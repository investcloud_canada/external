﻿using System;

namespace Tracs.BeamApi.Framework
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class MappingKeyAttribute : Attribute
    {
        public string Key { get; set; }
    }
}