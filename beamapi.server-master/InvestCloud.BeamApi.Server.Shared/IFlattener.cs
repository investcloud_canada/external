﻿using System;
using System.Collections.Generic;

namespace InvestCloud.BeamApi.Server.Shared
{
    public interface IFlattener
    {
        Dictionary<string, string> GetFieldsForApf(Type t);

        Dictionary<string, Tuple<object, Type>> GetFieldValuesForKey(Type t,
            object mappedInstance,
            string useTranslations,
            object originalInstance = null);

        List<Dictionary<string, Tuple<object, Type>>> GetFieldValuesForLst<T>(
            Type t, 
            List<T> instances, 
            string useTranslations,
            object originalInstance = null) where T : new();

        T HydrateDtoFromBeamFields<T>(Dictionary<string, string> beamFields);
        Object ConvertStringToDataType(Type type, string beamValue, string fieldName);
    }
}