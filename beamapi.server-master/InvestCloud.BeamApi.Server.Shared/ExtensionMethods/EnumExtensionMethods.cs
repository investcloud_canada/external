﻿using System;

namespace InvestCloud.BeamApi.Server.Shared.ExtensionMethods
{
    public static class EnumExtensionMethods
    {
        public static string Name(this Enum @enum)
        {
            var name = Enum.GetName(@enum.GetType(), @enum);
            return name;
        }
    }
}
