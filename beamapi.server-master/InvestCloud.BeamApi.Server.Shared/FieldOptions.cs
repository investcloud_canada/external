﻿using System;

namespace InvestCloud.BeamApi.Server.Shared
{
    [Serializable]
    public enum FieldOption
    {
        Optional = 1,
        Required = 2,
        Readonly = 3,
        Conditional = 4,
        NotCaptured = 5
    }
}
