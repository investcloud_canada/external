﻿using System;

namespace InvestCloud.BeamApi.Server.Shared
{
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Property | AttributeTargets.Field)]
    public class FieldOptionsAttribute : Attribute
    {
        public string FieldOptionFieldName { get; set; }
        public string FieldOptionsSettingName { get; set; }
    }

}
