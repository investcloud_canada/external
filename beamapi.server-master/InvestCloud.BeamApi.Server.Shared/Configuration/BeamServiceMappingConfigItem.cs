﻿namespace InvestCloud.BeamApi.Server.Shared.Configuration
{
    public class BeamServiceMappingConfigItem
    {
        public string App  {get; set;}
        public string Function {get;set;}
        public string Verb {get;set;}
        public string Interface {get;set;}
        public string Method {get;set;}
        public string Request {get;set;}
        public string Response {get;set;}
    }
}