﻿namespace InvestCloud.BeamApi.Server.Shared.Configuration
{
    public interface IBeamApiJsonConfigurationFileProvider
    {
        BeamServiceMappingConfig GetConfiguration();
    }
}