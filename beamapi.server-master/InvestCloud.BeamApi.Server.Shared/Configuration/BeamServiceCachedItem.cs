﻿using System;

namespace InvestCloud.BeamApi.Server.Shared.Configuration
{
    public class BeamServiceCachedItem
    {
        public Object EnvelopedDataToInputDtoObjectFunc { get; set; }
        public Object InputDtoObjectToInputObjectFunc { get; set; }
        public Object CallServiceFunc { get; set; }
        public Object OutputObjectToOutputDtoObjectFunc { get; set; }
        public BeamServiceMappingConfigItem ConfigItem { get; set; }

    }
}