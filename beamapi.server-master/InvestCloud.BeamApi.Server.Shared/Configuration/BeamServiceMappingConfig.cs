﻿using System.Collections.Generic;

namespace InvestCloud.BeamApi.Server.Shared.Configuration
{
    public class BeamServiceMappingConfig
    {
        public BeamServiceMappingConfig() {
            Services = new List<BeamServiceMappingConfigItem>();
        }

        public List<BeamServiceMappingConfigItem> Services { get; set; }
    }
}