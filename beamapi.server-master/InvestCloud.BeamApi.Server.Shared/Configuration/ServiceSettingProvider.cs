﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared.Exception;

namespace InvestCloud.BeamApi.Server.Shared.Configuration
{
    public interface IServiceSettingProvider
    {
        BeamServiceCachedItem GetBeamServiceCachedItemByKey(string appName, string functionName, string verbName);
    }

    public class ServiceSettingProvider : IServiceSettingProvider
    {
        private readonly IConverter _converter;
        private readonly IBeamApiJsonConfigurationFileProvider _configurationFileProvider;
        private Dictionary<string, BeamServiceCachedItem> _beamServiceMappingCache;
        
        public ServiceSettingProvider(IConverter converter, IBeamApiJsonConfigurationFileProvider configurationFileProvider)
        {
            _converter = converter;
            _configurationFileProvider = configurationFileProvider;
            _beamServiceMappingCache = new Dictionary<string, BeamServiceCachedItem>();
            BeamServiceMappingConfig = new BeamServiceMappingConfig();
        }

        public BeamServiceMappingConfig BeamServiceMappingConfig { get; set; }

        public void Initialise()
        {
            
            // NCU POC var jsonConfigText = File.ReadAllText(config.GetRequired("config.beamapijsonconfig"));
            BeamServiceMappingConfig = _configurationFileProvider.GetConfiguration();//  JsonConvert.DeserializeObject<BeamServiceMappingConfig>(jsonConfigText);

            IsInitialised = true;
            SetBeamServiceMappingCache();
        }

        public void SetBeamServiceMappingCache()
        {
            ServiceConfigurationPrecondtion();

            var localCache = new Dictionary<string, BeamServiceCachedItem>();

            SetAppApfFunction(localCache);

            SetAppOtherFunctions(localCache);

            Interlocked.Exchange(ref _beamServiceMappingCache, localCache);
        }

        public void ServiceConfigurationPrecondtion()
        {
            var invalidAppNames = BeamServiceMappingConfig.Services
                .Where(x => string.IsNullOrEmpty(x.App)).Select(x => $"{x.App}-{x.Function}-{x.Verb}");
            if (invalidAppNames.Any())
                throw new BeamApiException($"Empty app names for items: {String.Join(String.Empty, invalidAppNames)}.");

            var invalidFunctionNames = BeamServiceMappingConfig.Services
                .Where(x => !Enum.GetNames(typeof(BeamFunctionName)).Contains(x.Function)).Select(x => x.Function);
            if (invalidFunctionNames.Any())
                throw new BeamApiException($"Invalid function names: {String.Join(String.Empty, invalidFunctionNames)}.");

            var invalidVerbs = BeamServiceMappingConfig.Services
                .Where(x => !Enum.GetNames(typeof(BeamFunctionVerb)).Contains(x.Verb)).Select(x => x.Verb);
            if (invalidVerbs.Any())
                throw new BeamApiException($"Invalid verb names: {String.Join(String.Empty, invalidVerbs)}.");

            var invalidMethodNames = BeamServiceMappingConfig.Services
                .Where(x => string.IsNullOrEmpty(x.Method)).Select(x => $"{x.App}-{x.Function}-{x.Verb}");
            if (invalidMethodNames.Any())
                throw new BeamApiException($"Invalid method names for items: {String.Join(String.Empty, invalidMethodNames)}.");
        }



        public void SetAppApfFunction(Dictionary<string, BeamServiceCachedItem> serviceDict)
        {
            var services = BeamServiceMappingConfig.Services.Select(x => new
            {
                x.App,
                x.Response,
                x.Request
            }).GroupBy(x => new {x.App, x.Response}, (c, b) => new
            {
                App = c.App,
                Response = c.Response,
                Requests = b.Where(x => !string.IsNullOrWhiteSpace(x.Request)).Select(x => Type.GetType(x.Request))
            }).ToList();
            foreach (var service in services)
            {
                var type = Type.GetType(service.Response);

                var fields = _converter.GetFieldInfoForApf(type, service.Requests.ToList());

                var item = new BeamServiceCachedItem() { CallServiceFunc = (Func<fieldInfo[]>)(() => fields) };
                serviceDict.Add($"{service.App}-APF-I", item);
                serviceDict.Add($"{service.App}-DTQ-R", item);
            }
        }

        public void SetAppOtherFunctions(Dictionary<string, BeamServiceCachedItem> serviceDict)
        {
            foreach (var x in BeamServiceMappingConfig.Services.Where(x => !String.IsNullOrEmpty(x.Function)&& !String.IsNullOrEmpty(x.Verb)))
            {
                //Create lambda expression func to call the service, and auto mapper method 
                serviceDict.Add($"{x.App}-{x.Function}-{x.Verb}",
                    CreateBeamServiceCachedItem(x)
                );
                //Whenever we set KEY-C, we also need to create KEY-R
                //We can't just create KEY-R, as in TestHarness there is no KEY-R
                if (x.Function == BeamFunctionName.KEY.ToString())
                {
                    serviceDict.Add($"{x.App}-{x.Function}-R",
                        CreateBeamServiceCachedItem(x)
                    );
                }

            }
        }

        private BeamServiceCachedItem CreateBeamServiceCachedItem(BeamServiceMappingConfigItem x)
        {
            return new BeamServiceCachedItem()
            {
                EnvelopedDataToInputDtoObjectFunc = String.IsNullOrEmpty(x.Request) ? null : DtoMapper.CreateLambdaExpressionFuncForConvertingFieldsToInputDto(x.Request),
                InputDtoObjectToInputObjectFunc = String.IsNullOrEmpty(x.Request) ? null : DtoMapper.CreateLambdaExpressionFuncForDtoMappingToInputObject(x.Interface, x.Method),
                CallServiceFunc = ServiceMapper.CreateLambdaExpressionFunc(x.Interface, x.Method),
                OutputObjectToOutputDtoObjectFunc = String.IsNullOrEmpty(x.Response)?null:
                    (x.Function == nameof(BeamFunctionName.LST)|| x.Function == nameof(BeamFunctionName.MUP)) ? 
                        DtoMapper.CreateLambdaExpressionFuncForDtoMappingToListOutputDto(x.Response): 
                        DtoMapper.CreateLambdaExpressionFuncForDtoMappingToOutputDto(x.Response),
                ConfigItem = x
            };
        }

        private void UpdateBeamServiceMappingCache(Object updatedConfig)
        {
            BeamServiceMappingConfig = (BeamServiceMappingConfig)updatedConfig;
            SetBeamServiceMappingCache();
        }

        public BeamServiceCachedItem GetBeamServiceCachedItemByKey(string appName, string functionName, string verbName)
        {
            var servicekey = $"{appName}-{functionName}-{verbName}";
            if (this.BeamServiceMappingCache.ContainsKey(servicekey))
            {
                return this.BeamServiceMappingCache[servicekey];
            }
            throw new BeamApiException($"Invalid service request with {servicekey}");
        }

        private Dictionary<string, BeamServiceCachedItem> BeamServiceMappingCache
        {
            get
            {
                if (!IsInitialised)
                {
                    Initialise();
                    return _beamServiceMappingCache;
                }

                return _beamServiceMappingCache;
            }
        }


        public bool IsInitialised { get; set; }
    }
}