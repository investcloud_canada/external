using System;

namespace InvestCloud.BeamApi.Server.Shared.DTO
{
    [Serializable]
    public class PagingRequest
    {
        private const int MaxResultsPerPage = 100;
        private int _resultsPerPage = MaxResultsPerPage;
        // [DtoPrivacy(DtoPrivacyType.None)]
        public int CurrentPage { get; set; }

        // [DtoPrivacy(DtoPrivacyType.None)]
        public int ResultsPerPage
        {
            get { return _resultsPerPage; }
            // prevent users from returning an unlimited number of rows
            set { if (value < MaxResultsPerPage) _resultsPerPage = value; }
        }
    }
}