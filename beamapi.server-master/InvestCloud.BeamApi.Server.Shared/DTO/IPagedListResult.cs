﻿namespace InvestCloud.BeamApi.Server.Shared.DTO
{
    public interface IPagedListResult
    {
        PagingResponse PagingResponse { get; set; }
    }   
}