using System;

namespace InvestCloud.BeamApi.Server.Shared.DTO
{
    [Serializable]
    public class PagingResponse : PagingRequest
    {
        private int _totalPages = 0;
        public PagingResponse()
        {
        }

        public PagingResponse(PagingRequest request)
        {
            CurrentPage = request.CurrentPage;
            ResultsPerPage = request.ResultsPerPage;
        }
        // [DtoPrivacy(DtoPrivacyType.None)]
        public int TotalCount { get; set; }
        //[DtoPrivacy(DtoPrivacyType.None)]
        public int TotalPages { 
            get
            {
                if (_totalPages == 0)
                {
                    if (TotalCount == 0)
                        return 0;

                    _totalPages = (TotalCount%ResultsPerPage == 0 && TotalCount > 0
                        ? TotalCount/ResultsPerPage
                        : TotalCount/ResultsPerPage + 1);
                }
                return _totalPages;
            }
        }
    }
}