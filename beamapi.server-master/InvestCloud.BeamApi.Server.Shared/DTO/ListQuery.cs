﻿
using Tracs.BeamApi.Framework;

namespace InvestCloud.BeamApi.Server.Shared.DTO
{
    public class ListQuery
    {
        [MappingKey(Key = "$top")]
        public string Top
        {
            get;
            set;
        }

        [MappingKey(Key = "$skip")]
        public string Skip
        {
            get;
            set;
        }

        [MappingKey(Key = "$filter")]
        public string Filter {
            get;
            set;
        }

        [MappingKey(Key = "$orderby")]
        public string OrderBy
        {
            get;
            set;
        }

        //[MappingKey(Key = "$executeQueryForCount")]
        //public string ExecuteQueryForCount
        //{
        //    get;
        //    set;
        //}
    }
}