﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace InvestCloud.BeamApi.Server.Shared
{
    public class ServiceMapper
    {

        public static dynamic CreateLambdaExpressionFunc(string tService, string methodName)
        {
            var objType = typeof(object);
            var instance = Expression.Parameter(objType, "serviceInstance");
            var inputServiceType = Type.GetType(tService);
            var isParamNeeded = true;
            var methodInfo = inputServiceType.GetMethods().FirstOrDefault(x => x.Name == methodName && x.GetParameters().Length == 1);
            if (methodInfo == null)
            {
                isParamNeeded = false;
                methodInfo = inputServiceType.GetMethods().FirstOrDefault(x => x.Name == methodName);
            }

            if (methodInfo == null)
                throw new System.Exception($"Can't find the method {tService}.{methodName} with one parameter or the ApiMethod attributed method with no parameters during creating expression func");

            if (isParamNeeded)
            {
                var inputType = methodInfo.GetParameters().First().ParameterType;
                var input = Expression.Parameter(objType, "inputData");

                var call = Expression.Call(
                    Expression.Convert(instance, inputServiceType),
                    methodInfo,
                    Expression.Convert(input, inputType));
                var body = Expression.Convert(call, objType);
                var lambdaFunc = Expression.Lambda<Func<object, object, dynamic>>(body, instance, input).Compile();
                return lambdaFunc;
            }
            else
            {
                var call = Expression.Call(
                    Expression.Convert(instance, inputServiceType),
                    methodInfo);
                var body = Expression.Convert(call, objType);
                var lambdaFunc = Expression.Lambda<Func<object, dynamic>>(body, instance).Compile();
                return lambdaFunc;
            }
        }

    }
}