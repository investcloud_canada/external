﻿namespace InvestCloud.BeamApi.Server.Shared.Exception
{
    public class BeamApiException : System.Exception
    {
        public BeamApiException(string message)
            : base(message)
        {
        }
        
        public BeamApiException(string message, System.Exception e)
            : base(message, e)
        {
        }
    }
}