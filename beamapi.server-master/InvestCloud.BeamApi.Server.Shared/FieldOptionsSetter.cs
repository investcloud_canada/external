﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tracs.BeamApi.Framework
{
    public class FieldOptionsSetter<TT>
    {
        public FieldOptionsSetter(
            object originalInstance = null,
            string fieldOptionsSettingName = null,
            Func<object, string, string, string, TT> fnToSetFieldOption = null
        )
        {
            OriginalInstance = originalInstance;
            FieldOptionsSettingName = fieldOptionsSettingName;
            FnToSetFieldOption = fnToSetFieldOption;
        }

        public object OriginalInstance { get; private set; }
        public string FieldOptionsSettingName { get; set; }
        public Func<object, string, string, string, TT> FnToSetFieldOption { get; private set; }
    }
}
