﻿using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared
{
    public static class Constants
    {
        public const string BeamApiHttpContextRequestKey = nameof(BeamApiHttpContextRequestKey);
        public const string BeamApiHttpContextResponseKey = nameof(BeamApiHttpContextResponseKey);
        /// <summary>
        /// The name of the logger (e.g. log4net, Microsoft.Extensions.Logging, etc.).
        /// Shared across both the .Net Framework and .Net Core implementations.
        /// </summary>
        public const string BeamApiLoggerName = "InvestCloud.BeamApi.Server";
        /// <summary>
        /// The key used to store the session Id in the HttpContext (as retrieved from the <see cref="messageEnvelope"/> when the call was received).
        /// Shared across both the .Net Framework and .Net Core implementations.
        /// </summary>
        public const string BeamApiSessionSecurityKey = "InvestCloud.BeamApi.Server.SecurityKey";
        public const string BeamApiSessionUserId = "InvestCloud.BeamApi.Server.UserId";
    }
}