using System;

namespace InvestCloud.BeamApi.Server.Shared
{
    public interface IBeamApiTransactionWrapper : IDisposable
    {
        void Initialise();
        void Commit();
        bool IsInitialised();
    }
}