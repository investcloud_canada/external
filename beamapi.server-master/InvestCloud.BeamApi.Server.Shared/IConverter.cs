﻿using System;
using System.Collections.Generic;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared.DTO;

namespace InvestCloud.BeamApi.Server.Shared
{
    public interface IConverter
    {
        fieldInfo[] GetFieldInfoForApf(Type responseType, IList<Type> requestTypes);
        fieldInfo[] GetFieldInfoForKey(Type t, object mappedInstance, object originalInstance, string useTranslations = null);
        Tuple<fieldInfo[], byte[][]> GetFieldInfoForLst<T>(Type t, List<T> instances, PagingResponse pagingResponse, string useTranslations = null, object originalInstance = null) where T : new();
        List<T> GetListFromListData<T>(byte[][] listData);
        Object GetDtoFromFields<T>(messageEnvelope messageEnvelope);
        string ConvertObjectToString(object rawValue, Type type);
    }
}