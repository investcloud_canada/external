﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared.Configuration;
using InvestCloud.BeamApi.Server.Shared.DTO;
using InvestCloud.BeamApi.Server.Shared.Errors;
using InvestCloud.BeamApi.Server.Shared.ExtensionMethods;
using InvestCloud.BeamApi.Server.Shared.Translatable;

namespace InvestCloud.BeamApi.Server.Shared
{
    public class Service: IService
    {
        private readonly IServiceSettingProvider _serviceSettingProvider;
        private readonly IServiceProvider _serviceCollection;
        private readonly IBeamApiErrorHandler _beamApiErrorHandler;
        private readonly IControllerErrorHandler _controllerErrorHandler;
        private readonly ITranslatableHandler _translatableHandler;
        private readonly IConverter _converter;

        public Service(IServiceSettingProvider serviceSettingProvider, IServiceProvider serviceCollection, IBeamApiErrorHandler beamApiErrorHandler, IControllerErrorHandler controllerErrorHandler, ITranslatableHandler translatableHandler, IConverter converter)
        {
            this._serviceSettingProvider = serviceSettingProvider;
            this._serviceCollection = serviceCollection;
            _beamApiErrorHandler = beamApiErrorHandler;
            _controllerErrorHandler = controllerErrorHandler;
            _translatableHandler = translatableHandler;
            _converter = converter;
        }
        public async Task<call2Response> call2(call2Request request)
        {
            // NCU POC var serviceSettingProvider =
            // NCU POC     Rplan.Framework.Dependencies.Dependencies.Container.GetInstance<IServiceSettingProvider>();
            var response = new messageEnvelope();

            var watch = new Stopwatch();
            watch.Start();
            try
            {
               // NCU POC:  using (Rplan.Framework.Dependencies.Dependencies.Container.BeginScope())
               // NCU POC:  {
                    // NCU POC: var databaseSessionOrange = Rplan.Framework.Dependencies.Dependencies.Container
                    // NCU POC:     .GetInstance<DatabaseSessionOrange>();
                    // NCU POC:  var beamApiSecurityService = Rplan.Framework.Dependencies.Dependencies.Container
                    // NCU POC:     .GetInstance<IBeamApiSecurityService>();


                    // NCU POC: using (var tx = databaseSessionOrange.Session.BeginTransaction())
                    // NCU POC: {
                        try
                        {
                            // NCU POC: beamApiSecurityService.SetupCurrentUserFromSessionKey(
                            // NCU POC:     MessageEnvelopeHelper.GetSessionKey(request.arg0));

                            var appName = request.arg0.control.app;
                            var functionName = request.arg0.control.function;
                            var verbName = request.arg0.control.verb;
                            var beamServiceCachedItem = _serviceSettingProvider.GetBeamServiceCachedItemByKey(appName, functionName, verbName);

                            if (IsMessageEnvelopeFunctionSameAsParam(request.arg0, BeamFunctionName.APF) ||
                                IsMessageEnvelopeFunctionSameAsParam(request.arg0, BeamFunctionName.DTQ))
                            {
                                response.fields = ((Func<fieldInfo[]>)beamServiceCachedItem.CallServiceFunc)();
                                response.listData = new byte[0][];
                            }
                            else
                            {
                                var rawResult = await InvokeServiceMethod(request.arg0, beamServiceCachedItem);
                                var converted =
                                    (Tuple<fieldInfo[], byte[][]>)ConvertRawResult(rawResult, request.arg0, beamServiceCachedItem);
                                response.fields = converted.Item1;
                                response.listData = converted.Item2;
                            }

                            response.Status = (ushort)MessageStatus.Success;
                        }
                        catch (System.Exception ex)
                        {
                            while (ex.InnerException != null) ex = ex.InnerException;
                            _beamApiErrorHandler.Handle(ex, response);
                        }
                        finally
                        {
                            bool flushSucceded = false;
                            try //Flush may still fail
                            {
                                // NCU POC: databaseSessionOrange.Session.Flush();
                                flushSucceded = true;
                            }
                            finally
                            {
                                //Just in case clear clears everything pre flush too
                                if (!flushSucceded)
                                {
                                    // NCU POC: databaseSessionOrange.Session.Clear(); //Clear is required here because in the case where flush fails, CreateSQLQuery inside TearDownCurrentUser will try to flush again before executing.
                                }
                                // NCU POC: beamApiSecurityService.TearDownCurrentUser();
                                // NCU POC: tx.Commit();

                                // NCU POC: Rplan.Framework.Dependencies.Dependencies.Container.GetInstance<NHibernateContextSessionProvider>().Dispose();
                            }
                        }
                    // NCU POC: }
                // NCU POC: }
            }
            catch (System.Exception ex)
            {
                _beamApiErrorHandler.Handle(ex, response);
            }
            finally
            {
                watch.Stop();
                response.audit = new audit { timeInCall = (int)watch.ElapsedMilliseconds };
                response.control = request.arg0.control;
                response.security = request.arg0.security;
            }

            return new call2Response(response);
        }

        private bool IsMessageEnvelopeFunctionSameAsParam(messageEnvelope arg0, BeamFunctionName funcName)
        {
            return String.Compare(arg0.control.function, funcName.Name(), StringComparison.OrdinalIgnoreCase) == 0;
        }

        private async Task<dynamic> InvokeServiceMethod(messageEnvelope arg0, BeamServiceCachedItem cachedObject)
        {
            var businessService = _serviceCollection.GetService(
                // NCU POC Rplan.Framework.Dependencies.Dependencies.Container.GetInstance(
                    Type.GetType(cachedObject.ConfigItem.Interface));
            dynamic rawResult = null;

            //If the request DTO is empty, which means only need to pass the user Id to the service layer
            if (cachedObject.EnvelopedDataToInputDtoObjectFunc == null)
            {
                var func = (Func<Object, dynamic>)cachedObject.CallServiceFunc;
                rawResult = await func(businessService);
            }
            else
            {
                var func = (Func<Object, Object, dynamic>)cachedObject.CallServiceFunc;
                // NCU POC var inputDataMapper = (Func<IMapper, object, object>)cachedObject.InputDtoObjectToInputObjectFunc;
                var inputData = ((Func<IConverter, messageEnvelope, object>)cachedObject.EnvelopedDataToInputDtoObjectFunc)(_converter, arg0);
                Object mappedResult = inputData; // NCU POC inputDataMapper(Rplan.Framework.Dependencies.Dependencies.Container.GetInstance<IMapper>(), inputData);

                rawResult = await func(businessService, mappedResult);
            }

            if (rawResult.GetType().GetProperty("Exception") != null && rawResult.Exception != null)
            {
                var ex = (System.Exception)rawResult.Exception;
                while (ex.InnerException != null) ex = ex.InnerException;
                _controllerErrorHandler.Handle(ex, arg0.control.app, (BeamFunctionName)Enum.Parse(typeof(BeamFunctionName), arg0.control.function), (BeamFunctionVerb)Enum.Parse(typeof(BeamFunctionVerb), arg0.control.verb), arg0.control.clientName);
                /*
                if (ex != null)
                {
                    _controllerErrorHandler.Handle(ex);
                    // NCU POC: if (ex is RestrictionException restrictionException)
                    // NCU POC: {
                    // NCU POC:     throw restrictionException;
                    // NCU POC: }
                    if (ex is ValidationErrorsException validationErrorsException)
                    {
                        throw validationErrorsException;
                    }
                    else if (ex is PreconditionFailedException preconditionFailedException)
                    {
                        throw preconditionFailedException;
                    }
                    else if (ex is TechnicalException technicalException)
                    {
                        ExceptionDispatchInfo.Capture(technicalException).Throw();
                    }
                    ExceptionDispatchInfo.Capture(ex).Throw();
                }
                ExceptionDispatchInfo.Capture((System.Exception)rawResult.Exception).Throw();
                */
            }
            return rawResult;
        }

        /// <summary>
        /// If the result is wrapped in a Task, then need to unwrap the result 
        /// </summary>
        /// <param name="rawResult"></param>
        /// <param name="cachedObject"></param>
        /// <returns></returns>
        private Tuple<fieldInfo[], byte[][]> ConvertRawResult(dynamic rawResult, messageEnvelope messageEnvelope, BeamServiceCachedItem cachedObject)
        {
            // var orginalResponse = rawResult.GetType().GetProperty("Result") != null ? rawResult.Result : rawResult;
            var orginalResponse = rawResult.GetType().GetMethod("GetAwaiter") != null ? rawResult.GetAwaiter().GetResult() : rawResult;
            var mappedResult = orginalResponse;
                // NCU POC((Func<IMapper, object, object>)cachedObject.OutputObjectToOutputDtoObjectFunc)(
                    // NCU POCRplan.Framework.Dependencies.Dependencies.Container.GetInstance<IMapper>(),
                    // NCU POCorginalResponse);
            if (cachedObject.OutputObjectToOutputDtoObjectFunc != null)
            {
                var responseType = Type.GetType(cachedObject.ConfigItem.Response);
                var requestType = Type.GetType(cachedObject.ConfigItem.Request);
                var useTranslations = _translatableHandler.GetTranslation(requestType, responseType, messageEnvelope);
                /* NCU TODO abstract
                if (requestType != null && typeof(ITranslatable).IsAssignableFrom(requestType)
                    || requestType == null && typeof(ITranslatable).IsAssignableFrom(responseType))
                {                    
                    useTranslations = messageEnvelope.fields?.FirstOrDefault(x => x.fieldName == Flattener.UseTranslationsFieldName)?.value;                 
                }                
                */

                if (cachedObject.ConfigItem.Function == nameof(BeamFunctionName.LST) || cachedObject.ConfigItem.Function == nameof(BeamFunctionName.MUP))
                {
                    PagingResponse pagingResponse = null;

                    if (orginalResponse is IPagedListResult)
                        pagingResponse = (orginalResponse as IPagedListResult).PagingResponse;
                    return _converter.GetFieldInfoForLst(responseType, mappedResult, pagingResponse, useTranslations);
                }
                else //If the function is not LST, then convert the result as fieldinfo array
                {
                    return new Tuple<fieldInfo[], byte[][]>(
                        _converter.GetFieldInfoForKey(responseType, mappedResult, orginalResponse, useTranslations),
                        new byte[0][]);
                }
            }
            //When there is no return from the API
            return new Tuple<fieldInfo[], byte[][]>(new[] { new fieldInfo() }, new byte[0][]);
        }
    }
}
