﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared
{
    public class DtoMapper
    {
        public static Func<IMapper, object, object> CreateLambdaExpressionFuncForDtoMappingToInputObject(
            string tService,
            string methodName)
        {
            var inputServiceType = Type.GetType(tService);
            var methodInfo = inputServiceType.GetMethods().First(x => x.Name == methodName && x.GetParameters().Length == 1);
            var inputType = methodInfo.GetParameters().First().ParameterType;
            var mapperType = typeof(AutoMapper.IMapper);
            var mapMethodInfo = mapperType.GetMethods()
                .First(mi => mi.Name == "Map" && mi.GetGenericArguments().Count() == 1);
            var genericMapMethod = mapMethodInfo.MakeGenericMethod(inputType);
            var instance = Expression.Parameter(mapperType, "IMapperInstance");
            var dtoInputData = Expression.Parameter(typeof(object), "dtoInputData");
            var call = Expression.Call(instance, genericMapMethod, dtoInputData);
            var body = Expression.Convert(call, typeof(object));
            var lambdaFunc = Expression.Lambda<Func<IMapper, object, object>>(body, instance, dtoInputData).Compile();
            return lambdaFunc;
        }

        public static Func<IMapper, object, object> CreateLambdaExpressionFuncForDtoMappingToListOutputDto(string businessOutputDataTypeName)
        {
            var listType = typeof(IList<>);
            var businessOutputDataType = listType.MakeGenericType(Type.GetType(businessOutputDataTypeName));
            return LambdaExpressionFuncForDtoMappingToOutputDto(businessOutputDataType);
        }

        public static Func<IMapper, object, object> CreateLambdaExpressionFuncForDtoMappingToOutputDto(string businessOutputDataTypeName)
        {
            var businessOutputDataType = Type.GetType(businessOutputDataTypeName);
            return LambdaExpressionFuncForDtoMappingToOutputDto(businessOutputDataType);
        }

        private static Func<IMapper, object, object> LambdaExpressionFuncForDtoMappingToOutputDto(Type businessOutputDataType)
        {
            var mapperType = typeof(AutoMapper.IMapper);
            var mapMethodInfo = mapperType.GetMethods()
                .First(mi => mi.Name == "Map" && mi.GetGenericArguments().Count() == 1);
            var genericMapMethod = mapMethodInfo.MakeGenericMethod(businessOutputDataType);
            var instance = Expression.Parameter(mapperType, "IMapperInstance");
            var businessOutputData = Expression.Parameter(typeof(object), "businessOutputData");
            var call = Expression.Call(instance, genericMapMethod, businessOutputData);
            var body = Expression.Convert(call, typeof(object));
            var lambdaFunc = Expression.Lambda<Func<IMapper, object, object>>(body, instance, businessOutputData)
                .Compile();
            return lambdaFunc;
        }


        public static Func<IConverter, messageEnvelope, object> CreateLambdaExpressionFuncForConvertingFieldsToInputDto(
            string intputDtoDataTypeName)
        {
            var mi = typeof(IConverter).GetMethod(nameof(IConverter.GetDtoFromFields));
            var GetDtoFromFieldsFunc = mi.MakeGenericMethod(Type.GetType(intputDtoDataTypeName));
            var instance = Expression.Parameter(typeof(IConverter), "IConverterInstance");
            var fieldsData = Expression.Parameter(typeof(messageEnvelope), "fieldsData");
            var call = Expression.Call(instance, GetDtoFromFieldsFunc, fieldsData);
            var body = Expression.Convert(call, typeof(object));
            var lambdaFunc = Expression.Lambda<Func<IConverter, messageEnvelope, object>>(body, instance, fieldsData)
                .Compile();
            return lambdaFunc;
        }
    }
}