﻿namespace InvestCloud.BeamApi.Server.Shared
{
    public interface IBeamApiOptions
    {
        string[] Paths { get; set; }
        bool Debug { get; set; }
    }

    public class BeamApiOptions : IBeamApiOptions
    {
        public BeamApiOptions()
        {
            Paths = new string[]{};
        }
        
        public string[] Paths { get; set; }
        public bool Debug { get; set; }
    }
}