﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared.Attributes;
using InvestCloud.BeamApi.Server.Shared.DTO;
using InvestCloud.BeamApi.Server.Shared.Exception;
using InvestCloud.BeamApi.Server.Shared.Translatable;
using Tracs.BeamApi.Framework;

namespace InvestCloud.BeamApi.Server.Shared
{
    public class Converter : IConverter
    {
        private readonly IFlattener _flattener;
        private readonly ITranslatableHandler _translatableHandler;
        private const string Count = "__count";

        public Converter(IFlattener flattener, ITranslatableHandler translatableHandler)
        {
            _flattener = flattener;
            _translatableHandler = translatableHandler;
        }

        public fieldInfo[] GetFieldInfoForApf(Type responseType, IList<Type> requestTypes)
        {
            var res = _flattener.GetFieldsForApf(responseType);
            if (requestTypes != null && requestTypes.Any(x => x != null && (x.IsSubclassOf(typeof(ListQuery)) || x == typeof(ListQuery))))
            {
                var properties = typeof(ListQuery).GetProperties();
                foreach (var property in properties)
                {
                    var attribute = property.GetCustomAttribute<MappingKeyAttribute>();
                    if (attribute != null) res.Add(attribute.Key, string.Empty);
                }
            }

            _translatableHandler.MaybeAddUseTranslationsFieldName(requestTypes, res);
            /* TODO NCU abstract
            if (requestTypes != null && requestTypes.Count > 0 && requestTypes.Any(x => x != null && typeof(ITranslatable).IsAssignableFrom(x)))
            {
                if (!res.ContainsKey(_flattener.UseTranslationsFieldName))
                    res.Add(_flattener.UseTranslationsFieldName, string.Empty);
            }
            */

            return res.Select(kvp => new fieldInfo
            {
                fieldName = kvp.Key,
                fieldState = (ushort)FieldState.DisplayOnly,
                enrichment = kvp.Value
            }).ToArray();
        }

        public fieldInfo[] GetFieldInfoForKey(Type t, object mappedInstance, object originalInstance, string useTranslations = null)
        {
            var res = _flattener.GetFieldValuesForKey(t, mappedInstance, useTranslations, originalInstance);
            return res.Select(kvp => new fieldInfo
            {
                fieldName = kvp.Key,
                fieldState = (ushort)FieldState.Unknown,
                value = kvp.Value.Item1 == null? null: ConvertObjectToString(kvp.Value.Item1, kvp.Value.Item2)
            }).ToArray();
        }
        public Tuple<fieldInfo[], byte[][]> GetFieldInfoForLst<T>(Type t, List<T> instances, PagingResponse pagingResponse, string useTranslations = null, object originalInstance = null) where T : new()
        {
            var res = _flattener.GetFieldValuesForLst(t, instances, useTranslations, originalInstance);

            if (pagingResponse != null && res.Count > 1)
            {
                res[0].Add(Count, Tuple.Create(null as object, pagingResponse.TotalCount.GetType()));
                res[1].Add(Count, Tuple.Create(pagingResponse.TotalCount as object, pagingResponse.TotalCount.GetType()));
            }
            var fieldInfo = res.First().Select(kvp => new fieldInfo
            {
                fieldName = kvp.Key,
                fieldState = (ushort)FieldState.DisplayOnly
            }).ToArray();
            var listData = new byte[res.Count][];
            // header row
            {
                var bytes = new List<byte>();
                for (var i = 0; i < res[0].Keys.Count; i++)
                {
                    var key = res[0].Keys.ElementAt(i);
                    bytes.AddRange(Encoding.UTF8.GetBytes(key));
                    if (i != res[0].Keys.Count - 1)
                        bytes.Add((byte)'\u0001');
                }
                listData[0] = bytes.ToArray();
            }

            // data, start from index 1, as the item in index 0 is for header only
            for (var i = 1; i < res.Count; i++)
            {
                var line = res[i];
                var bytes = new List<byte>();
                for (var j = 0; j < line.Keys.Count; j++)
                {
                    var key = line.Keys.ElementAt(j);
                    if (line[key].Item1 != null)
                        bytes.AddRange(Encoding.UTF8.GetBytes(ConvertObjectToString(line[key].Item1, line[key].Item2)));
                    if (j != line.Keys.Count - 1)
                        bytes.Add((byte)'\u0001');

                }
                listData[i] = bytes.ToArray();
            }
            return Tuple.Create(fieldInfo, listData);
        }

        public List<T> GetListFromListData<T>(byte[][] listData)
        {
            if (listData.Length < 1)
            {
                throw new BeamApiException("Need at least 1 row listdata for header.");
            }

            var result = new List<T>();
            var headerList = Encoding.UTF8.GetString(listData[0]).Split('\u0001').ToList();
            var isCountColumnIncluded = headerList.Remove(Count);
            for (var i = 1; i < listData.Length; i++)
            {
                var rowData = Encoding.UTF8.GetString(listData[i]).Split('\u0001').ToList();
                if( isCountColumnIncluded && i == 1) //Remove first data row's count column
                    rowData.RemoveAt(rowData.Count - 1);
                if (headerList.Count != rowData.Count)
                {
                    throw new BeamApiException("The length of header and the length of data item are inconsistent");
                }
                result.Add(
                    _flattener.HydrateDtoFromBeamFields<T>(
                        headerList.Select((x, index) => Tuple.Create(x, rowData[index])).ToDictionary(x => x.Item1, x => x.Item2)));
            }

            return result;
        }

        //This method is invoked in CreateLambdaExpressionFuncForConvertingFieldsToInputDto through reflection
        //DO NOT REMOVE IT
        public Object GetDtoFromFields<T>(messageEnvelope messageEnvelope)
        {
            //If it is a list function, then need to convert data from the list
            if (messageEnvelope.control.function == nameof(BeamFunctionName.MUP))
            {
                return GetListFromListData<T>(messageEnvelope.listData);
            }
            else {
                return _flattener.HydrateDtoFromBeamFields<T>(messageEnvelope.fields?.Select(x => Tuple.Create(x.fieldName, x.value)).ToDictionary(x => x.Item1, x => x.Item2));
            }
        }


        public string ConvertObjectToString(object rawValue, Type type)
        {
            //If the type is nullable and the rawValue is null, then set the convertedStringValue as string empty
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                type = Nullable.GetUnderlyingType(type);
            }

            string convertedValue;
            if (type == typeof(DateTime))
            {
                var utcDateTime = DateTime.SpecifyKind(((DateTime)rawValue), DateTimeKind.Utc);
                convertedValue = utcDateTime.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK", CultureInfo.InvariantCulture);
            }
            else if (type == typeof(decimal))
                convertedValue = ((decimal)rawValue).ToString(CultureInfo.InvariantCulture);
            else if (type == typeof(double))
                convertedValue = ((double)rawValue).ToString(CultureInfo.InvariantCulture);
            else if (type == typeof(float))
                convertedValue = ((float)rawValue).ToString(CultureInfo.InvariantCulture);
            else if (type == typeof(Int16))
                convertedValue = ((Int16)rawValue).ToString(CultureInfo.InvariantCulture);
            else if (type == typeof(Int32))
                convertedValue = ((Int32)rawValue).ToString(CultureInfo.InvariantCulture);
            else if (type == typeof(Int64))
                convertedValue = ((Int64) rawValue).ToString(CultureInfo.InvariantCulture);
            else
                convertedValue = rawValue?.ToString();
            return convertedValue;
        }
    }
}