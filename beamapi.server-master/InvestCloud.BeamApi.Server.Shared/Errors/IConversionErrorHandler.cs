﻿using System;

namespace InvestCloud.BeamApi.Server.Shared.Errors
{
    public interface IConversionErrorHandler
    {
        void Handle(string fieldName, string value, Type expectedType);
    }
}