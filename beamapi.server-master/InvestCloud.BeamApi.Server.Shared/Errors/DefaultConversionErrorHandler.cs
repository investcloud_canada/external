﻿using System;
using InvestCloud.BeamApi.Server.Shared.Exception;

namespace InvestCloud.BeamApi.Server.Shared.Errors
{
    /// <summary>
    /// The default implementation simply throws a <see cref="BeamApiException"/> with a meaningful error message.
    /// </summary>
    public class DefaultConversionErrorHandler : IConversionErrorHandler
    {
        public void Handle(string fieldName, string value, Type expectedType)
        {
            throw new BeamApiException(
                $"Invalid value '{value}' for field '{fieldName}' of type '{expectedType.FullName}'.");
        }
    }
}