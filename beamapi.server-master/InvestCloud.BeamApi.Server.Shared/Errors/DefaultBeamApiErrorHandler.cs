﻿using System;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Errors
{
    /// <summary>
    /// This implementation simply sets the status to <see cref="MessageStatus.ApplicationError" />, and outputs the <see cref="Exception.Message" />
    /// to the <see cref="messageEnvelope.MainMessage"/> of the response
    /// </summary>
    public class DefaultBeamApiErrorHandler : IBeamApiErrorHandler
    {
        //private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void Handle(System.Exception ex, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.ApplicationError;
            response.MainMessage = $"Application Error: " + ex.Message;
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
            /*
            switch (ex)
            {
                case Exception x when x is BeamApiException|| x is PreconditionFailedException || x is TechnicalException /* || x is AutoMapperMappingException * /:
                {
                    var baseEx = ex.GetBaseException();
                    if (baseEx.GetType() == typeof(PreconditionFailedException))
                        x = baseEx;
                    
                    response.Status = (ushort)MessageStatus.UserError;
                    response.MainMessage = x.Message;
                        /*
                    if (x is RestrictionException)
                    {
                        response.MainMessage += $"{Environment.NewLine}{(x as RestrictionException).GetLogMessage()}"; ;
                    }
                    * /
                    break;
                }
                case ValidationErrorsException vex:
                    response.Status = (ushort)MessageStatus.ValidationError;
                    response.MainMessage = vex.GetErrorMessages().Aggregate((x, y) => $"{x},{y}");
                    break;
                default:
                    var loggedException = ex as TechnicalException ?? new TechnicalException("Unhandled web exception", ex);
                    // Logger.Error(loggedException.Id, ex);
                    response.Status = (ushort)MessageStatus.ApplicationError;
                    response.MainMessage = $"Application Error {loggedException.Id}";
                    break;
            }
            */
        }

    }
}