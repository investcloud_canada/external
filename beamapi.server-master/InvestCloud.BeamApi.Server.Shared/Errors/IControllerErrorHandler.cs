﻿using System;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Errors
{
    public interface IControllerErrorHandler
    {
        void Handle(System.Exception exception, string app, BeamFunctionName function, BeamFunctionVerb verb, string clientName);
    }
}