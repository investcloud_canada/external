﻿using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Errors
{
    public interface IBeamApiErrorHandler
    {
        void Handle(System.Exception ex, messageEnvelope response);
    }
}