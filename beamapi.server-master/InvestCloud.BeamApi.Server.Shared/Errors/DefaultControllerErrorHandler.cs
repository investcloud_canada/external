﻿using System;
using System.Runtime.ExceptionServices;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Errors
{
    /// <summary>
    /// This implementation simply throws the exception (using <see cref="ExceptionDispatchInfo"/>).
    /// </summary>
    public class DefaultControllerErrorHandler : IControllerErrorHandler
    {
        public void Handle(System.Exception exception, string app, BeamFunctionName function, BeamFunctionVerb verb, string clientName)
        {
            ExceptionDispatchInfo.Capture(exception).Throw();
        }
    }
}