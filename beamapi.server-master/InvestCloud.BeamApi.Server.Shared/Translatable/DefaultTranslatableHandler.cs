﻿using System;
using System.Collections.Generic;
using System.Reflection;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Translatable
{
    /// <summary>
    /// A default implementation which does nothing (no translations).
    /// </summary>
    public class DefaultTranslatableHandler : ITranslatableHandler
    {
        private static readonly IDictionary<string, PropertyInfo> TranslatableFieldsInternal = new Dictionary<string, PropertyInfo>();
        public IDictionary<string, PropertyInfo> TranslatableFields => TranslatableFieldsInternal;

        public string GetTranslation(Type requestType, Type responseType, messageEnvelope envelope)
        {
            return null;
        }

        public bool ShouldTranslate(string useTranslations, Type type, string propName)
        {
            return false;
        }

        public void MaybeAddUseTranslationsFieldName(IList<Type> requestTypes, Dictionary<string, string> res)
        {
        }
    }

}