﻿using System;

namespace InvestCloud.BeamApi.Server.Shared.Attributes
{
    public class TranslatableAttribute : Attribute
    {
        public string CorrespondsTo { get; set; }
    }
}