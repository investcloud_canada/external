﻿using System;
using System.Collections.Generic;
using System.Reflection;
using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared.Translatable
{
    public interface ITranslatableHandler
    {
        IDictionary<string, PropertyInfo> TranslatableFields { get; } 
        //const string UseTranslationsFieldName = "UseTranslations";
        string GetTranslation(Type requestType, Type responseType, messageEnvelope envelope);
        bool ShouldTranslate(string useTranslations, Type type, string propName);
        void MaybeAddUseTranslationsFieldName(IList<Type> requestTypes, Dictionary<string, string> res);
    }
}