﻿using BeamApi.Host;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InvestCloud.BeamApi.Server.Shared
{
    public interface IService
    {
        Task<call2Response> call2(call2Request request);
    }
}
