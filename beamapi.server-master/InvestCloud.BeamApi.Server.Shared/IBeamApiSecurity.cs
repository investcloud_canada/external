﻿using BeamApi.Host;

namespace InvestCloud.BeamApi.Server.Shared
{
    public interface IBeamApiSecurity
    {
        /// <summary>
        /// Set up the current user (and any other session requirements) at the start of the request,
        /// based on the <see cref="security.securitykey"/>.
        /// </summary>
        /// <param name="securityKey"></param>
        /// <param name="userId"></param>
        void RequestStart(string securityKey, string userId);
        /// <summary>
        /// Tear down the current user at the end of the current request.
        /// </summary>
        void RequestEnd();
    }
}