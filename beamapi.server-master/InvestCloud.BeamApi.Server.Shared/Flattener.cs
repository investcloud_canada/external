﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using InvestCloud.BeamApi.Server.Shared.Attributes;
using InvestCloud.BeamApi.Server.Shared.Errors;
using InvestCloud.BeamApi.Server.Shared.Exception;
using InvestCloud.BeamApi.Server.Shared.ExtensionMethods;
using InvestCloud.BeamApi.Server.Shared.Translatable;
using Tracs.BeamApi.Framework;

namespace InvestCloud.BeamApi.Server.Shared
{
    public class Flattener : IFlattener
    {
        private static readonly IDictionary<Type, IList<PropertyInfo>> TypeProperties = new Dictionary<Type, IList<PropertyInfo>>();
        private readonly ITranslatableHandler _translatableHandler;
        private readonly IConversionErrorHandler _conversionErrorHandler;

        // TODO abstract private static IDictionary<string, PropertyInfo> TranslatableFields = new Dictionary<string, PropertyInfo>();
        // public const string UseTranslationsFieldName = "UseTranslations";

        public Flattener(ITranslatableHandler translatableHandler, IConversionErrorHandler conversionErrorHandler)
        {
            _translatableHandler = translatableHandler;
            _conversionErrorHandler = conversionErrorHandler;
        }
		
        public Dictionary<string, string> GetFieldsForApf(Type t)
        {
            var fieldOptionsSetter = new FieldOptionsSetter<Tuple<string, string>>(
                originalInstance: null,
                fieldOptionsSettingName: null,
                fnToSetFieldOption: (originalInstance, fieldOptionsSettingName, optionFieldName, displayName) =>
                    //This fund is to create field option field like "FieldOption_Addresss_AddressLine2",
                    //if there is a corresponding optionfield dictionary in the original instance
                    Tuple.Create(
                        displayName,
                        "FieldOption Value"
                    )
            );
            var res = RecurseFieldsInfo(
                t,
                fnOnProperty:(prop, altProp, prefix, instance) =>
                    Tuple.Create(
                        prefix + prop.Name,
                        prop.GetCustomAttributes().OfType<DescriptionAttribute>().FirstOrDefault()!= null ? prop.GetCustomAttributes().OfType<DescriptionAttribute>().First().Description:""),
                useTranslations:null,
                prefix:"",
                mappedInstance:null,
                fieldOptionsSetter:fieldOptionsSetter
            );
 
            return res.ToDictionary(x => x.Item1, x => x.Item2);
        }

        public Dictionary<string, Tuple<object, Type>> GetFieldValuesForKey(Type t,
            object mappedInstance,
            string useTranslations,
            object originalInstance = null)
        {            
            var fieldOptionsSetter = new FieldOptionsSetter<Tuple<string, object, Type>>(
                originalInstance: originalInstance,
                fnToSetFieldOption: (originalInst, fieldOptionsSettingName, optionFieldName, displayName) =>
                {
                    //This func is to read corresponding fieldoption value from the dictionary
                    var optionField = ((Dictionary<string, FieldOption>) originalInst?.GetType()?.GetProperties()?
                        .SingleOrDefault(pi => pi.Name == fieldOptionsSettingName)?.GetValue(originalInst));
                    var optionFieldValue = string.Empty;
                    if (optionField != null && optionField.ContainsKey(optionFieldName))
                        optionFieldValue = (optionField)[optionFieldName].Name();
                    return Tuple.Create(
                        displayName,
                        optionFieldValue as Object,
                        typeof(System.String)
                    );
                }
            );
            var res = RecurseFieldsInfo(
                t,
                fnOnProperty: (prop, altProp, prefix, inst) =>
                {
                    var fieldName = $"{prefix}{prop.Name}";
                    return Tuple.Create(
                        fieldName,
                        inst != null ? altProp == null ? prop.GetValue(inst) : altProp.GetValue(inst) : null,
                        prop.PropertyType);
                },
                useTranslations:useTranslations,
                mappedInstance: mappedInstance,
                fieldOptionsSetter: fieldOptionsSetter
            );
            return res.ToDictionary(x => x.Item1, x => Tuple.Create(x.Item2, x.Item3));
        }

        public List<Dictionary<string, Tuple<object, Type>>> GetFieldValuesForLst<T>(
            Type t, 
            List<T> instances, 
            string useTranslations,
            object originalInstance = null) where T : new()
        {
            //Add first item - used as header item for the lst function 
            var defaultInstanceOfT = Activator.CreateInstance<T>();            
            
            var result = new List<Dictionary<string, Tuple<object, Type>>>() { GetFieldValuesForKey(t, defaultInstanceOfT, useTranslations, originalInstance) };
            
            if (instances.Any())
            {
                result.AddRange(instances.Select(x => GetFieldValuesForKey(t, x, useTranslations, originalInstance)).ToList());                
            }
                        
            return result;
        }        

        //ienumerable type property is ignored by design.
        private List<T> RecurseFieldsInfo<T>(
            Type t, 
            Func<PropertyInfo, PropertyInfo, string, object, T> fnOnProperty,
            string useTranslations,
            string prefix = "",            
            object mappedInstance = null, 
            FieldOptionsSetter<T> fieldOptionsSetter = null)
        {
            var res = new List<T>();
            var properties = GetTypeProperties(t);
            
            foreach (var prop in properties)
            {
                if (IsPrimitive(prop.PropertyType))
                {
                    if (_translatableHandler.ShouldTranslate(useTranslations, t, prop.Name))
                    {
                        res.Add(fnOnProperty(prop, _translatableHandler.TranslatableFields[GetTranslatableDictKey(t, prop.Name)], prefix, mappedInstance));
                    }
                    else
                        res.Add(fnOnProperty(prop, null, prefix, mappedInstance));

                    //If the field has field option set, then add extra field in api response.
                    var optionFieldName = prop.GetCustomAttributes().OfType<FieldOptionsAttribute>().FirstOrDefault()
                        ?.FieldOptionFieldName;
                    if(fieldOptionsSetter?.FieldOptionsSettingName == null)
                        fieldOptionsSetter.FieldOptionsSettingName = t.GetCustomAttributes().OfType<FieldOptionsAttribute>().FirstOrDefault()
                            ?.FieldOptionsSettingName;
                    if (!string.IsNullOrEmpty(optionFieldName) 
                        && fieldOptionsSetter.FnToSetFieldOption!= null
                        && fieldOptionsSetter.FieldOptionsSettingName!=null)
                    {
                        var displayName = $"FieldOption_{prefix}{prop.Name}";
                        res.Add(fieldOptionsSetter.FnToSetFieldOption(fieldOptionsSetter.OriginalInstance, fieldOptionsSetter.FieldOptionsSettingName, optionFieldName, displayName));
                    }
                }
                else if (IsPropertyAEnumerable(prop))
                {
                    continue; //skip the list type property
                }
                else
                {
                    var prefix2 = $"{prefix}{prop.Name}_";
                    //If the field has field option set, then add extra field in api response.
                    var fieldOptionsSettingNameOnProperty = prop.GetCustomAttributes().OfType<FieldOptionsAttribute>().FirstOrDefault()
                        ?.FieldOptionsSettingName;
                    res.AddRange(RecurseFieldsInfo(prop.PropertyType, fnOnProperty, useTranslations, prefix2, 
                        mappedInstance != null ? prop.GetValue(mappedInstance) : null, 
                        new FieldOptionsSetter<T>(fieldOptionsSetter.OriginalInstance, fieldOptionsSettingNameOnProperty, fieldOptionsSetter.FnToSetFieldOption)));
                }
            }
            //res.Add(Tuple.Create(countField.Name, countField.GetValue(instance), T));
            return res;
        }

        private IList<PropertyInfo> GetTypeProperties(Type type)
        {            
            if (!TypeProperties.ContainsKey(type))
            {
                var properties = type.GetProperties();
                TypeProperties.Add(type, properties);

                foreach (var prop in properties)
                {
                    var attribute = prop.GetCustomAttributes().OfType<TranslatableAttribute>().SingleOrDefault();
                    if (attribute != null)
                    {                        
                        _translatableHandler.TranslatableFields.Add(GetTranslatableDictKey(type,attribute.CorrespondsTo), prop);
                    }
                }
            }

            return TypeProperties[type];
        }

        private static string GetTranslatableDictKey(Type type, string propertyName)
        {
            return $"{type.FullName}.{propertyName}";
        }

        private static bool IsPropertyAEnumerable(PropertyInfo prop)
        {
            return (prop.PropertyType.IsGenericType 
                    && prop.PropertyType.GetGenericTypeDefinition().GetInterfaces()
                        .Any(x => x.IsGenericType &&x.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                   || typeof(IEnumerable).IsAssignableFrom(prop.PropertyType);
        }

        public T HydrateDtoFromBeamFields<T>(Dictionary<string, string> beamFields)
        {
            return (T)RecurseSetValue(typeof(T), beamFields);
        }

        private object RecurseSetValue(Type t, Dictionary<string, string> beamFields, string prefix = "")
        {
            var properties = t.GetProperties();
            object instance;
            if (t == typeof(String))
            {
                instance = default(string);
                return instance;
            }
            if ((beamFields == null || beamFields.Count == 0) && (t == typeof(Nullable) ||
                                               (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))))
            {
                return null;
            }
            else
            {
                instance = Activator.CreateInstance(t);
            }

            foreach (var prop in properties)
            {
                if (IsPrimitive(prop.PropertyType) && prop.CanWrite)
                {
                    var isNullableType = false;
                    var type = prop.PropertyType;
                    var conversionType = type;
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        conversionType = Nullable.GetUnderlyingType(type);
                        isNullableType = true;
                    }

                    var attribute = prop.GetCustomAttribute<MappingKeyAttribute>();

                    var fieldName = attribute == null ? $"{prefix}{prop.Name}" : $"{prefix}{attribute.Key}";

                    if (beamFields != null && beamFields.ContainsKey(fieldName))
                    {
                        var beamValue = beamFields[fieldName];
                        if (!string.IsNullOrEmpty(beamValue) && beamValue != null)
                        {
                            prop.SetValue(instance, ConvertStringToDataType(conversionType,beamValue, fieldName));
                        }
                    }
                    else //If the value is not provided then set it as default value
                    {
                        if (isNullableType)
                            prop.SetValue(instance, null);
                        else
                            prop.SetValue(instance, type == typeof(string) ? default(string) : Activator.CreateInstance(type));
                    }
                }
                else if (IsPropertyAEnumerable(prop))
                {
                    continue; //skip the list type property
                }
                else
                {
                    var prefix2 = (!string.IsNullOrEmpty(prefix) ? prefix : "") + prop.Name + "_";
                    var beamFields2 = beamFields?
                        .Where(kvp => kvp.Key.StartsWith(prefix2))
                        .ToDictionary(x => x.Key, x => x.Value);
                    if(prop.CanWrite)
                        prop.SetValue(instance, RecurseSetValue(prop.PropertyType, beamFields2, prefix2));
                }
            }
            return instance;
        }

        private static bool IsPrimitive(Type t)
        {
            return t.IsPrimitive 
                   || t.IsEnum
                   || t == typeof(Decimal) 
                   || t == typeof(string) 
                   || t == typeof(DateTime) 
                   || t == typeof(Guid)
                   || (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        public Object ConvertStringToDataType(Type type, string beamValue, string fieldName)
        {
            Object convertedValue = null;
            try
            {
                if(type == typeof(Guid))
                    convertedValue = new Guid(beamValue);
                else if (type == typeof(DateTime))
                {
                    DateTime tempDateTime;
                    if (!DateTime.TryParseExact(beamValue,
                       "yyyy-MM-ddTHH:mm:ssK",
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.AssumeUniversal,
                        out tempDateTime))
                    {
                        if(!DateTime.TryParseExact(beamValue,
                            "yyyy-MM-dd",
                            CultureInfo.InvariantCulture,
                            DateTimeStyles.AssumeUniversal,
                            out tempDateTime))
                        {
                            _conversionErrorHandler.Handle(fieldName, beamValue, type);
                            // TODO abstract throw new Exception($"VALIDATION_NOT_VALID_FOR_DATETIME_{fieldName.ToUpper()}");
                        }
                    }
                    
                    convertedValue = tempDateTime.ToUniversalTime();
                }
                else if (type.IsEnum)
                {
                    int n;
                    bool isNumeric = int.TryParse(beamValue, out n);
                    if (isNumeric)
                    {
                        convertedValue = Enum.Parse(type, beamValue);
                        // Enum.Parse has an odd quirk that if supplied a string containing a numeric value, where the numeric
                        // value doesn't correspond to a valid enum index, it will "invent" a new enum value of the supplied number.
                        // Therefore, the following check is required to catch this situation and raise an argument exception.
                        if (!type.GetEnumNames().Contains(Convert.ToString(convertedValue)))
                            _conversionErrorHandler.Handle(fieldName, beamValue, type);
                            //throw new Exception($"VALIDATION_NOT_VALID_FOR_ENUM_{fieldName.ToUpper()}");
                    }
                    else
                    {
                        var enumStringValues = beamValue.Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrEmpty(x));
                        if (Enum.GetNames(type).Any(x => enumStringValues.Contains(x)))
                        {
                            convertedValue = Enum.Parse(type, beamValue);
                        }
                        else
                        {
                            _conversionErrorHandler.Handle(fieldName, beamValue, type);
                            // throw new Exception($"VALIDATION_NOT_VALID_FOR_ENUM_{fieldName.ToUpper()}");
                        }

                    }
                   
                }
                else
                    convertedValue = Convert.ChangeType(beamValue, type);
            }
            catch (FormatException)
            {
                throw new BeamApiException($"FormatException, value '{beamValue}', field name '{fieldName}', expected type '{type.FullName}'");
            }
            catch (ArgumentNullException)
            {
                throw new BeamApiException($"ArgumentNullException, value '{beamValue}', field name '{fieldName}', expected type '{type.FullName}' cannot be null");
            }
            catch (OverflowException)
            {
                throw new BeamApiException($"OverflowException, value '{beamValue}', field name '{fieldName}', result in an OverflowException while convert to expected type '{type.FullName}'");
            }
            catch (ArgumentException)
            {
                throw new BeamApiException($"ArgumentException, value '{beamValue}', field name '{fieldName}', result in an ArgumentException while convert to expected type '{type.FullName}'");
            }
            return convertedValue;
        }
    }
}