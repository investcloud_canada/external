﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.AspNetCore.Http;

namespace InvestCloud.BeamApi.Server.NetCore
{
    /// <summary>
    /// Simple validation of the incoming request:
    /// <ul>
    /// <li>must match one of the paths specified in <see cref="IBeamApiOptions.Paths"/></li>
    /// <li>must be POST</li>
    /// <li>must be to a valid path</li>
    /// <li>must have the '' content-type header</li>
    /// </ul>
    /// </summary>
    public class BeamApiRequestValidatorMiddleware : BaseBeamApiMiddleware
    {
        private readonly RequestDelegate _next;

        public BeamApiRequestValidatorMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IBeamApiOptions options)
        {
            EnableAsyncIo(context);
            var requestPath = context.Request.Path.Value;
            if (!options.Paths.Any(x => requestPath.Equals(x, StringComparison.InvariantCultureIgnoreCase)))
            {
                await OutputValidationError(context, $"Invalid path '{context.Request.Path}'; supported paths are {string.Join(", ", options.Paths)}", StatusCodes.Status404NotFound);
                return;
            }

            if (context.Request.Method?.ToUpper() != "POST")
            {
                await OutputValidationError(context, $"Unexpected method {context.Request.Method}", StatusCodes.Status400BadRequest);
                return;
            }

            if (context.Request.ContentLength == 0)
            {
                await OutputValidationError(context, $"Missing request content (length {context.Request.ContentLength})", StatusCodes.Status400BadRequest);
                return;
            }

            await _next.Invoke(context);
        }

        private async Task OutputValidationError(HttpContext context, string message, int statusCode)
        {
            context.Response.StatusCode = statusCode;
            await context.Response.WriteAsync(message);
        }

    }

}
