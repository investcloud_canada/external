﻿using System.Threading.Tasks;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.AspNetCore.Http;

namespace InvestCloud.BeamApi.Server.NetCore
{
    /// <summary>
    /// A thin wrapper around <see cref="IBeamApiSecurity"/>. Guarantees that <see cref="IBeamApiSecurity.RequestEnd"/> is called,
    /// regardless of whether the underlying controller call was handled successfully or an exception was thrown.
    /// </summary>
    public class BeamApiSecurityMiddleware
    {
        private readonly RequestDelegate _next;

        public BeamApiSecurityMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IBeamApiSecurity beamApiSecurity)
        {
            var securityKey = (string) context.Items[Constants.BeamApiSessionSecurityKey];
            var userId = (string) context.Items[Constants.BeamApiSessionUserId];
           beamApiSecurity.RequestStart(securityKey, userId);
           try
           {
               await _next.Invoke(context);
           }
           finally
           {
               beamApiSecurity.RequestEnd();
           }
           
        }
    }
}