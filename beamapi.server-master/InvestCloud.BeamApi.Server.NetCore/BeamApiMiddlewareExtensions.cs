﻿using Microsoft.AspNetCore.Builder;

namespace InvestCloud.BeamApi.Server.NetCore
{
    public static class BeamApiMiddlewareExtensions
    {
        public static IApplicationBuilder UseBeamApiRequestParsingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BeamApiRequestParsingMiddleware>();
        }

        public static IApplicationBuilder UseBeamApiControllerInvokerMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BeamApiControllerInvokerMiddleware>();
        }

        public static IApplicationBuilder UseBeamApiSecurityMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BeamApiSecurityMiddleware>();
        }

        public static IApplicationBuilder UseBeamApiTimerMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BeamApiTimerMiddleware>();
        }

        public static IApplicationBuilder UseBeamApiUnhandledExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BeamApiUnhandledExceptionMiddleware>();
        }

        public static IApplicationBuilder UseBeamApiDatabaseTransactionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BeamApiDatabaseTransactionMiddleware>();
        }

        public static IApplicationBuilder UseBeamApiRequestValidatorMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BeamApiRequestValidatorMiddleware>();
        }

        public static IApplicationBuilder UseBeamApiRequestMiddleWare(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BeamApiRequestMiddleWare>();
        }

        /// <summary>
        /// Sets up all Beam Api middleware defined in this assembly in the correct order:
        /// <ol>
        /// <li><see cref="BeamApiUnhandledExceptionMiddleware"/></li>
        /// <li><see cref="BeamApiRequestValidatorMiddleware"/></li>
        /// <li><see cref="BeamApiTimerMiddleware"/></li>
        /// <li><see cref="BeamApiRequestParsingMiddleware"/></li>
        /// <li><see cref="BeamApiDatabaseTransactionMiddleware"/></li>
        /// <li><see cref="BeamApiSecurityMiddleware"/></li>
        /// <li><see cref="BeamApiControllerInvokerMiddleware"/></li>
        /// </ol>
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseAllBeamApiMiddleware(this IApplicationBuilder builder)
        {
            builder.UseBeamApiUnhandledExceptionMiddleware();
            builder.UseBeamApiRequestValidatorMiddleware();
            builder.UseBeamApiTimerMiddleware();
            builder.UseBeamApiRequestParsingMiddleware();
            builder.UseBeamApiDatabaseTransactionMiddleware();
            builder.UseBeamApiSecurityMiddleware();
            builder.UseBeamApiControllerInvokerMiddleware();
            return builder;
        }
    }
}