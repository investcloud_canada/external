using System;
using System.Threading.Tasks;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared;
using InvestCloud.BeamApi.Server.Shared.Exception;
using InvestCloud.BeamApi.Server.Shared.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace InvestCloud.BeamApi.Server.NetCore
{
    /// <summary>
    /// Performs the following tasks:
    /// <ul>
    /// <li>De-serialises the request and serialises the response (using <see cref="RequestResponseSerialization"/>)</li>
    /// <li>Adds the SecurityKey and the UserId from the request to the <see cref="HttpContext.Items"/> (using the <see cref="Constants.BeamApiSessionSecurityKey"/> and <see cref="Constants.BeamApiSessionUserId"/> keys respectively)</li>
    /// <li>Calls the next middleware in the chain (typically, authorisation / session handling).</li>
    /// </ul>
    /// </summary>
    public class BeamApiRequestParsingMiddleware : BaseBeamApiMiddleware
    {
        private readonly RequestDelegate _next;

        public BeamApiRequestParsingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IRequestResponseSerialization serialization)
        {
            EnableAsyncIo(context);

            call2Request request;
            // 1. parse request
            try
            {
                request = serialization.ParseRequest(context.Request.Body);
                context.Items[Constants.BeamApiHttpContextRequestKey] = request;
            }
            catch (Exception e)
            {
                throw new BeamApiException("Couldn't parse incoming SOAP message: " + e.Message, e);
            }

            context.Items[Constants.BeamApiSessionSecurityKey] = request.arg0.security.securitykey;
            context.Items[Constants.BeamApiSessionUserId] = request.arg0.security.userId;

            // 2. call service to handle the request
            await _next.Invoke(context);
            
            // persist the session (if required) & set the correct content type for the output
            // NCU POC - comment out session handling
            // Rplan.Framework.Dependencies.Dependencies.Container.GetInstance<IWebSessionProvider>().Save();
            context.Response.ContentType = "text/xml;charset=utf-8";
            
            // 3. serialize the response to the output stream
            try
            {
                var response = (call2Response) context.Items[Constants.BeamApiHttpContextResponseKey];
                serialization.SerializeResponse(response, context.Response.Body);
            }
            catch (Exception e)
            {
                throw new BeamApiException("Error serializing SOAP response: " + e.Message, e);
            }
        }
    }
}