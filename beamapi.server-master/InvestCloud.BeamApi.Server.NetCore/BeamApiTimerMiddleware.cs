﻿using System.Diagnostics;
using System.Threading.Tasks;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetCore
{
    /// <summary>
    /// Simple middleware to record the time taken to execute the request. The time is stored in <see cref="audit.timeInCall"/>,
    /// and logged using an <see cref="ILogger"/> (at <see cref="LogLevel.Trace"/>).
    /// </summary>
    public class BeamApiTimerMiddleware
    {
        private readonly RequestDelegate _next;
        private static ILogger _logger;

        public BeamApiTimerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, ILoggerFactory loggerFactory)
        {
            if (_logger == null)
                _logger = loggerFactory.CreateLogger(Constants.BeamApiLoggerName);
            
            
            var watch = new Stopwatch();
            watch.Start();
            try
            {
                await _next.Invoke(context);
            }
            finally
            {
                watch.Stop();
                call2Response response = null;
                if (context.Items.TryGetValue(Constants.BeamApiHttpContextResponseKey, out var resp))
                {
                    response = (call2Response) resp;
                    if (response.@return == null) response.@return = new messageEnvelope();
                    if (response.@return.audit == null) response.@return.audit = new audit();
                    response.@return.audit.timeInCall = (int) watch.ElapsedMilliseconds;
                    response.@return.audit.timeInCallSpecified = true;
                }
                if (_logger.IsEnabled(LogLevel.Trace))
                    _logger.LogTrace($"Time taken to execute {response?.@return?.control?.app}-{response?.@return?.control?.function}-{response?.@return?.control?.verb}: {watch.ElapsedMilliseconds}ms");
            }
        }
    }
}