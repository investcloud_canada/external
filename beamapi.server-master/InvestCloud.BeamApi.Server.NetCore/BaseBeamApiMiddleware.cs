﻿using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace InvestCloud.BeamApi.Server.NetCore
{
    public class BaseBeamApiMiddleware
    {

        protected void EnableAsyncIo(HttpContext context)
        {
            var syncIoFeature = context.Features.Get<IHttpBodyControlFeature>();
            if (syncIoFeature != null)
            {
                syncIoFeature.AllowSynchronousIO = true;
            }

        }
    }
}