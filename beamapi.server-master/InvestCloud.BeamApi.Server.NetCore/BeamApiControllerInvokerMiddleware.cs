﻿using System;
using System.Net;
using System.Threading.Tasks;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared;
using InvestCloud.BeamApi.Server.Shared.Exception;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetCore
{
    /// <summary>
    /// A thin .net Core Middleware wrapper around <see cref="Service"/>.
    /// </summary>
    public class BeamApiControllerInvokerMiddleware
    {
        private RequestDelegate _next;

        public BeamApiControllerInvokerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IService svc)
        {
            var request = (call2Request) context.Items[Constants.BeamApiHttpContextRequestKey];
            var response = await svc.call2(request);
            context.Items[Constants.BeamApiHttpContextResponseKey] = response;
        }
    }
}