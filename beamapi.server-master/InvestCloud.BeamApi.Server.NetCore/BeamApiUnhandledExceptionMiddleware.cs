﻿using System;
using System.Text;
using System.Threading.Tasks;
using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared;
using InvestCloud.BeamApi.Server.Shared.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace InvestCloud.BeamApi.Server.NetCore
{
    /// <summary>
    /// Simple handler for unhandled exceptions. This does the following:
    /// <ul>
    /// <li>catches any exception thrown by 'child' middleware</li>
    /// <li>builds up a short error message containing information about the call context (if available)</li>
    /// <li>logs the error (using <see cref="ILogger{TCategoryName}"/> with a key of <see cref="Constants.BeamApiLoggerName"/>)</li>
    /// <li>attempts to return a BeamApi response with this message</li>
    /// <li>if that fails, return the message as plain text (with a <see cref="StatusCodes.Status500InternalServerError"/>).</li>
    /// </ul>
    /// </summary>
    public class BeamApiUnhandledExceptionMiddleware : BaseBeamApiMiddleware
    {
        private static ILogger _logger;
        private readonly RequestDelegate _next;

        public BeamApiUnhandledExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, ILoggerFactory loggerFactory)
        {
            if (_logger == null)
                _logger = loggerFactory.CreateLogger(Constants.BeamApiLoggerName);
            EnableAsyncIo(context);
            
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception e)
            {
                var messageBuilder = new StringBuilder("Unhandled exception");
                // check whether the response exists in the context
                if (context.Items.TryGetValue(Constants.BeamApiHttpContextRequestKey, out var req))
                {
                    var request = (call2Request) req;
                    messageBuilder.Append( $" calling {request.arg0.control.app}-{request.arg0.control.function}-{request.arg0.control.verb}");
                }

                messageBuilder.Append(": ").Append(e.Message).Append(" See log for full detail.");
                var message = messageBuilder.ToString();
                _logger.LogError(message, e);
                try
                {
                    context.Response.ContentType = "text/xml;charset=utf-8";
                    // attempt to return a Beam API response
                    var response = new call2Response
                    {
                        @return = new messageEnvelope
                        {
                            Status = (ushort) MessageStatus.ApplicationError,
                            MainMessage = message
                        }
                    };
                    // not injected via DI to avoid errors being unreported if the DI is not set up correctly!
                    var serialization = new RequestResponseSerialization();
                    serialization.SerializeResponse(response, context.Response.Body);
                }
                catch (Exception exception)
                {
                    _logger.LogError("Failed to return a BeamApi formatted error message when handling the above error", exception);
                    // if failing, return a plain-text response
                    context.Response.StatusCode = (int) StatusCodes.Status500InternalServerError;
                    await context.Response.WriteAsync(message);
                }
            }
        }
    }
}