﻿// Decompiled with JetBrains decompiler
// Type: InvestCloud.BeamApi.Server.NetCore.BeamApiRequestMiddleWare
// Assembly: InvestCloud.BeamApi.Server.NetCore, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5FC9632D-A1CA-4429-A093-A2C73700DFF4
// Assembly location: C:\GreenTests\green-api-review\Class Libraries\InvestCloud.BeamApi.Server.NetCore.dll

using BeamApi.Host;
using InvestCloud.BeamApi.Server.Shared;
using InvestCloud.BeamApi.Server.Shared.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using System;
using System.Net;
using System.Threading.Tasks;

namespace InvestCloud.BeamApi.Server.NetCore
{
    public class BeamApiRequestMiddleWare
    {
        private RequestDelegate _next;

        public BeamApiRequestMiddleWare(RequestDelegate next) => this._next = next;

        public async Task Invoke(HttpContext context, IService svc)
        {
            IHttpBodyControlFeature syncIOFeature = context.Features.Get<IHttpBodyControlFeature>();
            if (syncIOFeature != null)
                syncIOFeature.AllowSynchronousIO = true;
            RequestResponseSerialization serialization = new RequestResponseSerialization();
            call2Request request;
            try
            {
                request = serialization.ParseRequest(context.Request.Body);
            }
            catch (Exception ex)
            {
                this.LogAndOutputError(context, HttpStatusCode.BadRequest, "Couldn't parse incoming SOAP message: " + ex.Message, ex);
                return;
            }
            call2Response response;
            try
            {
                response = await svc.call2(request);
            }
            catch (Exception ex)
            {
                this.LogAndOutputError(context, HttpStatusCode.InternalServerError, "Unhandled exception executing request " + request.arg0.control.app + "-" + request.arg0.control.function + "-" + request.arg0.control.verb + ": " + ex.Message, ex);
                return;
            }
            context.Response.ContentType = "text/xml;charset=utf-8";
            try
            {
                serialization.SerializeResponse(response, context.Response.Body);
            }
            catch (Exception ex)
            {
                this.LogAndOutputError(context, HttpStatusCode.InternalServerError, "Error serializing response for " + request.arg0.control.app + "-" + request.arg0.control.function + "-" + request.arg0.control.verb + ": " + ex.Message, ex);
            }
        }

        private void LogAndOutputError(
          HttpContext context,
          HttpStatusCode statusCode,
          string message,
          Exception e)
        {
            context.Response.StatusCode = (int)statusCode;
            context.Response.WriteAsync(message);
        }
    }
}
