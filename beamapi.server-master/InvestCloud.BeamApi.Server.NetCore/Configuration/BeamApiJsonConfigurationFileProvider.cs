﻿using System.IO;
using InvestCloud.BeamApi.Server.Shared.Configuration;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;

namespace InvestCloud.BeamApi.Server.NetCore.Configuration
{
    /// <summary>
    /// Loads a file called 'beamapi_config.json' in the root of the site
    /// </summary>
    public class BeamApiJsonConfigurationFileProvider : IBeamApiJsonConfigurationFileProvider
    {
        private readonly IWebHostEnvironment _config;

        public BeamApiJsonConfigurationFileProvider(IWebHostEnvironment config) 
        {
            _config = config;
        }

        public BeamServiceMappingConfig GetConfiguration()
        {
            var jsonConfigText = File.ReadAllText(_config.ContentRootFileProvider.GetFileInfo("beamapi_config.json").PhysicalPath);
            return JsonConvert.DeserializeObject<BeamServiceMappingConfig>(jsonConfigText);
        }
    }
}