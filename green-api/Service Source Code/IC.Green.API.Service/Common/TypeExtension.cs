﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using IC.Green.API.Messages;

namespace IC.Green.API.Service.Common
{
    public static class TypeExtensions
    {
        /// <summary>
        /// Execute public static method if exists
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static T ExecuteMethod<T>(this Type type, string name, object[] parameters) where T : class
        {
            var mi = type.GetMethod(name, BindingFlags.Static | BindingFlags.Public);
            if (mi != null)
            {
                return mi.Invoke(null, parameters) as T;
            }
            return default(T);
        }


        public static T GetField<T>(this Type type, string name) where T : class
        {

            FieldInfo fieldInfo = type.GetField(name, BindingFlags.Static | BindingFlags.Public);
            if (name == "Fields")
                fieldInfo.SetValue(type, GetFields(type));
            if (fieldInfo == null) return default(T);
            return fieldInfo.GetValue(null) as T;
        }

        public static List<BeamField> GetBeamFields(this Type type, string name)
        {
            FieldInfo fieldInfo = type.GetField(name, BindingFlags.Static | BindingFlags.Public);
            if (fieldInfo == null) //check if the method is overriden
            {
                return GetFields(type);
            }

            return fieldInfo.GetValue(null) as List<BeamField>;
        }

        public static List<BeamField> GetFields(Type T)
        {
            List<BeamField> lstFileds = new List<BeamField>();
            string assemblyName = "IC.Green.Business.DTO.dll";
            Assembly assembly = Assembly.LoadFrom(GetClassLibraryPath() + assemblyName);
            
            Type thisObject = assembly.GetType("IC.Green.Business.DTO." + T.Name.Replace("Handler", string.Empty) + "Detail", false);

            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(thisObject))
            {
                lstFileds.Add(new BeamField(propertyDescriptor.Name, propertyDescriptor.PropertyType.GetType()));
            }
            return lstFileds;
        }

        /// <summary>
        /// This Method used to get Classlibrarypath
        /// </summary>
        /// <returns></returns>
        public static string GetClassLibraryPath()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\";
        }
    }
}