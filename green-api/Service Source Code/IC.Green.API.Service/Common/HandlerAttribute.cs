﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IC.Green.API.Service.Common
{

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class HandlerAttribute : Attribute
    {
        public string Name { get; private set; }
        public bool UserSpecific { get; private set; }
        public bool SessionSpecific { get; private set; }
        public bool Stateless { get; private set; }
        /// <summary>
        /// Add OData filters+pagination to the service
        /// </summary>
        public bool Pagination { get; private set; }
        public bool Cacheable { get; private set; }
        public bool EnableSource { get; private set; }

        public HandlerAttribute(string name, bool userSpecific = false, bool sessionSpecific = false, bool stateless = false, bool pagination = false, bool cacheable = true, bool enablesource = false)
        {
            Name = name;
            UserSpecific = userSpecific;
            SessionSpecific = sessionSpecific;
            Stateless = stateless;
            Pagination = pagination;
            Cacheable = cacheable;
            EnableSource = enablesource;
        }
    }
}
