﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Oracle.ManagedDataAccess.Client;

namespace IC.Green.API.Service.Common
{
    public class OrangeSecurity 
    {
        #region Constants

        public string ActiveStatus = "A";

        #endregion Constants
        readonly OrangeConnection _objConnection;
        public OrangeSecurity()
        {

        }
        public OrangeSecurity(OrangeConnection objConnection)
        {
            _objConnection = objConnection;
        }

        public bool IsValidKey(string securityKey)
        {
            bool isValidKey = false;
            using OracleConnection con = new OracleConnection(OrangeConnection.ConnectionString);
            using OracleCommand cmd = con.CreateCommand();
            con.Open();
            cmd.CommandText = "Select USERID From SecSession Where SecurityKey=:securityKey And Status =:status";
            cmd.Parameters.Add(new OracleParameter("securityKey", securityKey));
            cmd.Parameters.Add(new OracleParameter("status", ActiveStatus));
            OracleDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                isValidKey = true;
            }
            return isValidKey;
        }
    }
}
