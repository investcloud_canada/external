﻿using Akka.Actor;
using Akka.Event;
using BeamApi.Host;
using IC.Cluster.Common;
using IC.Cluster.Messages;
using IC.Green.API.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using IC.Green.Business.DTO.Security;
using InvestCloud.BeamApi.Server.Shared.Configuration;

namespace IC.Green.API.Service.Common
{
    public class SecureHandler : BaseHandler
    { 
        protected SecurityToken SecurityToken = null;
        //protected static string Host = Factory.GetAkkaConfig().GetString("navy.api.idr.beam-api-host");

        public SecureHandler(IActorRef api, HandlerArgs args = null)
            : base( api, args)
        {
        }

        public static void CheckSecurity(BeamRequest brequest, ILoggingAdapter log)
        {
            if (new OrangeSecurity().IsValidKey(brequest.Message.security.securitykey))
            {
                SecurityRequest objSecurityRequest = new SecurityRequest(brequest.Message.security.securitykey,
                    brequest.Message.security.userId);

                var response = API.Ask<SecurityResponse>(objSecurityRequest, TimeSpan.FromSeconds(60)).Result;
                brequest.SessionToken = response?.GreenAuthenticationToken;
            }

            return;
        }
    }
}