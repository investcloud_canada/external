﻿using Akka.Actor;
using Akka.Event;
//using App.Metrics;
using BeamApi.Host;
using IC.Cluster.Common;

using IC.Cluster.Messages;
using IC.Green.API.Messages;
using Microsoft.AspNet.OData.Builder;
using Microsoft.OData.Edm;
using Microsoft.OData.UriParser;
using Microsoft.OData.UriParser.Aggregation;
using System;
//using domain = IC.Navy.Domain.Messages;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using InvestCloud.BeamApi.Server.Shared;
using Newtonsoft.Json;

namespace IC.Green.API.Service.Common
{
    public abstract class BaseHandler : ReceiveActor, IDisposable
    {
        public const string VERSION = "v1.0.0";
        private const string CountColumn = "__count";
        protected static string QuerySource;
        public static IActorRef API;
        protected readonly HandlerArgs HandlerArgs;
        protected double Timeout;
        protected TimeSpan InternalTimeout;
        protected static string RCCY = "ReportingCCY";
        protected static string BCCY = "BookCCY";
        protected IActorRef Mediator;
        protected IActorRef Requestor;
        protected BeamRequest Request;
        //protected IMetricsRoot metrics;
        //protected ITracer tracer;

        protected ILoggingAdapter Log = Context.GetLogger();
        
        public static readonly List<fieldInfo> ODataFilters = new List<fieldInfo>()
        {
            new fieldInfo { fieldName = "$top" },
            new fieldInfo { fieldName = "$skip" },
            new fieldInfo { fieldName = "$filter" },
            new fieldInfo { fieldName = "$apply" },
            new fieldInfo { fieldName = "$orderby" },
            new fieldInfo { fieldName = "FlagPagingMode", enrichment = "Y/N" }
        };

        public static readonly List<fieldInfo> SecurityFields = new List<fieldInfo>()
        {
            new fieldInfo { fieldName = "SessionKey", enrichment = "SessionKey from Core.Session" },
            new fieldInfo { fieldName = "UserName" },
        };

        public BaseHandler(IActorRef api, HandlerArgs args = null)
        {
            //metrics = this.UseMetrics();
            //tracer = this.UseTracing();
             API = api;
            HandlerArgs = args;
            //Mediator = api; //tell a Publish message to API will forward it to Mediator
        }

        public void DefaultReceive()
        {
            Receive<BeamRequest>(request => ReceiveBeamRequest(request));
            Receive<ReceiveTimeout>(t => ReceiveReceiveTimeout(t));
            ReceiveAny(x => { /* Ignore it */ });
        }

        Stopwatch Watch;

        protected void ReceiveBeamRequest(BeamRequest request)
        {
            Log.Info("Request for {0} {1}-{2}", request.MessageId, request.Message.control.app, request.Message.control.function);

            Watch = Stopwatch.StartNew();

            Requestor = Sender;
            Request = request;
            messageEnvelope response = HandleBeamRequest(request);
            SendResponse(response);
        }

        protected void SendResponse(messageEnvelope response)
        {
            Requestor.Tell(response);

            Log.Info("Response for {0} {1}-{2} {3}", Request.MessageId, Request.Message.control.app, Request.Message.control.function,
                string.IsNullOrEmpty(response.MainMessage) ? "OK" : "ERROR: " + response.MainMessage);
            Watch.Stop();

            /*if (Request.SecurityToken != null)
            {
                var tags = new MetricTags(new[] { "handler", "function", "tenant" }, new[] { Request.Message.control?.app, Request.Message.control?.function, Request.SecurityToken.DataStore });
                metrics?.SetRequest(tags);
                metrics?.HandlerTimer(tags, Watch.ElapsedMilliseconds);
            }*/
        }

        protected virtual void ReceiveReceiveTimeout(ReceiveTimeout t)
        {
            //Log.Info("{0} - No messages, shutting down.", Self.Path.Name);
            Context.SetReceiveTimeout(null);
            Context.System.Scheduler.ScheduleTellOnce(TimeSpan.FromSeconds(2), Self, PoisonPill.Instance, Self);
        }

        protected Task<T> AskFor<T>(IMessage m)
        {
            try
            {
                return API.Ask<T>(m, InternalTimeout);
            }
            catch (Exception ex)
            {
                Log.Error("{0} {1} {2}", m.MessageId, m.GetType().Name, ex.ToString());
                return Task.FromResult(default(T));
            }
        }

        /*
        protected Task<domain.SearchResult<T>> AskLookup<T>(domain.Lookup m, int? streamSize = null) where T : domain.BaseMessage
        {
            try
            {
                m.AllowStreaming = streamSize.HasValue && streamSize.Value > 0;
                m.Tags = new Dictionary<string, List<string>>();
                m.Tags["StreamSize"] = new List<string>() { streamSize.HasValue ? streamSize.ToString() : "0" };
                var result = API.Ask<domain.SearchResult<IMessage>>(m).Result;
                var r = new domain.SearchResult<T>()
                {
                    MessageId = result.MessageId,
                    SecurityToken = result.SecurityToken,
                    Entities = result.Entities.Select(e => e as T).ToList(),
                };

                return Task.FromResult(r);
            }
            catch (Exception ex)
            {
                Log.Error("{0} {1} {2}", m.MessageId, m.GetType().Name, ex.ToString());
                return Task.FromResult(new domain.SearchResult<T>());
            }
        }*/

        protected override void PreStart()
        {
            var noTimeout = HandlerArgs != null && HandlerArgs.NoTimeouts;
            if (!noTimeout)
            {

                var ha = GetType().GetCustomAttributes(typeof(HandlerAttribute), false).Cast<HandlerAttribute>().FirstOrDefault();
                if (ha != null)
                {
                    Timeout = 300;// config.GetDouble("navy.api.cache.timeout" + ha.Name);
                }
                if (Timeout == 0)
                {
                    Timeout = 300;// config.GetDouble("navy.api.cache.timeout");
                }
                Context.SetReceiveTimeout(TimeSpan.FromSeconds(Timeout));

                InternalTimeout = TimeSpan.FromSeconds(300 /*config.GetDouble("navy.api.cache.internaltimeout")*/);
            }

            base.PreStart();
        }

        protected override void PreRestart(Exception reason, object message)
        {
            var m = message as BeamRequest;
            if (m == null)
            {
                Sender.Tell(new Messages.BaseResult() { Cause = reason.Message });
            }
            else
            {
                Sender.Tell(new messageEnvelope()
                {
                    Status = (ushort)MessageStatus.ApplicationError,
                    control = m.Message.control,
                    security = m.Message.security,
                    listData = m.Message.listData,
                    MainMessage = reason.ToString(), // reason.AsMainMessage(),
                });
            }

            base.PreRestart(reason, message);
        }

        //protected override void PostStop()
        //{
        //    Log.Info("Stopping {0}", Self.Path);
        //    base.PostStop();
        //}

        public class DataEntity
        {
            [Key]
            public string Id { get; set; }

            public IDictionary<string, object> Properties { get; set; }
        }

        protected static string ODataFilterToDataTableExpression(FilterClause filter)
        {
            var sb = new StringBuilder();
            if (filter != null)
            {
                ParseNodes(filter.Expression, sb, false);
            }
            return sb.ToString();
        }

        protected static string OdataFilterLikeToContain(string filter)
        {
            //TODO: "LIKE" syntax is not supported in Odata, this is a temporary hack to convert Like statements to Contains until
            //the SDK is updated to support contains.

            string result = filter;
            string op = "like";
            var index = filter.ToLower().IndexOf(op);

            if (index != -1)
            {
                string before = Regex.Match(filter.Substring(0, index), @"(\w*)\s*$").Groups[1].Value;
                string after = Regex.Match(filter.Substring(index + op.Length), @"^\s*'%(\w*)%'").Groups[1].Value;

                string contains = string.Format("contains({0},'{1}')", before, after);
                string like = string.Format("{0} like '%{1}%'", before, after);

                var str = Regex.Replace(result, like, contains, RegexOptions.IgnoreCase);
                result = OdataFilterLikeToContain(str);
            }
            return result;
        }

        //TODO - replace with Microsoft.AspNetCore.Odata
        
        protected static string OdataOrderbyToString(OrderByClause o)
        {
            if (o == null) return string.Empty;
            StringBuilder sb = new StringBuilder();

            if (o.Expression.Kind == QueryNodeKind.SingleValueOpenPropertyAccess)
            {
                var exp = o.Expression as SingleValueOpenPropertyAccessNode;
                var direction = o.Direction == OrderByDirection.Descending ? "desc" : string.Empty;
                sb.Append(exp.Name + " " + direction);
            }

            if (o.Expression.Kind == QueryNodeKind.SingleValueFunctionCall)
            {
                var exp = o.Expression as SingleValueFunctionCallNode;
                var p = exp.Parameters.FirstOrDefault();
                if (p != null)
                {
                    var direction = o.Direction == OrderByDirection.Descending ? "desc" : string.Empty;
                    sb.Append(((SingleValueOpenPropertyAccessNode)p).Name + " " + direction);
                }
            }

            if (o.ThenBy != null)
            {
                var thenby = OdataOrderbyToString(o.ThenBy);
                if (!string.IsNullOrEmpty(thenby)) sb.Append("," + thenby);
            }
            return sb.ToString();
        }
        

        protected class EqualityComparer<T> : IEqualityComparer<T>
        {
            public bool Equals(T x, T y)
            {
                IDictionary<string, object> xP = x as IDictionary<string, object>;
                IDictionary<string, object> yP = y as IDictionary<string, object>;

                if (xP.Count != yP.Count)
                    return false;
                if (xP.Keys.Except(yP.Keys).Any())
                    return false;
                if (yP.Keys.Except(xP.Keys).Any())
                    return false;
                foreach (var pair in xP)
                    if (pair.Value.Equals(yP[pair.Key]) == false)
                        return false;
                return true;
            }

            public int GetHashCode(T obj)
            {
                return obj.ToString().GetHashCode();
            }
        }

        protected static DataTable ParseApply(TransformationNode node, DataTable dt)
        {
            if (node.Kind == TransformationNodeKind.GroupBy)
            {
                var g = node as GroupByTransformationNode;
                var columns = g.GroupingProperties.Select(x => x.Name);

                var dt2 = new DataTable();

                foreach (var c in columns)
                {
                    var src = dt.Columns[c];
                    dt2.Columns.Add(new DataColumn(src.ColumnName, src.DataType));
                }

                var equalityComparer = new EqualityComparer<IDictionary<string, object>>();

                var q = dt.AsEnumerable()
                        .GroupBy(x => columns.ToDictionary(t => t, t => x[t]), equalityComparer);

                if (g.ChildTransformations.Kind == TransformationNodeKind.Aggregate)
                {
                    var a = g.ChildTransformations as AggregateTransformationNode;
                    var aggregates = new Dictionary<string, Tuple<string, string, string, Type>>();

                    foreach (var e in a.Expressions)
                    {
                        string name = "";
                        if (e.Expression.Kind == QueryNodeKind.SingleValueOpenPropertyAccess)
                        {
                            var p = e.Expression as SingleValueOpenPropertyAccessNode;
                            name = p.Name;
                        }
                        else if (e.Expression.Kind == QueryNodeKind.SingleValuePropertyAccess)
                        {
                            var p = e.Expression as SingleValuePropertyAccessNode;
                            name = p.Property.Name;
                        }

                        var method = e.Method.ToString();
                        var alias = e.Alias;
                        var expr = string.Format("{0}({1})", method, name);

                        aggregates[alias] = Tuple.Create(alias, name, expr, dt.Columns[name].DataType);

                        // Add new columns to destination table
                        var src = dt.Columns[name];
                        dt2.Columns.Add(new DataColumn(alias, src.DataType));
                    }

                    var rowData = q.Select(x =>
                    {
                        var t = x.CopyToDataTable();
                        foreach (var ag in aggregates)
                        {
                            var src = t.Columns[ag.Value.Item2];
                            t.Columns.Add(new DataColumn(ag.Value.Item1, src.DataType, ag.Value.Item3));
                        }
                        return t;
                    })
                        .SelectMany(x =>
                        {
                            List<object[]> rows = new List<object[]>();
                            foreach (DataRow r in x.Rows)
                            {
                                var newRow = new List<object>();
                                foreach (DataColumn c in dt2.Columns)
                                {
                                    newRow.Add(r[c.ColumnName]);
                                }

                                rows.Add(newRow.ToArray());
                                break; // Only first row
                            }
                            return rows;
                        });

                    foreach (var r in rowData)
                    {
                        dt2.Rows.Add(r);
                    }

                    return dt2;
                }
            }

            return dt;
        }

        protected static void ParseNodes(QueryNode node, StringBuilder sb, bool quoted)
        {
            if (node.Kind == QueryNodeKind.UnaryOperator)
            {
                var op = node as UnaryOperatorNode;
                if (op.OperatorKind == UnaryOperatorKind.Not)
                {
                    sb.Append(" NOT (");
                    ParseNodes(op.Operand, sb, quoted);
                    sb.Append(") ");
                }
            }
            else if (node.Kind == QueryNodeKind.BinaryOperator)
            {
                var op = node as BinaryOperatorNode;
                ParseNodes(op.Left, sb, quoted);
                bool notNull = true;
                if (op.Right.Kind == QueryNodeKind.Constant)
                {
                    var c = op.Right as ConstantNode;
                    if (c.Value == null)
                    {
                        switch (op.OperatorKind)
                        {
                            case BinaryOperatorKind.Equal: sb.Append(" IS "); break;
                            case BinaryOperatorKind.NotEqual: sb.Append(" IS NOT "); break;
                        }
                        sb.Append("NULL");
                        notNull = false;
                    }
                }

                if (notNull)
                {
                    switch (op.OperatorKind)
                    {
                        case BinaryOperatorKind.Equal: sb.Append(" = "); break;
                        case BinaryOperatorKind.NotEqual: sb.Append(" <> "); break;
                        case BinaryOperatorKind.LessThan: sb.Append(" < "); break;
                        case BinaryOperatorKind.LessThanOrEqual: sb.Append(" <= "); break;
                        case BinaryOperatorKind.GreaterThan: sb.Append(" > "); break;
                        case BinaryOperatorKind.GreaterThanOrEqual: sb.Append(" >= "); break;
                        case BinaryOperatorKind.Or: sb.Append(" OR "); break;
                        case BinaryOperatorKind.And: sb.Append(" AND "); break;
                    }
                    ParseNodes(op.Right, sb, quoted);
                }
            }
            else if (node.Kind == QueryNodeKind.Convert)
            {
                var cvt = node as ConvertNode;
                ParseNodes(cvt.Source, sb, quoted);
            }
            else if (node.Kind == QueryNodeKind.SingleValuePropertyAccess)
            {
                var prop = node as SingleValuePropertyAccessNode;
                sb.Append(prop.Property.Name);
            }
            else if (node.Kind == QueryNodeKind.SingleValueOpenPropertyAccess)
            {
                var prop = node as SingleValueOpenPropertyAccessNode;
                sb.Append(prop.Name);
            }
            else if (node.Kind == QueryNodeKind.Constant)
            {
                var c = node as ConstantNode;
                if (c.TypeReference == null)
                {
                    sb.Append("NULL");
                }
                else if (c.TypeReference.IsDate())
                {
                    sb.AppendFormat(CultureInfo.InvariantCulture.DateTimeFormat, "#{0}#", c.Value);
                }
                else if (c.TypeReference.IsDateTimeOffset())
                {
                    var dto = (DateTimeOffset)c.Value;
                    sb.AppendFormat(CultureInfo.InvariantCulture.DateTimeFormat, "#{0}#", dto.Date);
                }
                else if (c.TypeReference.IsString())
                {
                    // Dates can show up quoted so we have to parse them out.
                    DateTime dv;
                    if (c.Value != null && DateTime.TryParse(c.Value.ToString(), out dv))
                    {
                        sb.AppendFormat(CultureInfo.InvariantCulture.DateTimeFormat, "#{0}#", dv.Date);
                    }
                    else
                    {
                        sb.AppendFormat(quoted ? "{0}" : "'{0}'", c.Value);
                    }
                }
                else
                {
                    sb.Append(c.Value);
                }
            }
            else if (node.Kind == QueryNodeKind.SingleValueFunctionCall)
            {
                var f = node as SingleValueFunctionCallNode;
                if (f.Name == "contains")
                {
                    var p1 = f.Parameters.First();
                    var p2 = f.Parameters.Skip(1).First();

                    ParseNodes(p1, sb, quoted);
                    sb.AppendFormat(" LIKE '%");
                    ParseNodes(p2, sb, true);
                    sb.Append("%' ");
                }
                else if (f.Name == "startswith")
                {
                    var p1 = f.Parameters.First();
                    var p2 = f.Parameters.Skip(1).First();

                    ParseNodes(p1, sb, quoted);
                    sb.AppendFormat(" LIKE '");
                    ParseNodes(p2, sb, true);
                    sb.Append("%' ");
                }
                else if (f.Name == "endswith")
                {
                    var p1 = f.Parameters.First();
                    var p2 = f.Parameters.Skip(1).First();

                    ParseNodes(p1, sb, quoted);
                    sb.AppendFormat(" LIKE '%");
                    ParseNodes(p2, sb, true);
                    sb.Append("' ");
                }
                else if (f.Name == "tolower")
                {
                    var p = f.Parameters.First();
                    var sb2 = new StringBuilder();
                    ParseNodes(p, sb2, quoted);
                    sb.Append(sb2.ToString().ToLower());
                }
                else if (f.Name == "toupper")
                {
                    var p = f.Parameters.First();
                    var sb2 = new StringBuilder();
                    ParseNodes(p, sb2, quoted);
                    sb.Append(sb2.ToString().ToUpper());
                }
                else if (f.Name == "concat")
                {
                    var p1 = f.Parameters.First();
                    var p2 = f.Parameters.Skip(1).First();

                    ParseNodes(p1, sb, quoted);
                    ParseNodes(p2, sb, quoted);
                }
            }
        }

        protected void ApplyFilters(Dictionary<string, string> meta, DataTable table, string customFilters, messageEnvelope response, IEnumerable<string> columnsRemoved = null)
        {
            var filterField = meta.Get("$filter");
            var applyField = meta.Get("$apply", "");
            var orderbyField = meta.Get("$orderby", "");
            var skipField = meta.Get("$skip");
            var topField = meta.Get("$top");
            var paginate = meta.IsTrue("FlagPagingMode");
            //
            var builder = new ODataConventionModelBuilder();
            builder.EntitySet<DataEntity>("Entities");
            var model = builder.GetEdmModel();

            var sb = new StringBuilder("Entities?");

            if (!string.IsNullOrWhiteSpace(filterField)) sb.AppendFormat("&$filter={0}", OdataFilterLikeToContain(filterField));

            if (!string.IsNullOrWhiteSpace(applyField)) sb.AppendFormat("&$apply={0}", applyField);
            if (!string.IsNullOrWhiteSpace(orderbyField)) sb.AppendFormat("&$orderby={0}", orderbyField);


            var p = new ODataUriParser(model, new Uri(sb.ToString(), UriKind.Relative));

            var filter = p.ParseFilter();
            var apply = p.ParseApply();

            string odataFilters = ODataFilterToDataTableExpression(filter);

            var orderby = p.ParseOrderBy();
            orderbyField = OdataOrderbyToString(orderby);
            //Log.Info("FILTER BY '{0}' THEN BY '{1}' ORDER BY '{2}'", customFilters, odataFilters, orderbyField);

            // Filter & Sort
            DataView dv = table.AsDataView();
            if (!string.IsNullOrWhiteSpace(customFilters))
            {
                dv.RowFilter = customFilters;
                table = dv.ToTable();
            }
            
            IEnumerable<DataRow> rows = table.Select(odataFilters, orderbyField);

            // Group by & Aggregate
            if (apply != null && apply.Transformations.Count() > 0)
            {
                table = ParseApply(apply.Transformations.First(), rows.CopyToDataTable());
                rows = table.AsEnumerable();
            }
            //
           // IEnumerable<DataRow> rows = table.AsEnumerable();
            int count = rows.Count();

            if (paginate)
            {
                // Pagination
                if (!string.IsNullOrWhiteSpace(skipField)) rows = rows.Skip(int.Parse(skipField));

                if (!string.IsNullOrWhiteSpace(topField)) rows = rows.Take(int.Parse(topField));

                // Count
                if (!table.Columns.Contains(CountColumn)) table.Columns.Add(new DataColumn(CountColumn, typeof(int)));

                var first = rows.FirstOrDefault();
                if (first != null)
                {
                    first.SetField(CountColumn, count);
                }
            }

            // Handle removed columns
            foreach (string colName in columnsRemoved ?? Enumerable.Empty<string>())
            {
                if (table.Columns.Contains(colName))
                {
                    table.Columns.Remove(colName);
                }
            }

            response.listData = ToListData(this.GetType(), table, rows);
        }

        public virtual messageEnvelope HandleBeamRequest(BeamRequest br)
        {
            var request = br.Message;

            //var secure = SecurityChecked(br); //already was secured before coming here
            //Source is attached to the SecurityToken

            var response = new messageEnvelope()
            {
                Status = (ushort)MessageStatus.Success,
                control = request.control,
                security = request.security,
                listData = request.listData,
                fields = request.fields
            };

            try
            {
                switch (request.control.function)
                {
                    case "APF":
                        APF(request, response);
                        break;

                    case "LST":
                        LST(br, response);
                        break;

                    case "KEY":
                        KEY(br, response);
                        break;

                    case "FEN":
                        FEN(br, response);
                        break;

                    case "FVA":
                        FVA(br, response);
                        break;

                    case "WIT":
                        WIT(br, response);
                        break;

                    case "MUP":
                        MUP(br, response);
                        break;

                    case "TUP":
                        TUP(br, response);
                        break;

                    default:
                        response.Status = (ushort)MessageStatus.UserError;
                        response.MainMessage = "Function not implemented";
                        break;
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                response.Status = (ushort)MessageStatus.SystemError;
                response.MainMessage = e.ToString();// e.AsMainMessage();
            }

            return response;
        }

        protected virtual void APF(messageEnvelope request, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.UserError;
            response.MainMessage = "Function not implemented";
        }

        protected virtual void LST(BeamRequest request, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.UserError;
            response.MainMessage = "Function not implemented";
        }

        protected virtual void KEY(BeamRequest request, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.UserError;
            response.MainMessage = "Function not implemented";
        }

        protected virtual void TUP(BeamRequest request, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.UserError;
            response.MainMessage = "Function not implemented";
        }

        protected virtual void MUP(BeamRequest request, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.UserError;
            response.MainMessage = "Function not implemented";
        }

        protected virtual void FEN(BeamRequest request, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.UserError;
            response.MainMessage = "Function not implemented";
        }

        protected virtual void FVA(BeamRequest request, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.UserError;
            response.MainMessage = "Function not implemented";
        }

        protected virtual void WIT(BeamRequest request, messageEnvelope response)
        {
            response.Status = (ushort)MessageStatus.UserError;
            response.MainMessage = "Function not implemented";
        }

        protected virtual bool SecurityChecked(messageEnvelope request)
        {
            return true;
        }

        public static Dictionary<string, string> GetMeta(messageEnvelope request)
        {
            if (request == null || request.fields == null) return new Dictionary<string, string>();

            var meta = request.fields.ToDictionary(f => f.fieldName, f => f.value);
            if (!meta.ContainsKey("_PageSize"))
            {
                meta["_PageSize"] = "200";
            }
            return meta;
        }

        public static string GetFilters(Dictionary<string, string> meta, string customFilters)
        {
            var filterField = meta.Get("$filter");
            var applyField = meta.Get("$apply", "");
            var orderbyField = meta.Get("$orderby", "");
            var skipField = meta.Get("$skip");
            var topField = meta.Get("$top");
            var paginate = meta.IsTrue("FlagPagingMode");

            var builder = new ODataConventionModelBuilder();
            builder.EntitySet<DataEntity>("Entities");
            var model = builder.GetEdmModel();

            var sb = new StringBuilder("Entities?");
            if (!string.IsNullOrWhiteSpace(filterField))
            {
                sb.AppendFormat("&$filter={0}", OdataFilterLikeToContain(filterField));
            }

            //TODO:
            //if (!string.IsNullOrWhiteSpace(applyField))
            //{
            //    sb.AppendFormat("&$apply={0}", applyField);
            //}

            var p = new ODataUriParser(model, new Uri(sb.ToString(), UriKind.Relative));
            var filter = p.ParseFilter();
            string odataFilters = ODataFilterToDataTableExpression(filter);

            //Log.Info("FILTER BY '{0}' THEN BY '{1}' ORDER BY '{2}'", customFilters, odataFilters, orderbyField);

            // Filter & Sort
            var fs = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(customFilters))
            {
                fs.AppendFormat("({0})", customFilters);
            }
            if (!string.IsNullOrWhiteSpace(odataFilters))
            {
                if (fs.Length > 0) fs.Append(" AND ");
                fs.AppendFormat("({0})", odataFilters);
            }

            if (paginate && string.IsNullOrWhiteSpace(orderbyField)) orderbyField = "Id";

            if (!string.IsNullOrWhiteSpace(orderbyField))
            {
                if (fs.Length > 0) fs.Append(" ");
                fs.AppendFormat("ORDER BY {0}", orderbyField);
            }

            if (paginate)
            {
                // Pagination
                if (string.IsNullOrWhiteSpace(skipField)) skipField = "0";
                if (string.IsNullOrWhiteSpace(topField)) topField = "200";

                fs.AppendFormat(" OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY", skipField, topField);
            }

            return fs.ToString().Replace("#", "'"); //replace is for date filters
        }

        protected bool ValidField(messageEnvelope request, string fieldName, string regex = null)
        {
            var field = request.fields.FirstOrDefault(f => f.fieldName == fieldName);
            if (field == null) return false;

            if (string.IsNullOrEmpty(regex)) return !string.IsNullOrEmpty(field.value);

            return true;
        }

        protected static byte[][] EncodeListData(IEnumerable<string[]> listData)
        {
            return listData.Select(x => Encoding.UTF8.GetBytes(string.Join("\u0001", x))).ToArray();
        }

        protected static string[] DecodeListData(byte[] row)
        {
            return Encoding.UTF8.GetString(row).Split('\u0001');
        }

        public static DataTable FromListData(byte[][] listData)
        {
            if (listData == null || listData.Length == 0) return null;

            var dt = new DataTable();

            var header = listData.First();
            var columns = DecodeListData(header);
            var n = columns.Length;
            var rows = listData.Count();

            for (int i = 0; i < n; i++)
            {
                dt.Columns.Add(columns[i]);
            }

            for (int r = 1; r < rows; r++)
            {
                var row = DecodeListData(listData[r]);
                dt.Rows.Add(row);
            }

            return dt;
        }

        public static byte[][] ToListData(Type type, DataTable dt, IEnumerable<DataRow> rows = null, BeamRequest req = null)
        {
            var cdata = type.ExecuteMethod<DataTable>("GenerateCustomData", new object[] { req, dt });
            var table = cdata ?? dt;

            var listData = new List<string[]>();

            var list = new List<string>();
            foreach (DataColumn c in table.Columns)
            {
                list.Add(c.ColumnName);
            }
            listData.Add(list.ToArray());

            if (rows == null)
            {
                foreach (DataRow dr in table.Rows)
                {
                    listData.Add(ToStringArray(dr.ItemArray));
                    //listData.Add(dr.ItemArray.Select(i => i.ToString()).ToArray());
                }
            }
            else
            {
                foreach (DataRow dr in rows)
                {
                    listData.Add(ToStringArray(dr.ItemArray));
                    //listData.Add(dr.ItemArray.Select(i => i.ToString()).ToArray());
                }
            }

            return EncodeListData(listData);
        }

        private static string[] ToStringArray(object[] items)
        {
            return items.Select(i => i is DateTime ?
                        DateTime.SpecifyKind(((DateTime)i), DateTimeKind.Utc).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK", CultureInfo.InvariantCulture) :
                        i.ToString()).ToArray();
        }

        protected virtual List<string[]> TransformData(DataTable dt)
        {
            var listData = new List<string[]>();

            var list = new List<string>();
            foreach (DataColumn c in dt.Columns)
            {
                list.Add(c.ColumnName);
            }
            listData.Add(list.ToArray());

            foreach (DataRow dr in dt.Rows)
            {
                listData.Add(dr.ItemArray.Select(i => i.ToString()).ToArray());
            }

            return listData;
        }

        /// <summary>
        /// Override in each service handler if there are custom parameters to validate
        /// </summary>
        protected virtual bool CheckCustomParameters(messageEnvelope request, List<fieldInfo> errorFields, List<fieldInfo> errorMessages, List<BeamField> Fields, List<BeamField> Filters)
        {
            return true;
        }

        protected bool CheckParameters(messageEnvelope request, messageEnvelope response, List<BeamField> Fields, List<BeamField> Filters)
        {
            var requestFields = request.fields?.ToDictionary(x => x.fieldName) ?? new Dictionary<string, fieldInfo>();

            List<fieldInfo> fields = new List<fieldInfo>();
            List<fieldInfo> messages = new List<fieldInfo>();

            foreach (var field in Fields.Union(Filters))
            {
                fieldInfo fld = null;
                requestFields.TryGetValue(field.Info.fieldName, out fld);

                //check for mandatory fields
                if (field.Info.fieldState == (ushort)FieldState.Mandatory)
                {
                    if (fld == null || string.IsNullOrWhiteSpace(fld.value))
                    {
                        var error = "Required";
                        fields.Add(new fieldInfo() { fieldName = field.Info.fieldName, fieldError = error, fieldState = (ushort)FieldState.Error, forceEmpty = ' ' });
                        messages.Add(new fieldInfo() { fieldName = "INFO", value = error, fieldState = (ushort)FieldState.Unknown, forceEmpty = ' ' });
                        continue;
                    }
                }

                //check for allowed values only
                if (fld != null && field.AllowedValues != null && field.AllowedValues.Length > 0)
                {
                    if (!field.AllowedValues.Any(v => string.Equals(v, fld.value, StringComparison.OrdinalIgnoreCase)))
                    {
                        var error = "Value not allowed";
                        fields.Add(new fieldInfo() { fieldName = field.Info.fieldName, fieldError = error, fieldState = (ushort)FieldState.Error, forceEmpty = ' ' });
                        messages.Add(new fieldInfo() { fieldName = "INFO", value = error, fieldState = (ushort)FieldState.Unknown, forceEmpty = ' ' });
                        continue;
                    }
                }
            }

            bool valid = messages.Count == 0 && CheckCustomParameters(request, fields, messages, Fields, Filters);

            if (!valid)
            {
                response.fields = fields.ToArray();
                response.messages = messages.ToArray();
                response.MainMessage = "Invalid parameters";
                response.Status = (ushort)MessageStatus.ValidationError;
            }

            return valid;
        }
        /*
        protected void SetResponseStatus(Caller caller, messageEnvelope response)
        {
            if (caller.Status == StatusEnum.SUCCESS)
            {
                response.MainMessage = "";
                response.Status = (ushort)MessageStatus.Success;
            }
            else
            {
                response.MainMessage = caller.MainMessage;
                switch (caller.Status)
                {
                    case StatusEnum.SUCCESS:
                        response.Status = (ushort)MessageStatus.Success;
                        break;

                    case StatusEnum.APPLICATION_ERROR:
                        response.Status = (ushort)MessageStatus.ApplicationError;
                        break;

                    case StatusEnum.SUCCESS_MORE_DATA:
                        response.Status = (ushort)MessageStatus.SuccessThereMayBeMoreData;
                        break;

                    case StatusEnum.SUCCESS_NO_MORE_DATA:
                        response.Status = (ushort)MessageStatus.Success;
                        break;

                    case StatusEnum.USER_ERROR:
                        response.Status = (ushort)MessageStatus.UserError;
                        break;

                    case StatusEnum.SYSTEM_ERROR:
                        response.Status = (ushort)MessageStatus.SystemError;
                        break;

                    case StatusEnum.VALIDATION_ERROR:
                        response.Status = (ushort)MessageStatus.ValidationError;
                        break;

                    case StatusEnum.SUCCESS_WITH_INFO:
                        response.Status = (ushort)MessageStatus.SuccessWithInfo;
                        break;

                    case StatusEnum.SUCCESS_WITH_DATA_QUALITY_WARNING:
                        response.Status = (ushort)MessageStatus.SuccessWithDataQualityWarning;
                        break;
                }
            }
        }
        */
     
        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(-1, -1, x => Directive.Restart, true);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) { }
    }
}