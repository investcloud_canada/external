﻿using Akka.Actor;
using BeamApi.Host;
using IC.Green.API.Messages;
using IC.Green.API.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using IC.Green.Business.DTO.Common;


namespace IC.Green.API.Service.Handler
{
    public class DomainMappedHandler<T> : SecureHandler
    {
        public DomainMappedHandler(IActorRef api)
            : base(api)
        {
            DefaultReceive();
        }

        
        protected override void LST(BeamRequest request, messageEnvelope response)
        {
            var qryResult = ExecuteQueryRequest(request, response);

            DataTable table = CreateDataTable<T>(qryResult.Entities);
            response.listData = ToListData(this.GetType(), table, null);
            response.Status = (ushort)MessageStatus.Success;
            response.MainMessage = qryResult.Messages.LastOrDefault();
        }

        protected override void KEY(BeamRequest request, messageEnvelope response)
        {
            var qryResult = ExecuteQueryRequest(request, response);
            SetResult<T>(qryResult.Entities.FirstOrDefault(), response);
            response.listData = null;
            response.Status = (ushort)MessageStatus.Success;
            response.MainMessage = qryResult.Messages.LastOrDefault();
        }

        protected override void TUP(BeamRequest request, messageEnvelope response)
        {
            var responseMessage = ExecuteCommandRequest(request, response);
            response.MainMessage = responseMessage.Messages.LastOrDefault();
            //Add response Mapping code
        }

        protected DataTable CreateDataTable<O>(IList<O> entities)
        {
            Type type = typeof(O);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }
            if (entities !=null && entities.Count > 0)
            {
                foreach (O entity in entities)
                {
                    object[] values = new object[properties.Length];
                    for (int i = 0; i < properties.Length; i++)
                    {
                        values[i] = properties[i].GetValue(entity);
                    }

                    dataTable.Rows.Add(values);
                }
            }
            return dataTable;
        }

        protected void SetResult<O>(O item, messageEnvelope response)
        {
            Type type = typeof(O);
            var properties = type.GetProperties();
            response.fields = new fieldInfo[properties.Length];

            var values = new object[properties.Length];
            for (int i = 0; i < properties.Length; i++)
            {
                response.fields[i] = new fieldInfo
                { fieldName = properties[i].Name, value = Convert.ToString(properties[i].GetValue(item)) };
            }
        }
        protected DataTable CreateDataTable<O>(O item, messageEnvelope response)
        {
            Type type = typeof(O);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name,
                    Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            response.fields = new fieldInfo[properties.Length];

            var values = new object[properties.Length];
            for (int i = 0; i < properties.Length; i++)
            {
                response.fields[i] = new fieldInfo
                { fieldName = properties[i].Name, value = Convert.ToString(properties[i].GetValue(item)) };
            }
            return dataTable;
        }

        private QueryResult<T> ExecuteQueryRequest(BeamRequest request, messageEnvelope response)
        {
            Dictionary<string, string> dctInput = new Dictionary<string, string>();
            dctInput = request.Message.fields.ToDictionary(f => f.fieldName, f => f.value);
            QueryRequest objRequest = new QueryRequest(response.control.app, request.SessionToken, dctInput,
                ConvertOperationToMethodName(response.control.function));
            return API.Ask<QueryResult<T>>(objRequest, TimeSpan.FromSeconds(60)).Result;
        }

        private Result ExecuteCommandRequest(BeamRequest request, messageEnvelope response)
        {
            Dictionary<string, string> dctInput = new Dictionary<string, string>();
            dctInput = request.Message.fields.ToDictionary(f => f.fieldName, f => f.value);
            CommandRequest objRequest = new CommandRequest(response.control.app, request.SessionToken, dctInput,
                ConvertOperationToMethodName(response.control.function+ response.control.verb));
            
            return API.Ask<Result>(objRequest, TimeSpan.FromSeconds(60)).Result;
        }

        private string ConvertOperationToMethodName(string operationName)
        {
            switch (operationName.ToUpper())
            {
                case "KEY":
                    return "Key";
                case "LST":
                    return "List";
                case "TUPC":
                    return "Create";
                case "TUPU":
                    return "Update";
                case "TUPD":
                    return "Delete";
                default:
                    return null;
            }
        }
    }
}
