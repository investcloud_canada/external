﻿using System;
using System.Collections.Generic;
using System.Data;
using Akka.Actor;
using BeamApi.Host;
using IC.Green.API.Messages;
using IC.Green.API.Service.Common;
using IC.Green.Business.DTO;


namespace IC.Green.API.Service.Handler
{
    [Handler("PortfolioWrapper", stateless: true, pagination: false, cacheable: false, userSpecific: true, enablesource: true)]
    internal class PortfolioWrapperHandler : DomainMappedHandler<PortfolioWrapperDetail>
    {
        public PortfolioWrapperHandler(IActorRef api)
            : base(api)
        {  
        }

        /*public static readonly List<BeamField> Fields = new List<BeamField>()
        {
                 new BeamField("PortfolioWrapperId", typeof(string)),
                 new BeamField("PortfolioWrapperName", typeof(string)),
        };*/
    }
}