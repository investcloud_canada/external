﻿using Akka.Actor;
using IC.Green.API.Messages;
using IC.Green.API.Service.Common;
using IC.Green.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IC.Green.API.Service.Handler
{
    [Handler("PartyAgreementType", stateless: true, pagination: true, cacheable: false, userSpecific: true, enablesource: true)]
    internal class PartyAgreementTypeHandler : DomainMappedHandler<PartyAgreementTypeDetail>
    {
        public PartyAgreementTypeHandler(IActorRef api)
            : base(api)
        {
        }

    }
}
