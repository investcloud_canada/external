﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Configuration.Hocon;
using Akka.Event;
using IC.Cluster.Common;
using IC.Cluster.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using IC.Green.Business.DTO.Common;
using IC.Green.Business.DTO.Constants;
using IC.Green.Business.DTO.Security;


namespace IC.Green.API.Service
{
    public class GetVersion
    {
    }

    public class VersionInfo
    {
        public string Service { get; set; }
        public string Version { get; set; }
    }

    public class Core : ReceiveActor
    {
        private const String VERSION = "v1.0.0";
        private readonly ILoggingAdapter Log = Context.GetLogger();
        private IActorRef Mediator;
        private IActorRef DomainSystem;

        public Core()
        {
            Receive<GetVersion>(q =>
            {
                Sender.Tell(new VersionInfo() { Service = "API", Version = "1.1" });
            });

            Receive<SubscribeAck>(ack => { });
            
            Receive<SecurityRequest>(ExecuteRequest);
            Receive<QueryRequest>(ExecuteRequest);
            Receive<CommandRequest>(ExecuteRequest);

            ReceiveAny(x => Log.Warning("Unhandled message {0}", x));
        }

        private void ExecuteRequest(SecurityRequest objRequest)
        {
            Mediator = DistributedPubSub.Get(Context.System).Mediator;
            var objPublish = new Publish(PublishSubscribeTopics.DomainService, objRequest);
            var response = Mediator.Ask<SecurityResponse>(objPublish, TimeSpan.FromSeconds(60)).Result;
            Sender.Tell(response);
        }

        private void ExecuteRequest(QueryRequest objRequest)
        {
            Mediator = DistributedPubSub.Get(Context.System).Mediator;
            var objPublish = new Publish(PublishSubscribeTopics.DomainService, objRequest);
            var response = Mediator.Ask<ExecutionResult>(objPublish, TimeSpan.FromSeconds(60)).Result;
            Sender.Tell(response);
        }

        private void ExecuteRequest(CommandRequest objRequest)
        {
            Mediator = DistributedPubSub.Get(Context.System).Mediator;
            var objPublish = new Publish(PublishSubscribeTopics.DomainService, objRequest);
            var response = Mediator.Ask<ExecutionResult>(objPublish, TimeSpan.FromSeconds(60)).Result;
            Sender.Tell(response);
        }

     

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(-1, -1, x => Directive.Restart, true);
        }

        protected override void PreStart()
        {
            Mediator = DistributedPubSub.Get(Context.System).Mediator;

           // SystemActors.DomainSystem = Context.ActorOf(Props.Create<PublishTopicDomainActor>(Mediator, Topics.Domain), "DomainSystemRef");
            Mediator.Tell(new Subscribe(PublishSubscribeTopics.BeamApiService, Self));
            base.PreStart();
        }

        protected override void PostStop()
        {
            Mediator.Tell(new Unsubscribe(PublishSubscribeTopics.BeamApiService, Self));
            base.PostStop();
        }
    }
}