using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Spring.Context;
using Spring.Context.Support;
using IC.Cluster.Common;
using Akka.Actor;

namespace IC.Green.Domain
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllers();
            //services.AddControllers().AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            LaunchActorSystem();
            InitGreenDbSpringObjectFactory();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            
        }

        private void InitGreenDbSpringObjectFactory()
        {
            InitGreenDbProvider();

            IApplicationContext context = new XmlApplicationContext(
                new string[]
                {
                    "assembly://IC.Green.Config/IC.Green.Config/DomainServiceConfig.xml"
                });

            ContextRegistry.RegisterContext(context);
        }

        private void LaunchActorSystem()
        {
            var actorSystem = HostFactory.Launch();

            actorSystem.ActorOf(Props.Create(typeof(Core), null), "Core");
            Console.WriteLine($"{typeof(Core).Name} actor created");
        }

        private void InitGreenDbProvider()
        {
            Babelsystem.ConnectionString.DataBaseProviderFactory.DataBaseConnection =
                Configuration.GetConnectionString("dbConnection");
            ;
        }
    }
}
