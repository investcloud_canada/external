﻿using Akka.Actor;
using IC.Green.Domain.Interface;
using Spring.Context;
using Spring.Context.Support;
using Babelsystem.User.Security.BL;
using IC.Green.Business.DTO.Common;
using IC.Green.Business.DTO.Security;
using IC.Green.DataValidation.BL;
using System;
using IC.Green.Business.DTO.Constants;

namespace IC.Green.Domain
{
    public class Worker: ReceiveActor
    {
        private IApplicationContext _context;

        public Worker()
        {
            Receive<SecurityRequest>(ExecuteRequest);
            Receive<QueryRequest>(ExecuteRequest);
            Receive<CommandRequest>(ExecuteRequest);
        }

        private void ExecuteRequest(SecurityRequest objRequest)
        {
            var response = DataValidator.ExecuteSingleSignOnRequest(objRequest);
            Sender.Tell(response);

        }

        private void ExecuteRequest(QueryRequest objRequest)
        {
            var result=new ExecutionResult { };
            string responseResult = FunctionalDataAccessLogic.VerifyServiceRequest(objRequest.SessionToken, string.Empty,string.Empty, string.Empty);
            if (responseResult.Trim().ToUpper() == Babelsystem.Business.DTO.Base.BabelsysConstant.Success.Trim().ToUpper())
                result = DomainInterface.ProcessQueryRequest(objRequest);
            else
                result = new QueryResult<ExecutionResult> { MessageId = objRequest.MessageId, Messages = new String[] { responseResult }, ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail};
            
            Sender.Tell(result);
        }

        private void ExecuteRequest(CommandRequest objRequest)
        {
            var result = new ExecutionResult { };
            string responseResult = FunctionalDataAccessLogic.VerifyServiceRequest(objRequest.SessionToken, string.Empty, string.Empty, string.Empty);
            if (responseResult.Trim().ToUpper() == Babelsystem.Business.DTO.Base.BabelsysConstant.Success.Trim().ToUpper())
                result = DomainInterface.ProcessCommandRequest(objRequest);
            else
                result = new Result { MessageId = objRequest.MessageId, Messages = new String[] { responseResult }, ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail };

            Sender.Tell(result);
        }

        #region Interface

        private IDomainInterface _domainInterface;

        public IDomainInterface DomainInterface
        {
            get
            {
                if (_domainInterface == null)
                {
                    _context ??= ContextRegistry.GetContext();
                    _domainInterface = (IDomainInterface)_context["DomainInterface"];
                }

                return _domainInterface;
            }
        }

        ISecurityDataValidator _dataValidator;

        public ISecurityDataValidator DataValidator
        {
            get
            {
                if (_dataValidator == null)
                {
                    _context ??= ContextRegistry.GetContext();
                    _dataValidator = (ISecurityDataValidator) _context["DomainDataValidatorBusinessLogic"];
                }
                return _dataValidator;
            }

           
        }

        private Babelsystem.User.Security.BL.IFunctionalDataAccessLogic _functionalDataAccessLogic;

        public Babelsystem.User.Security.BL.IFunctionalDataAccessLogic FunctionalDataAccessLogic
        {
            get
            {
                if (_functionalDataAccessLogic == null)
                {
                    IApplicationContext context = ContextRegistry.GetContext();
                    _functionalDataAccessLogic =
                        (Babelsystem.User.Security.BL.IFunctionalDataAccessLogic)
                        context["FunctionalDataAccessBussinessLogic"];
                }

                return _functionalDataAccessLogic;
            }
            set { _functionalDataAccessLogic = value; }
        }

        #endregion Interface


    }
}
