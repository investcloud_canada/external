﻿using Akka.Actor;
using Akka.Event;
using Akka.Cluster.Tools.PublishSubscribe;
using System;
using System.Collections.Generic;
using IC.Cluster.Common;
using IC.Cluster.Messages;
using IC.Green.Business.DTO.Common;
using IC.Green.Business.DTO.Constants;
using IC.Green.Business.DTO.Security;

namespace IC.Green.Domain
{
    public class Core : ReceiveActor
    {
        private ILoggingAdapter Log = Context.GetLogger();
        private IActorRef _mediator;
        private IActorRef _workerProcess;
        private Dictionary<Guid, IActorRef> _dctWorkers = new Dictionary<Guid, IActorRef>();

        public Core()
        {
            Receive<ExecutionResult>(ExecuteResponse);
            Receive<SecurityRequest>(ExecuteRequest);
            Receive<QueryRequest>(ExecuteRequest);
            Receive<CommandRequest>(ExecuteRequest);
            Receive<SecurityResponse>(ExecuteResponse);
        }

        private void ExecuteRequest(SecurityRequest objRequest)
        {
            StoreRequestReference(objRequest.MessageId);
            _workerProcess.Tell(objRequest);
        }

        private void ExecuteRequest(QueryRequest objRequest)
        {
            StoreRequestReference(objRequest.MessageId);
            _workerProcess.Tell(objRequest);
        }
        private void ExecuteRequest(CommandRequest objRequest)
        {
            StoreRequestReference(objRequest.MessageId);
            _workerProcess.Tell(objRequest);
        }

        private void ExecuteResponse(SecurityResponse objResponse)
        {
            if (objResponse != null)
            {
                var actorRef = GetSenderReference(objResponse.MessageId);
                actorRef.Tell(objResponse);
            }
        }

        private void ExecuteResponse(ExecutionResult objExecutionResult)
        {
            if (objExecutionResult != null)
            {
                var actorRef = GetSenderReference(objExecutionResult.MessageId);
                actorRef.Tell(objExecutionResult);
            }
        }

        private IActorRef GetSenderReference(Guid gdMessageId)
        {
            return _dctWorkers[gdMessageId];
        }
        private void StoreRequestReference(Guid gdMessageId)
        {
            _dctWorkers[gdMessageId] = Sender;
        }
        
        protected override void PreStart()
        {
            Log.Info($"{Self.Path} created");
            _mediator = DistributedPubSub.Get(Context.System).Mediator;
            _mediator.Tell(new Subscribe(PublishSubscribeTopics.DomainService, Self));
            _workerProcess = Context.ActorOf<Worker>("worker");
            /*_workerProcess = Context.ActorOf(Props.Create(typeof(CoordinatorMasterActor<IMessage>), new WorkerProcess(),
                new List<double>() {0},
                TimeSpan.FromSeconds(60),
                TimeSpan.FromSeconds(60)));*/
            base.PreStart();
        }

        protected override void PostStop()
        {
            _mediator.Tell(new Unsubscribe(PublishSubscribeTopics.DomainService, Self));
            base.PostStop();
        }
    }
}
