﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;


namespace IC.Green.API.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class HealthCheckHandler
    {
        public HealthCheckHandler(RequestDelegate next)
        {

        }

        public async Task Invoke(HttpContext httpContext)
        {

            byte[] message;

            bool isConnected = true;

            httpContext.Response.StatusCode = isConnected == true ? StatusCodes.Status200OK : StatusCodes.Status500InternalServerError;

            message = isConnected == true ? Encoding.UTF8.GetBytes($"OK : {DateTime.UtcNow}") : Encoding.UTF8.GetBytes($"Internal Error : {DateTime.UtcNow}");

            await httpContext.Response.Body.WriteAsync(message);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class HealthCheckHandlerExtensions
    {
        public static IApplicationBuilder UseHealthCheckHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HealthCheckHandler>();
        }
    }
}
