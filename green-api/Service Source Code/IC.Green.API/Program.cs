using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace IC.Green.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)

                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                    if (System.Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") != Environments.Development)
                    {
                        var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                        string path = config.GetSection("Certificate")["Path"];
                        string keyPath = config.GetSection("Certificate")["SecretKey"];
                        webBuilder.UseStartup<Startup>().UseKestrel(options =>
                        {
                            options.ListenAnyIP(50141, listenOption =>
                            {
                                listenOption.UseHttps(path, keyPath);
                            });
                        });
                    }
                })
                .ConfigureWebHost(config =>
                {
                    if (System.Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Environments.Development)
                    {
                        config.UseUrls("http://*:50141/");
                    }

                })
                .UseWindowsService();


    }
}
