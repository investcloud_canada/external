﻿using System;
using System.Linq;

namespace IC.Green.API.Beam
{
    public static partial class ExceptionExtension
    {
        public static string AsMainMessage(this Exception ex)
        {
            if (ex == null) return null;

            var ae = ex as AggregateException;
            return ae == null ? ex.Message : string.Join("|", ae.Flatten().InnerExceptions.Select(ie => ie.Message));
        }
    }
}
