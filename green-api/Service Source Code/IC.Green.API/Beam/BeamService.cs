﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.DependencyInjection;
using BeamApi.Host;
using IC.Green.API.Messages;
using InvestCloud.BeamApi.Server.Shared;
using InvestCloud.BeamApi.Server.Shared.Configuration;
using InvestCloud.BeamApi.Server.Shared.DTO;
using InvestCloud.BeamApi.Server.Shared.Errors;
using InvestCloud.BeamApi.Server.Shared.ExtensionMethods;
using InvestCloud.BeamApi.Server.Shared.Translatable;
using Microsoft.Extensions.DependencyInjection;

namespace IC.Green.API.Beam
{
    public class BeamService: IService
    {
        private readonly IServiceSettingProvider _serviceSettingProvider;
        private readonly IServiceProvider _serviceCollection;
        private readonly IBeamApiErrorHandler _beamApiErrorHandler;
        private readonly IControllerErrorHandler _controllerErrorHandler;
        private readonly ITranslatableHandler _translatableHandler;
        private readonly IConverter _converter;

        private readonly IServiceScope _scope;
        private readonly IAkkaService _akkaService;
        private IActorRef _beamActor;

        public BeamService(IServiceSettingProvider serviceSettingProvider, IServiceProvider serviceCollection, IBeamApiErrorHandler beamApiErrorHandler, IControllerErrorHandler controllerErrorHandler, ITranslatableHandler translatableHandler, IConverter converter, IAkkaService akkaService)
        {
            this._serviceSettingProvider = serviceSettingProvider;
            this._serviceCollection = serviceCollection;
            _beamApiErrorHandler = beamApiErrorHandler;
            _controllerErrorHandler = controllerErrorHandler;
            _translatableHandler = translatableHandler;
            _converter = converter;
            _beamActor = akkaService.BeamActor;

            Console.WriteLine("Beamservice initiated");
        }

        public async Task<call2Response> call2(call2Request request)
        {
            var response = new messageEnvelope();

            var watch = new Stopwatch();
            watch.Start();
            try
            {
                var mid = Guid.NewGuid();
                try
                {
                    var req = new BeamRequest
                    {
                        MessageId = mid,
                        Message = request.arg0
                    };

                    response = await _beamActor.Ask<messageEnvelope>(req, TimeSpan.FromMinutes(0.5)); //TODO add timeout to config
                    response.Status = (ushort)MessageStatus.Success;
                }
                catch (System.Exception ex)
                {
                    while (ex.InnerException != null) ex = ex.InnerException;
                    _beamApiErrorHandler.Handle(ex, response);
                }
                finally
                {
                    bool flushSucceded = false;
                    try //Flush may still fail
                    {
                        flushSucceded = true;
                    }
                    finally
                    {
                        //Just in case clear clears everything pre flush too
                        if (!flushSucceded)
                        {
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _beamApiErrorHandler.Handle(ex, response);
            }
            finally
            {
                watch.Stop();
                response.audit = new audit { timeInCall = (int)watch.ElapsedMilliseconds };
                response.control = request.arg0.control;
                response.security = request.arg0.security;
            }

            return new call2Response(response);
        }
    }
}
