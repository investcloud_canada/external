﻿using Akka.Actor;
using Akka.Cluster.Tools.PublishSubscribe;
using Akka.Event;
using Akka.Routing;
using BeamApi.Host;
using IC.Cluster.Common;
using IC.Cluster.Messages;
using IC.Green.API.Beam;
using IC.Green.API.Messages;
using IC.Green.API.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using IC.Green.API.Service.Common;
using Microsoft.Extensions.DependencyInjection;

namespace IC.Green.API.Beam
{
    public class BeamActor : ReceiveActor
    {
        private readonly ILoggingAdapter Log = Context.GetLogger();
        private readonly Dictionary<string, HandlerInfo> Handlers = new Dictionary<string, HandlerInfo>();
        private IActorRef BeamAPF;
        private readonly IActorRef API;
        private IActorRef Mediator;
        //private static readonly string Host = Factory.GetAkkaConfig().GetString("navy.api.idr.beam-api-host");
        private static readonly string BeamMessageTopic = "beam.messages";
        private readonly IServiceScope _scope;
        private readonly ActorSystem ActorSys;

        public BeamActor(IServiceProvider sp)
        {
             _scope = sp.CreateScope();
            var akkaService = _scope.ServiceProvider.GetRequiredService<IAkkaService>();

            API = akkaService.CoreActor;
            ActorSys = akkaService.ActorSystem;
            //Become(Subscribed);
            Receive<BeamRequest>(req => ReceiveBeamRequest(req));

            ReceiveAny(x => Log.Warning("Unhandled message {0}", x));
        }
        
        /*
        private void Unsubscribed()
        {
            Receive<SubscribeAck>(s => Become(Subscribed));

           // Mediator = DistributedPubSub.Get(Context.System).Mediator;
           // Mediator.Tell(new Subscribe(BeamMessageTopic, Self));
        }

        private void Subscribed()
        {
            //Receive<Notification>(_ => LogCurrentStatus());
            Receive<BeamRequest>(req => ReceiveBeamRequest(req));
           Receive<BeamCacheMessage>(e =>
            {
                API.Forward(e);
                BeamCacheInvalidation.Forward(e);
            });
            ReceiveAny(e => {});
        }*/
    

        private void ReceiveBeamRequest(BeamRequest req)
        {
            try
            {  
                messageEnvelope m = req.Message;
                string handlerName = m.control?.app;
                HandlerInfo type = null;
                if (string.IsNullOrEmpty(handlerName) || !Handlers.TryGetValue(handlerName.ToLower(), out type))
                {
                    m.MainMessage = string.Format("Service '{0}' not implemented", handlerName);
                    Sender.Tell(m);
                    return;
                }

              /*  if (m.control.app == "Session")
                {
                    req.LegacyRequest = true;
                    SystemActors.SessionActor.Forward(req);
                    return;
                }

                if (type.Attribute.Stateless)
                {
                    //TODO: eventually move all the services here
                    BeamCache.Forward(req);
                    return;
                }
              */

                Log.Info("Got beam request {0} {1}-{2}", req.MessageId, m.control.app, m.control.function);
               // req.LegacyRequest = true;

                // Generate cache key if applicable
                string key = null;
                /*var prop = type.Type.GetProperty("ParamFields");
                if (prop != null)
                {
                    var fields = prop.GetValue(null) as List<string>;
                    key = GenerateKeyForParams(type, fields, m);
                }
                */
                //Moved Security check from APFActor

                HandlerInfo handlerType = null;
                Handlers.TryGetValue(handlerName.ToLower(), out handlerType);
                
                if (type.Type.IsSubclassOf(typeof(SecureHandler)))
                {
                    SecureHandler.API = API;
                    SecureHandler.CheckSecurity(req, Log);
                    var secured =  req.SessionToken != null;
                    if (!secured)
                    {
                        m.Status = (ushort)MessageStatus.UserError;
                        m.MainMessage = "This application requires a logged on user";

                        Log.Info("This application requires a logged on user {0} {1}-{2} {3}", req.MessageId, m.control.app, m.control.function);
                        Sender.Tell(m);
                        return;
                    }
                }
                
                if (m.control.function == "APF")
                {
                    BeamAPF.Forward(req);
                    return;
                }

                IActorRef handler;

                if (key == null)
                {
                    // handler = Context.ActorOf(Props.Create(() => Activator.CreateInstance(type.Type, API) as BaseHandler)); // TODO: assert
                    //var actorSystem = _sp.GetService<ActorSystem>();
                    //handler = actorSystem.ActorOf(Props.Create(() => Activator.CreateInstance(type.Type, API) as BaseHandler));

                   handler = ActorSys.ActorOf(Props.Create(() => Activator.CreateInstance(type.Type, API) as BaseHandler)); // TODO: assert


                    Log.Info("Creating handler {0}", handlerName, "test");
                }
                else
                {
                    handler = Context.Child(key);
                    if (handler == ActorRefs.Nobody)
                    {
                        Log.Info("Handler {0} for key {1} not found, creating new one", handlerName, key);
                        handler = Context.ActorOf(Props.Create(() => Activator.CreateInstance(type.Type) as BaseHandler), key); // TODO: assert
                    }
                }

                //Log.Info("Forwarding to handler {0} for key {1}", handlerName, key);
                 handler.Forward(req);
                //m.MainMessage = "this is a test";
                //Sender.Tell(m);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());

                messageEnvelope m = req.Message;
                m.MainMessage = ex.Message;
                Sender.Tell(m);
            }
        }

        /*
        private readonly Dictionary<string, string> UserTenantLookup = new Dictionary<string, string>();

        private string GetTenant(messageEnvelope request)
        {
            string tenant = null;

            if (request.control.function != "APF" && request.control.app != "Session")
            {
                if (!UserTenantLookup.TryGetValue(request.security.userId, out tenant))
                {
                    var caller = SecureHandler.CreateCaller(request, "Im.TenantSetting", Host);
                    caller.apf("I", new Dictionary<string, string>(), BaseHandler.VERSION);
                    if (caller.Status == BeamApi.StatusEnum.SUCCESS)
                    {
                        var data = caller.Results.ToDictionary(x => x.Key, x => x.Value.value);
                        caller.lst(data, BaseHandler.VERSION, BeamApi.ListDataCompletenessRequest.ReturnAllData);
                        if (caller.Status == BeamApi.StatusEnum.SUCCESS)
                        {
                            var spRow = caller.dataSet.Tables[0].Select("Name = 'Navy.SSOServiceProviderQualifiedName'").FirstOrDefault();
                            tenant = spRow?["TenantName"].ToString();
                            UserTenantLookup[request.security.userId] = tenant;
                            Log.Info("Tenant for {0} is {1}", request.security.userId, tenant);
                        }
                    }
                }
            }

            return tenant;
        }
        

        private void InitHandlers()
        {
            var root = typeof(BaseHandler);
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    foreach (var t in assembly.GetTypes().Where(t => root.IsAssignableFrom(t) && !t.IsInterface))
                    {
                        var has = t.GetCustomAttributes(typeof(HandlerAttribute), false).Cast<HandlerAttribute>();
                        if (has == null) continue;
                        foreach (var ha in has)
                        {
                            Handlers.Add(ha.Name.ToLower(), new HandlerInfo { Type = t, Attribute = ha });
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }
            }
        }
        
        internal string GenerateKeyForParams(HandlerInfo type, List<string> paramFields, messageEnvelope msg)
        {
            if (paramFields == null) return null;

            StringBuilder sb = new StringBuilder("|");

            if (type.Attribute.SessionSpecific)
            {
                sb.AppendFormat("_Session={0}|", msg.security.securitykey);    //cache per session
            }
            if (type.Attribute.UserSpecific)
            {
                sb.AppendFormat("_UserId={0}|", msg.security.userId);    //cache per user
            }
            sb.AppendFormat("_Tenant={0}|_Type={1}|", GetTenant(msg), type.Attribute.Name);    //always cache per tenant, service

            if (msg.fields != null)
            {
                foreach (var fld in paramFields)
                {
                    var f = msg.fields.FirstOrDefault(x => x.fieldName == fld);
                    if (f != null)
                    {
                        sb.AppendFormat("{0}={1}|", f.fieldName, f.value);
                    }
                }
            }

            var hash = Helper.GetMd5Hash(MD5.Create(), sb.ToString());

            //Log.Info("CacheKey: {0} Hash: {1}", sb.ToString(), hash);

            return hash;
        }

        private void LogCurrentStatus()
        {
            try
            {
                Log.Info("{0} up for {1:n} hours with {2:n}MB allocated",
                    Context.System.Name,
                    Context.System.Uptime.TotalHours,
                    Convert.ToDecimal(GC.GetTotalMemory(false)) / 1024m / 1024m);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
        */
        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(-1, -1, x => Directive.Restart, true);
        }

        protected override void PreStart()
        {
            InitHandlers();

            var args = new HandlerArgs { NoTimeouts = true };
            var propsb = Props.Create<BeamAPFActor>(Handlers, API);
            /*
                 .WithRouter(FromConfig.Instance
                 .WithSupervisorStrategy(new OneForOneStrategy(-1, -1, x => Directive.Restart, true)));*/
            BeamAPF = Context.ActorOf(propsb, "BeamAPF");
            

            /*
             SystemActors.SessionActor = Context.ActorOf(Props.Create<SessionHandler>(API, args), "Session");

             BeamCacheInvalidation = Context.ActorOf(Props.Create<BeamCacheInvalidationActor>(API), "CacheInvalidation");

             Context.ActorOf(Props.Create<BeamCachePrimerActor>(API), "CachePrimer");

             BeamCache = Context.ActorOf(Props.Create<BeamCacheActor>(API, Handlers), "BeamCache");

             BeamLog = Context.System.ActorOf<BeamLogActor>("BeamLog");

             SystemActors.BeamActor = Self;

             var props = Props.Create<MultiEntitySelectionHandler>(API, args)
                    .WithRouter(FromConfig.Instance
                    .WithSupervisorStrategy(new OneForOneStrategy(-1, -1, x => Directive.Restart, true)));
             SystemActors.MultiEntitySelection = Context.ActorOf(props, "MultiEntitySelection");

             var time = Factory.GetAkkaConfig().GetDouble("navy.api.beam.logstatus");

             Context.System.Scheduler.ScheduleTellRepeatedly(TimeSpan.FromSeconds(time), TimeSpan.FromSeconds(time), Self, new Notification(), Self);

             */

            base.PreStart();
        }

        private void InitHandlers()
        {
            var root = typeof(BaseHandler);
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    foreach (var t in assembly.GetTypes().Where(t => root.IsAssignableFrom(t) && !t.IsInterface))
                    {
                        var has = t.GetCustomAttributes(typeof(HandlerAttribute), false).Cast<HandlerAttribute>();
                        if (has == null) continue;
                        foreach (var ha in has)
                        {
                            Handlers.Add(ha.Name.ToLower(), new HandlerInfo { Type = t, Attribute = ha });
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }
            }
        }

        protected override void PostStop()
        {
            Log.Info("Beam Actor Stopped");
            _scope.Dispose();
            Mediator.Tell(new Unsubscribe(BeamMessageTopic, Self));
            base.PostStop();
        }

        public class HandlerInfo
        {
            public Type Type { get; set; }
            public HandlerAttribute Attribute { get; set; }
        }
    }
}