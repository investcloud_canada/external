﻿using Akka.Actor;
using Akka.Configuration;
using Akka.DependencyInjection;
using Akka.Routing;
using IC.Green.API.Beam;
using mdi = Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IC.Green.API.Service;
using IC.Cluster.Common;
using Microsoft.Extensions.DependencyInjection;
using InvestCloud.BeamApi.Server.Shared;
using ServiceProvider = Akka.DependencyInjection.ServiceProvider;

namespace IC.Green.API.Beam
{
    public interface IAkkaService
    {
        IActorRef BeamActor { get; }
        IActorRef CoreActor { get; }
        ActorSystem ActorSystem { get; }
    }

    public class AkkaService : IAkkaService, IHostedService
    {
        public IActorRef BeamActor { get; private set; }
        public IActorRef CoreActor { get; private set; }
        public ActorSystem ActorSystem { get; private set; }

        private readonly IServiceProvider _sp;

        public AkkaService(IServiceProvider sp)
        {
            _sp = sp;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var hocon = ConfigurationFactory.ParseString(await File.ReadAllTextAsync("akka.hocon", cancellationToken));
            var bootstrap = BootstrapSetup.Create().WithConfig(hocon);
            var di = ServiceProviderSetup.Create(_sp);
            var actorSystemSetup = bootstrap.And(di);

            ActorSystem = ActorSystem.Create(hocon.GetString("host.cluster-name"), actorSystemSetup);

            CoreActor = ActorSystem.ActorOf(Props.Create(typeof(Core), null), "Core");

            var beamProps = ServiceProvider.For(ActorSystem).Props<BeamActor>();
            BeamActor = ActorSystem.ActorOf(beamProps.WithRouter(FromConfig.Instance), "beam");

            await Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Cordinated Shutdown");
            await CoordinatedShutdown.Get(ActorSystem).Run(CoordinatedShutdown.ClrExitReason.Instance);
        }

    }
}
