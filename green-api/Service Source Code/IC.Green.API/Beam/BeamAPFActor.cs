﻿using Akka.Actor;
using Akka.Event;
using BeamApi.Host;
using IC.Green.API.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using IC.Green.API.Service.Common;
using static IC.Green.API.Beam.BeamActor;

namespace IC.Green.API.Beam
{
    public class BeamAPFActor : ReceiveActor
    {
        private readonly ILoggingAdapter Log = Context.GetLogger();
        private readonly Dictionary<string, HandlerInfo> Handlers;
        private readonly IActorRef API;
        public BeamAPFActor(Dictionary<string, HandlerInfo> handlers, IActorRef api)
        {
            Handlers = handlers;
            API = api;
            Receive<BeamRequest>(req => ReceiveBeamRequest(req));
            ReceiveAny(e => Log.Warning("Unhandled message {0}", e));
        }

        private void ReceiveBeamRequest(BeamRequest req)
        {
            messageEnvelope m = req.Message;
            string handlerName = m.control.app;

            //Log.Info("Got request {0} - {1} {2}", m.control.app, m.control.function, req.InternalRequest);

            var response = new messageEnvelope()
            {
                Status = (ushort)MessageStatus.Success,
                control = m.control,
                security = m.security,
                listData = m.listData,
                fields = m.fields
            };

            HandlerInfo type = null;
            Handlers.TryGetValue(handlerName.ToLower(), out type);
            try
            {
                var fields = type.Type.GetBeamFields("Fields") ?? new List<BeamField>();
                var filters = type.Type.GetField<List<BeamField>>("Filters") ?? new List<BeamField>();
                var odata = type.Attribute.Pagination ? BaseHandler.ODataFilters : new List<fieldInfo>();

                response.fields = fields.Select(x => x.Info).Union(filters.Select(x => x.Info)).Union(odata).ToArray();

            }
            catch (Exception ex)
            {
                response.Status = (ushort)MessageStatus.ApplicationError;
                response.MainMessage = ex.AsMainMessage();
            }

            Sender.Tell(response);
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(-1, -1, x => Directive.Restart, true);
        }

        protected override void PreStart()
        {
           // var config = Factory.GetAkkaConfig();
            base.PreStart();
        }
    }
}