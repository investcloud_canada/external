using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using IC.Green.API.Middleware;
using InvestCloud.BeamApi.Server.NetCore;
using InvestCloud.BeamApi.Server.Shared;
using InvestCloud.BeamApi.Server.Shared.Translatable;
using InvestCloud.BeamApi.Server.Shared.Errors;
using InvestCloud.BeamApi.Server.Shared.Configuration;
using InvestCloud.BeamApi.Server.NetCore.Configuration;
using System;
using System.Text;
using IC.Green.API.Beam;
using IC.Green.API.Service;
using IC.Green.API.Service.Common;
using Akka.Actor;
using IC.Cluster.Common;

namespace IC.Green.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
 
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ITranslatableHandler, DefaultTranslatableHandler>();
            services.AddSingleton<IControllerErrorHandler, DefaultControllerErrorHandler>();
            services.AddSingleton<IConversionErrorHandler, DefaultConversionErrorHandler>();
            services.AddSingleton<IBeamApiErrorHandler, DefaultBeamApiErrorHandler>();
            services.AddSingleton<IBeamApiJsonConfigurationFileProvider, BeamApiJsonConfigurationFileProvider>();
            services.AddSingleton<IFlattener, Flattener>();
            services.AddSingleton<IConverter, Converter>();
            services.AddSingleton<IServiceSettingProvider, ServiceSettingProvider>();
            services.AddSingleton<IAkkaService, AkkaService>();
            services.AddTransient<IService, BeamService>();

             OrangeConnection.ConnectionString = Configuration.GetSection("OrangeConnection:ConnectionString").Value;
            services.AddHostedService(sp => (AkkaService)sp.GetRequiredService<IAkkaService>());
            /*
            services.AddSingleton(_ => HostFactory.Launch());

            services.AddSingleton<Actorprovider.BeamActorProvider>(provider =>
            {
                var actorSystem = provider.GetService<ActorSystem>();
                var coreActor = actorSystem.ActorOf(Props.Create(typeof(Core), null), "Core");
                var beamActor = actorSystem.ActorOf(Props.Create(typeof(BeamActor), coreActor), "Beam");
                return () => beamActor;
            }); */

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
            }

            app.MapWhen(context => context.Request.Path.Value.Contains("/healthcheck"), appBuilder =>
            {
                appBuilder.UseHealthCheckHandler();
            });

            app.UsePathBase("/AcctService/Acct");

            //investcloud beamapi tracs handler

            app.UseBeamApiRequestMiddleWare();
        }
    }
}
