﻿using System;
using System.Collections.Generic;
using System.Text;
using BeamApi.Host;
using IC.Cluster.Messages;

namespace IC.Green.API.Messages
{
    public class BeamRequest : IMessage
    {
        public Guid MessageId { get; set; }
        public string UserAgent { get; set; }
        public string Host { get; set; }
        public string IPAddress { get; set; }
        public messageEnvelope Message { get; set; }
        public string CacheKey { get; set; }
        /// <summary>
        /// same as CacheKey but without the AsOfDate/BeAsOf component
        /// </summary>
        public string CallKey { get; set; }
      
        public SecurityToken SecurityToken { get; set; }

        public string SessionToken { get; set; }
        
    }
}
