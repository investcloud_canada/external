﻿using System;
using BeamApi.Host;
using IC.Cluster.Messages;

namespace IC.Green.API.Messages
{
    public class BaseMessage : GenericMessage { }

    public class BaseResult : BaseMessage, IResult
    {
        public bool Success { get { return string.IsNullOrEmpty(Cause); } }
        public string Cause { get; set; }
        public IMessage Request { get; set; }
    }

    public class HandlerArgs : BaseMessage
    {
        public bool NoTimeouts { get; set; }
    }
}
