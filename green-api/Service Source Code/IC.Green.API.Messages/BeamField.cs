﻿
using BeamApi.Host;
using System;

namespace IC.Green.API.Messages
{
    public class BeamField
    {
        public fieldInfo Info { get; set; }
        public Type Type { get; set; }
        public bool IsParam { get; set; }
        public string[] AllowedValues { get; set; }

        public string DomainMappedField { get; set; }

        public BeamField(string name, Type type, string enrichment = "", FieldState state = FieldState.Unknown, string[] allowedValues = null, bool isParam = false, string domainMappedField = null)
        {
            Info = new fieldInfo { fieldName = name, enrichment = enrichment, fieldState = (ushort)state };
            Type = type;
            IsParam = isParam;
            DomainMappedField = domainMappedField;

            if (allowedValues != null && allowedValues.Length > 0)
            {
                AllowedValues = allowedValues;
                if (string.IsNullOrEmpty(enrichment))
                {
                    Info.enrichment = "'" + string.Join("' or '", AllowedValues) + "'";
                }
            }
        }

        public BeamField(string name, string enrichment = "", FieldState state = FieldState.Unknown, string[] allowedValues = null, bool isParam = false)
            : this(name, typeof(string), enrichment, state, allowedValues, isParam)
        {
        }

        public static BeamField Of<T>(string name, string enrichment = "", FieldState state = FieldState.Unknown, string[] allowedValues = null)
        {
            return new BeamField(name, typeof(T), enrichment, state, allowedValues);
        }
    }

    public class DomainMappedBeamField : BeamField
    {
        public DomainMappedBeamField(string name, string domainMappedField, Type type, FieldState state = FieldState.Unknown, string[] allowedValues = null, bool isParam = false)
            : base(name, type, string.Empty, state, allowedValues, isParam, domainMappedField) { }

        public DomainMappedBeamField(string name, string domainMappedField,  FieldState state = FieldState.Unknown, string[] allowedValues = null, bool isParam = false)
            : base(name, typeof(string), string.Empty, state, allowedValues, isParam, domainMappedField) { }
    }

    public class YesNoBeamField : BeamField
    {
        private static readonly string YesNoLabel = "Y/N";
        private static readonly string[] YesNoValues = new string[] { "Y", "N" };

        public YesNoBeamField(string name, FieldState state = FieldState.Unknown, bool isParam = false)
            : base(name, YesNoLabel, state, YesNoValues, isParam) { }
    }

    public class CurrencyTypeBeamField : BeamField
    {
        private static readonly string[] CurrencyTypeValues = new string[] { "Book", "Reporting" };

        public CurrencyTypeBeamField(string name, FieldState state = FieldState.Unknown, bool isParam = false)
            : base(name, null, state, CurrencyTypeValues, isParam) { }
    }

    public class GrossNetBeamField : BeamField
    {
        private static readonly string GrossNetLabel = "Gross/Net";
        private static readonly string[] CurrencyTypeValues = new string[] { "Gross", "Net" };

        public GrossNetBeamField(string name, FieldState state = FieldState.Unknown, bool isParam = false)
            : base(name, GrossNetLabel, state, CurrencyTypeValues, isParam) { }
    }

    public class FrequencyBeamField : BeamField
    {
        private static readonly string FrequencyLabel = "Daily,Monthly,Quarterly,Annually";
        private static readonly string[] FrequencyValues = new string[] { "Daily", "Monthly", "Quarterly", "Annually" };

        public FrequencyBeamField(string name, FieldState state = FieldState.Unknown, bool isParam = false)
            : base(name, FrequencyLabel, state, FrequencyValues, isParam) { }
    }
}
