﻿using IC.Cluster.Common;
using Microsoft.Extensions.Hosting;
using System;

namespace IC.Green.LightHouse
{
    class Program
    {
        static void Main(string[] args)
        {

            HostWindowsService.CreateHostBuilder(null, args).UseWindowsService().Build().Run();
        }
    }
}
