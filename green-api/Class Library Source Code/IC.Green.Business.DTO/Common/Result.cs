﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IC.Green.Business.DTO.Common
{
    public class Result : ExecutionResult
    {
        public string RecordStatus { get; set; }
        public string Identifier { get; set; }
    }
}
