﻿using System.Collections.Generic;

namespace IC.Green.Business.DTO.Common
{
    public class QueryRequest : Request
    {
        public QueryRequest(string serviceName, string sessionToken, Dictionary<string, string> dctQueryParameters,
            string operationName = null) : base(serviceName, sessionToken, operationName)
        {
            QueryParameters = dctQueryParameters;
        }

        public Dictionary<string, string> QueryParameters { get; set; }
    }
}
