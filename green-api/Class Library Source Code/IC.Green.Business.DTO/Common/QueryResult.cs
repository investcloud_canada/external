﻿using System;
using System.Collections.Generic;

namespace IC.Green.Business.DTO.Common
{
    public class QueryResult<T> : ExecutionResult
    {
        public List<T> Entities { get; set; }

        public QueryResult()
        {

        }

        public QueryResult(Guid gdMessageId, T entity)
        {
            MessageId = gdMessageId;
            Entities = new List<T> {entity};
        }

        public QueryResult(Guid gdMessageId, List<T> lstEntities)
        {
            MessageId = gdMessageId;
            Entities = lstEntities;

        }
    }

}
