﻿using System;
using IC.Cluster.Messages;
using IC.Green.Business.DTO.Constants;

namespace IC.Green.Business.DTO.Common
{
    public class ExecutionResult : IResult
    {
        public Guid MessageId { get; set; }
        public bool Success => Cause == null;
        public string Cause { get; set; }
        public IMessage Request { get; set; }
        public string [] Messages { get; set; }
        public DomainApiEnumerator.DomainExecutionStatus ExecutionStatus { get; set; }
    }
}
