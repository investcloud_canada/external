﻿using System;
using System.Collections.Generic;
using System.Text;
using IC.Cluster.Messages;

namespace IC.Green.Business.DTO.Common
{
    public class Request : IMessage
    {
        public Guid MessageId { get; set; }


        public Request(string serviceName, string sessionToken, string operationName = null)
        {
            MessageId = Guid.NewGuid();
            ServiceName = serviceName;
            Operation = operationName;
            SessionToken = sessionToken;
        }
        public string ServiceName { get; set; }
        public string Operation { get; set; }
        public string SessionToken { get; set; }

    }
}
