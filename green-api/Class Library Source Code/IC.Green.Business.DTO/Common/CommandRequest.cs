﻿
using System.Collections.Generic;

namespace IC.Green.Business.DTO.Common
{
    public class CommandRequest: Request
    {
        public CommandRequest(string serviceName, string sessionToken, Dictionary<string, string> dctCommandParameters,
            string operationName = null) : base(serviceName, sessionToken, operationName)
        {
            CommandParameters = dctCommandParameters;
        }

        public Dictionary<string, string> CommandParameters { get; set; }
    }
}
