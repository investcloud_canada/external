﻿using System;
using IC.Cluster.Messages;

namespace IC.Green.Business.DTO.Security
{
    public class SecurityRequest :IMessage
    {
        public SecurityRequest(string externalSecurityKey, string userId)
        {
            MessageId = Guid.NewGuid();
            ExternalSecurityKey = externalSecurityKey;
            UserId = userId;
        }
        public Guid MessageId { get; set; }
        public string ExternalSecurityKey { get; set; }
        public string UserId { get; set; }
    }
}
