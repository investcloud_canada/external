﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IC.Green.Business.DTO.Security
{
    public class SecurityResponse
    {
        public SecurityResponse(string securityKey, string authToken, Guid gdMessageId)
        {
            SecurityKey = securityKey;
            GreenAuthenticationToken = authToken;
            MessageId = gdMessageId;
        }

        public Guid MessageId { get; set; }
        public string SecurityKey { get; set; }
        public string GreenAuthenticationToken { get; set; }

    }
}
