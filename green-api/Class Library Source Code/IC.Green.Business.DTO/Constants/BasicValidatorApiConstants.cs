﻿namespace IC.Green.Business.DTO.Constants
{
    public class BasicValidatorApiConstants
    {
        public enum RequestMethod
        {
            C, R, U, D, L,
            CR, CU, CD, CL, RU, RD, RL, UD, UL, DL,
            CRU, CRD, CRL, CUD, CUL, CDL, RUD, RUL, RDL, UDL, CRUD,
            CRUL, CRDL, CUDL, RUDL,
            CRUDL
        }
    }
}
