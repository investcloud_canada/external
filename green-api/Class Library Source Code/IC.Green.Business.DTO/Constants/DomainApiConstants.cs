﻿namespace IC.Green.Business.DTO.Constants
{
    public class DomainApiConstants
    {
    }

    public class PublishSubscribeTopics
    {
        public const string DomainService = "Domain";
        public const string BeamApiService = "BeamApi";
    }
    public class PaginationConstants
    {
        public const string Filter = "$filter";
        public const string Top = "$top";
        public const string Skip = "$skip";
        public const string OrderBy = "$orderby";
        public const string Percentile = "%";
        public const string LikeOperator = "LIKE";
        public const string Desc = "DESC";
        public const string NullValue = "NULL";
    }
}
