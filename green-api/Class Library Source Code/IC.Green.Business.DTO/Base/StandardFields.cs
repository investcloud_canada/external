﻿using System;

namespace IC.Green.Business.DTO.Base
{
    public class StandardFields
    {
        public StandardFields()
        { }
        public StandardFields(DateTime startDate, string lastUpdateUser, string lastUpdateFrom)
        {
            ApplicationConfigurationData objApplicationConfigurationData = new ApplicationConfigurationData();
            StartDate = DateTime.Parse(startDate.ToString(objApplicationConfigurationData.DateTimeFormat));
            LastUpdatedAt = DateTime.Parse(DateTime.Now.ToString(objApplicationConfigurationData.DateTimeFormat));
            LastUpdateUser = lastUpdateUser;
            LastUpdatedFrom = lastUpdateFrom;
        }
        public virtual string ApplicationId { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string Status { get; set; }
        public virtual string LastUpdateUser { get; set; }
        public virtual DateTime LastUpdatedAt { get; set; }
        public virtual string LastUpdatedFrom { get; set; }
        public virtual int Version { get; set; }

    }
}
