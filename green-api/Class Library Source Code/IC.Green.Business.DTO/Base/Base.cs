﻿using System.Collections.Generic;
using IC.Green.ExtensionMethods;

namespace IC.Green.Business.DTO.Base
{
    public class Base
    {
        /// <summary>
        /// This method will do the basic validation for string input
        /// </summary>
        /// <param name="dctCommandParameters"></param>
        /// <param name="propertyName"></param>
        /// <param name="applicableFor"></param>
        /// <param name="mandatoryFor"></param>
        /// <param name="methodName"></param>
        /// <param name="lenthValue"></param>
        /// <param name="dctError"></param>
        /// <returns></returns>
        public string GetStringValue(Dictionary<string, string> dctCommandParameters, string propertyName, string applicableFor, string mandatoryFor, string methodName, int lengthValue, Dictionary<string, int> dctError)
        {
            string value = string.Empty;
            if (applicableFor.Contains(methodName))
            {
                if (dctCommandParameters.ContainsKey(propertyName))
                {
                    value = dctCommandParameters.GetValueOrDefault(propertyName);
                    if (!value.IsValidString())
                    {
                        dctError.TryAdd(propertyName, 10110);
                        return null;
                    }
                    if (lengthValue > 0 && !value.IsValidLength(lengthValue))
                    {
                        dctError.TryAdd(propertyName, 20775);
                        return null;
                    }
                    else
                        return value;
                }
                else if (mandatoryFor.Contains(methodName) && !value.IsStringPopulated())
                {
                    dctError.TryAdd(propertyName, 10116);
                    return null;
                }
            }
            return null;
        }
        /// <summary>
        /// This method will make the integer basic validation
        /// </summary>
        /// <param name="dctCommandParameters"></param>
        /// <param name="propertyName"></param>
        /// <param name="applicableFor"></param>
        /// <param name="mandatoryFor"></param>
        /// <param name="methodName"></param>
        /// <param name="dctError"></param>
        /// <returns></returns>
        public int GetIntValue(Dictionary<string, string> dctCommandParameters, string propertyName, string applicableFor, string mandatoryFor, string methodName, Dictionary<string, int> dctError)
        {
            string value = string.Empty;
            int returnValue = 0;
            if (applicableFor.Contains(methodName))
            {
                if (dctCommandParameters.ContainsKey(propertyName))
                {
                    value = dctCommandParameters.GetValueOrDefault(propertyName);
                    if (IsValidInteger(value, out returnValue))
                        return returnValue;
                    else
                    {
                        dctError.TryAdd(propertyName, 20643);
                        return returnValue;
                    }
                }
                else if (mandatoryFor.Contains(methodName) && !value.IsStringPopulated())
                {
                    dctError.TryAdd(propertyName, 10116);
                    return returnValue;
                }
            }
            return returnValue;
        }

        /// <summary>
        /// This method will check weather input string is valid or not
        /// </summary>
        /// <param name="id"></param>
        /// <param name="returnValue"></param>
        /// <returns></returns>
        private bool IsValidInteger(string id, out int returnValue)
        {
            int integer;
            if (int.TryParse(id, out integer))
            {
                returnValue = integer;
                return true;
            }
            else
            {
                returnValue = 0;
                return false;
            }
        }
    }
}
