﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IC.Green.Business.DTO
{
    public class PortfolioWrapperDetail
    {
        public string PortfolioWrapperName { get; set; }
        public string PortfolioWrapperId { get; set; }
        public string PortfolioId { get; set; }
    }
}
