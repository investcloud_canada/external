﻿using System;

namespace IC.Green.Business.DTO.StaticData
{
    public class PartyAgreementTypeTimeDimension
    {
        public PartyAgreementTypeTimeDimension()
        {
            PartyAgreementType = new PartyAgreementType();
            StandardFields = new Base.StandardFields();
        }

        public override int GetHashCode()
        {
            return this.PartyAgreementType.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            PartyAgreementTypeTimeDimension that = (PartyAgreementTypeTimeDimension)obj;
            return this.PartyAgreementType.Equals(that.PartyAgreementTypeId) && this.Version.Equals(that.Version);
        }

        public virtual Int32 PartyAgreementTypeId { get; set; }
        public virtual PartyAgreementType PartyAgreementType { get; set; }
        public virtual Base.StandardFields StandardFields { get; set; }
        public virtual int Version { get; set; }


        public virtual void CopyTo(PartyAgreementType target)
        {
            this.Version = target.StandardFields.Version;
            this.PartyAgreementTypeId = target.PartyAgreementTypeId;
            this.PartyAgreementType.AgreementLevel = target.AgreementLevel;
            this.PartyAgreementType.AgreementStatus = target.AgreementStatus;
            this.PartyAgreementType.AgreementVersion = target.AgreementVersion;
            this.PartyAgreementType.AgreementReference = target.AgreementReference;
            this.PartyAgreementType.EntityType = target.EntityType;
            this.PartyAgreementType.Firm = target.Firm;
            this.PartyAgreementType.Issuer = target.Issuer;
            this.PartyAgreementType.PartyAgreementDocType = target.PartyAgreementDocType;
            this.PartyAgreementType.PartyAgreementTypeName = target.PartyAgreementTypeName;
            this.PartyAgreementType.RequiredFlag = target.RequiredFlag;

            this.StandardFields.ApplicationId = target.StandardFields.ApplicationId;
            this.StandardFields.StartDate = target.StandardFields.StartDate;
            this.StandardFields.EndDate = target.StandardFields.EndDate;
            this.StandardFields.Status = target.StandardFields.Status;
            this.StandardFields.LastUpdatedAt = target.StandardFields.LastUpdatedAt;
            this.StandardFields.LastUpdatedFrom = target.StandardFields.LastUpdatedFrom;
            this.StandardFields.LastUpdateUser = target.StandardFields.LastUpdateUser;
        }
    }
}
