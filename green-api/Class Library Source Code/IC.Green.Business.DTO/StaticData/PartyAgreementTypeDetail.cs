﻿using System;
using System.Collections.Generic;
using IC.Green.Business.DTO.Constants;
namespace IC.Green.Business.DTO
{
    public class PartyAgreementTypeDetail : Base.Base
    {
        public string PartyAgreementType { get; set; }
        public string MaintainFlag { get; set; }
        public string Firm { get; set; }
        public string FirmName { get; set; }
        public string PartyAgreementTypeName { get; set; }
        public Int32? AgreementVersion { get; set; }
        public string Issuer { get; set; }
        public string IssuerName { get; set; }
        public string AgreementLevel { get; set; }
        public string AgreementLevelName { get; set; }
        public string EntityType { get; set; }
        public string EntityTypeName { get; set; }
        public string RequiredFlag { get; set; }
        public string AgreementStatus { get; set; }
        public string AgreementStatusName { get; set; }
        public string AgreementReference { get; set; }
        public string AgreementReferenceName { get; set; }
        public Int32? PartyAgreementTypeId { get; set; }
        public Int32? RowVersion { get; set; }
        public string PageNumber { get; set; }


        /// <summary>
        /// This method will Extract the data from input dictionary
        /// </summary>
        /// <param name="dctCommandParameters"></param>
        /// <param name="methodName"></param>
        /// <returns></returns>
        public Dictionary<string, int> ExtractData(Dictionary<string, string> dctCommandParameters, string methodName)
        {
            Dictionary<string, int> dctError = new Dictionary<string, int>();

            this.PartyAgreementType = GetStringValue(dctCommandParameters, nameof(this.PartyAgreementType), BasicValidatorApiConstants.RequestMethod.CL.ToString(), BasicValidatorApiConstants.RequestMethod.C.ToString(), methodName, 4, dctError);
            this.MaintainFlag = GetStringValue(dctCommandParameters, nameof(this.MaintainFlag), BasicValidatorApiConstants.RequestMethod.L.ToString(), BasicValidatorApiConstants.RequestMethod.L.ToString(), methodName, 1, dctError);
            this.Firm = GetIntValue(dctCommandParameters, nameof(this.Firm), BasicValidatorApiConstants.RequestMethod.CL.ToString(), BasicValidatorApiConstants.RequestMethod.C.ToString(), methodName, dctError).ToString();
            this.PartyAgreementTypeName = GetStringValue(dctCommandParameters, nameof(this.PartyAgreementTypeName), BasicValidatorApiConstants.RequestMethod.CU.ToString(), BasicValidatorApiConstants.RequestMethod.CU.ToString(), methodName, 35, dctError);
            this.AgreementVersion = GetIntValue(dctCommandParameters, nameof(this.AgreementVersion), BasicValidatorApiConstants.RequestMethod.CU.ToString(), string.Empty, methodName, dctError);
            this.Issuer = GetIntValue(dctCommandParameters, nameof(this.Issuer), BasicValidatorApiConstants.RequestMethod.CU.ToString(), string.Empty, methodName, dctError).ToString();
            this.AgreementLevel = GetStringValue(dctCommandParameters, nameof(this.AgreementLevel), BasicValidatorApiConstants.RequestMethod.CL.ToString(), BasicValidatorApiConstants.RequestMethod.C.ToString(), methodName, 4, dctError);
            this.EntityType = GetStringValue(dctCommandParameters, nameof(this.EntityType), BasicValidatorApiConstants.RequestMethod.CL.ToString(), BasicValidatorApiConstants.RequestMethod.C.ToString(), methodName, 4, dctError);
            this.RequiredFlag = GetStringValue(dctCommandParameters, nameof(this.RequiredFlag), BasicValidatorApiConstants.RequestMethod.CU.ToString(), BasicValidatorApiConstants.RequestMethod.CU.ToString(), methodName, 1, dctError);
            this.AgreementStatus = GetStringValue(dctCommandParameters, nameof(this.AgreementStatus), BasicValidatorApiConstants.RequestMethod.CU.ToString(), string.Empty, methodName, 4, dctError);
            this.AgreementReference = GetStringValue(dctCommandParameters, nameof(this.AgreementReference), BasicValidatorApiConstants.RequestMethod.CU.ToString(), BasicValidatorApiConstants.RequestMethod.CU.ToString(), methodName, 4, dctError);
            this.PartyAgreementTypeId = GetIntValue(dctCommandParameters, nameof(this.PartyAgreementTypeId), BasicValidatorApiConstants.RequestMethod.RUD.ToString(), BasicValidatorApiConstants.RequestMethod.RUD.ToString(), methodName, dctError);
            this.RowVersion = GetIntValue(dctCommandParameters, nameof(this.RowVersion), BasicValidatorApiConstants.RequestMethod.UD.ToString(), BasicValidatorApiConstants.RequestMethod.UD.ToString(), methodName, dctError);

            return dctError;
        }
    }
}
