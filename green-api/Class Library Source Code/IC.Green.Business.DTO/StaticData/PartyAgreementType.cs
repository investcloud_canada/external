﻿namespace IC.Green.Business.DTO.StaticData
{
    public class PartyAgreementType
    {
        private Base.StandardFields _standardFields;

        public PartyAgreementType()
        {
            _standardFields = new Base.StandardFields();
        }
        public virtual Base.StandardFields StandardFields
        {
            get { return _standardFields; }
            set { _standardFields = value; }
        }
        public virtual int PartyAgreementTypeId { get; set; }
        public virtual string PartyAgreementDocType { get; set; }
        public virtual int Firm { get; set; }
        public virtual string FirmName { get; set; }
        public virtual string PartyAgreementTypeName { get; set; }
        public virtual int AgreementVersion { get; set; }
        public virtual int Issuer { get; set; }
        public virtual string IssuerName { get; set; }
        public virtual string AgreementLevel { get; set; }
        public virtual string EntityType { get; set; }
        public virtual string RequiredFlag { get; set; }
        public virtual string AgreementStatus { get; set; }
        public virtual string AgreementReference { get; set; }
    }
}
