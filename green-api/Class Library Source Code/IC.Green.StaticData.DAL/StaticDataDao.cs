﻿using Babelsystem.Business.DTO.CommonUtil;
using IC.Green.Base.DAL;
using IC.Green.Business.DTO;
using IC.Green.Business.DTO.Constants;
using IC.Green.Business.DTO.StaticData;
using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace IC.Green.StaticData.DAL
{
    public class StaticDataDao : GreenBaseClass<object>, IStaticDataDao
    {
        /// <summary>
        /// This method will fetch list of party agreement types based on the Requested parameters
        /// </summary>
        /// <param name="objPartyAgreementTypeRequest"></param>
        /// <param name="dctPagination"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="resultCount"></param>
        /// <returns></returns>
        public IList GetPartyAgreementType(PartyAgreementTypeDetail objPartyAgreementTypeRequest, Dictionary<string, string> dctPagination, int pageNumber, int pageSize, out int resultCount)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                Hashtable htResultTransformField = new Hashtable();
                IList<SelectionCriteria> lstSelectionCriteria = new List<SelectionCriteria>();

                query.Append(@" Select
                             PAT.ID                 As PartyAgreementTypeId,   
                             PAT.AGRMT_DOC_TP		As PartyAgreementDocType, 
                             PAT.FIRM_PTY_ID		As Firm,
                             P.SHRT_NM				As FirmName, 
                             PAT.NM					As PartyAgreementTypeName, 
                             PAT.AGRMT_VER			As AgreementVersion, 
                             PAT.REQD_FLG			As RequiredFlag, 
                             PAT.ISSUER_PTY_ID		As Issuer,
                             PTY.SHRT_NM			As IssuerName,
							 PAT.LVL				As AgreementLevel,
							 PAT.ENTITY_TP			As EntityType,
                             PAT.AGRMT_STS			As AgreementStatus, 
                             PAT.AGRMT_REF			As AgreementReference
                         
                         ");
                query.Append(@" From  PARTY_AGREEMENT_TP PAT
                                        Inner Join Party P on P.PTY_ID = PAT.FIRM_PTY_ID
				                        Left Outer Join Party PTY on PTY.PTY_ID = PAT.ISSUER_PTY_ID");
                query.Append(@" Where PAT.STS != :status ");

                lstSelectionCriteria.Add(GetSelectionCriteria("status", "ARCH", NHibernateUtil.AnsiString));

                if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeRequest.Firm))
                {
                    query.Append(@" And PAT.FIRM_PTY_ID = :firm");
                    lstSelectionCriteria.Add(GetSelectionCriteria("firm", objPartyAgreementTypeRequest.Firm.ToString(), NHibernateUtil.Int32));
                }
                if (objPartyAgreementTypeRequest.AgreementLevel != null)
                {
                    query.Append(@" And PAT.LVL = :agreementLevel");
                    lstSelectionCriteria.Add(GetSelectionCriteria("agreementLevel", objPartyAgreementTypeRequest.AgreementLevel.ToString(), NHibernateUtil.AnsiString));
                }
                if (!string.IsNullOrEmpty(objPartyAgreementTypeRequest.EntityType))
                {
                    query.Append(@" And PAT.ENTITY_TP = :entityType");
                    lstSelectionCriteria.Add(GetSelectionCriteria("entityType", objPartyAgreementTypeRequest.EntityType, NHibernateUtil.AnsiString));
                }
                if (!string.IsNullOrEmpty(objPartyAgreementTypeRequest.PartyAgreementType))
                {
                    query.Append(@" And PAT.AGRMT_DOC_TP = :partyAgreementType");
                    lstSelectionCriteria.Add(GetSelectionCriteria("partyAgreementType", objPartyAgreementTypeRequest.PartyAgreementType, NHibernateUtil.AnsiString));
                }
                if (!string.IsNullOrEmpty(objPartyAgreementTypeRequest.MaintainFlag) && objPartyAgreementTypeRequest.MaintainFlag == GetConfiguredData(BooleanValues.Yes.ToString(), nameof(BooleanValues)))
                {
                    lstSelectionCriteria.Add(GetSelectionCriteria("firmPartyId", objPartyAgreementTypeRequest.Firm.ToString(), NHibernateUtil.Int32));
                    query.Append(" And " + CheckOrganisation(":firmPartyId", "PAT.FIRM_PTY_ID"));
                }
                else
                {
                    lstSelectionCriteria.Add(GetSelectionCriteria("firmPartyId", objPartyAgreementTypeRequest.Firm.ToString(), NHibernateUtil.Int32));
                    query.Append(" And " + CheckOrganisation("PAT.FIRM_PTY_ID", ":firmPartyId"));
                }
                if (dctPagination != null && dctPagination.Count > 0)
                    GetPartyAgreementTypeForServerSidePagination(objPartyAgreementTypeRequest, dctPagination, ref query, ref lstSelectionCriteria);

                htResultTransformField.Add("PartyAgreementTypeId", NHibernateUtil.Int32);
                htResultTransformField.Add("PartyAgreementDocType", NHibernateUtil.String);
                htResultTransformField.Add("Firm", NHibernateUtil.Int32);
                htResultTransformField.Add("FirmName", NHibernateUtil.String);
                htResultTransformField.Add("PartyAgreementTypeName", NHibernateUtil.String);
                htResultTransformField.Add("AgreementVersion", NHibernateUtil.Int32);
                htResultTransformField.Add("RequiredFlag", NHibernateUtil.String);
                htResultTransformField.Add("Issuer", NHibernateUtil.Int32);
                htResultTransformField.Add("IssuerName", NHibernateUtil.String);
                htResultTransformField.Add("AgreementLevel", NHibernateUtil.String);
                htResultTransformField.Add("EntityType", NHibernateUtil.String);
                htResultTransformField.Add("AgreementStatus", NHibernateUtil.String);
                htResultTransformField.Add("AgreementReference", NHibernateUtil.String);

                return ExecuteSqlQuery(query.ToString(), dctPagination, lstSelectionCriteria, htResultTransformField, typeof(PartyAgreementType), pageNumber, pageSize, out resultCount);
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw;
            }
        }
        /// <summary>
        /// This method will fetch the database column based on the pagination parameters
        /// </summary>
        /// <param name="objPartyAgreementTypeRequest"></param>
        /// <param name="dctFilters"></param>
        /// <param name="sbQuery"></param>
        /// <param name="lstSelectionCriteria"></param>
        private void GetPartyAgreementTypeForServerSidePagination(PartyAgreementTypeDetail objPartyAgreementTypeRequest, Dictionary<string, string> dctFilters, ref StringBuilder sbQuery, ref IList<SelectionCriteria> lstSelectionCriteria)
        {
            string[] strKey;
            string keyValue = string.Empty;
            int parameterCount = 0;
            string parameterColumnValue = string.Empty;
            string parameterOrderByValue = string.Empty;
            foreach (KeyValuePair<string, string> kvLookup in dctFilters)
            {

                if (!string.IsNullOrWhiteSpace(kvLookup.Key) && !string.IsNullOrWhiteSpace(kvLookup.Value))
                {
                    strKey = kvLookup.Key.Split('*');
                    if (strKey.Length > 1)
                    {
                        if (!string.IsNullOrWhiteSpace(strKey[0]) && !string.IsNullOrWhiteSpace(strKey[1]))
                        {
                            if (strKey[1].Contains(PaginationConstants.LikeOperator))
                                keyValue = kvLookup.Value.Trim() + PaginationConstants.Percentile;
                            else
                                keyValue = kvLookup.Value.Trim();

                            parameterColumnValue = GetPaginationParameters(strKey[0], objPartyAgreementTypeRequest);
                            if (!string.IsNullOrWhiteSpace(parameterColumnValue))
                            {
                                sbQuery.Append(" And ").Append(parameterColumnValue).Append(strKey[1]).Append(":parameter").Append(parameterCount).Append(" ");
                                lstSelectionCriteria.Add(GetSelectionCriteria("parameter" + parameterCount, keyValue, NHibernateUtil.AnsiString));
                                parameterCount++;
                            }
                        }
                    }
                }
            }
            if (dctFilters.TryGetValue(PaginationConstants.OrderBy, out parameterColumnValue))
            {
                parameterOrderByValue = GetOrderByParameters(parameterColumnValue.Split(" ")[0], objPartyAgreementTypeRequest);
                parameterOrderByValue = !string.IsNullOrWhiteSpace(parameterOrderByValue) ? parameterOrderByValue : parameterColumnValue.Split(" ")[0];

                if (parameterColumnValue.ToUpper().Contains(PaginationConstants.Desc))
                    sbQuery.Append(@"Order by ").Append(parameterOrderByValue).Append(" ").Append(PaginationConstants.Desc);
                else
                    sbQuery.Append(@"Order by ").Append(parameterOrderByValue);
            }
        }
        /// <summary>
        /// This method will add all the required pagination parameters to the dictionary
        /// </summary>
        /// <param name="propertName"></param>
        /// <param name="objPartyAgreementTypeRequest"></param>
        /// <returns></returns>
        private string GetPaginationParameters(string propertName, PartyAgreementTypeDetail objPartyAgreementTypeRequest)
        {
            Dictionary<string, string> dctPaginationColumns = new Dictionary<string, string>();
            string columnValue = string.Empty;

            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.FirmName).ToUpper(), "P.SHRT_NM");
            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeName).ToUpper(), "PAT.NM");
            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.Issuer).ToUpper(), "PAT.ISSUER_PTY_ID");
            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.IssuerName).ToUpper(), "PTY.SHRT_NM");
            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.RequiredFlag).ToUpper(), "PAT.REQD_FLG");
            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.AgreementStatus).ToUpper(), "PAT.AGRMT_STS");
            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.AgreementReference).ToUpper(), "PAT.AGRMT_REF");
            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId).ToUpper(), "PAT.ID");

            dctPaginationColumns.TryGetValue(propertName, out columnValue);
            return columnValue;
        }
        /// <summary>
        /// This method will add the required order by parameters to the dictionary
        /// </summary>
        /// <param name="propertName"></param>
        /// <param name="objPartyAgreementTypeRequest"></param>
        /// <returns></returns>
        private string GetOrderByParameters(string propertName, PartyAgreementTypeDetail objPartyAgreementTypeRequest)
        {
            Dictionary<string, string> dctPaginationColumns = new Dictionary<string, string>();
            string orderByValue = string.Empty;

            dctPaginationColumns.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId).ToUpper(), "Id");
            dctPaginationColumns.TryGetValue(propertName.ToUpper(), out orderByValue);
            return orderByValue;
        }

        /// <summary>
        /// This method will get the party agreement type based on the id 
        /// </summary>
        /// <param name="partyAgreementTypeId"></param>
        /// <returns></returns>
        public IList GetPartyAgreementType(Int32 partyAgreementTypeId)
        {
            try
            {
                SelectionCriteria[] selectionCriteria = new SelectionCriteria[1];
                selectionCriteria[0] = GetSelectionCriteria("PartyAgreementTypeId", partyAgreementTypeId.ToString(), NHibernateUtil.Int32, "=");
                return Find(typeof(PartyAgreementType), selectionCriteria);

            }
            catch (Exception ex)
            {
                LogException(ex);
                throw;
            }
        }
        /// <summary>
        /// This method will fetch the data from party agreement type table based on the AgreementDocument and status
        /// </summary>
        /// <param name="agreementDocument"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IList GetPartyAgreementDetails(string agreementDocument, string status)
        {
            try
            {
                SelectionCriteria[] selectionCriteria = new SelectionCriteria[2];
                selectionCriteria[0] = GetSelectionCriteria("AgreementDocumentType", agreementDocument.ToString(), NHibernateUtil.AnsiString, "=");
                selectionCriteria[1] = GetSelectionCriteria("StandardFields.Status", status, NHibernateUtil.AnsiString, "=");

                return Find(typeof(Babelsystem.Business.DTO.Portfolio.PartyAgreement), selectionCriteria);
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw;
            }
        }

    }
}
