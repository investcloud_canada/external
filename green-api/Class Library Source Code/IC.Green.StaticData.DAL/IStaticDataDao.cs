﻿using IC.Green.Business.DTO;
using System;
using System.Collections;
using System.Collections.Generic;

namespace IC.Green.StaticData.DAL
{
    public interface IStaticDataDao
    {
        void SaveOrUpdateAll(IList lstRecords);
        IList GetPartyAgreementType(Int32 partyAgreementTypeId);
        IList GetPartyAgreementType(PartyAgreementTypeDetail objPartyAgreementTypeRequest, Dictionary<string, string> dctPagination, int pageNumber, int pageSize, out int resultCount);
        IList GetPartyAgreementDetails(string agreementDocument, string status);
    }
}
