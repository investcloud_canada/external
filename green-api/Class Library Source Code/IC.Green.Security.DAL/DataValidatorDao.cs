﻿using Babelsystem.Base.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Babelsystem.Business.DTO.CommonUtil;
using Babelsystem.Business.DTO.UserAccessSecurity;
using NHibernate;

namespace IC.Green.DataValidation.DAL
{
    public class DataValidatorDao : BaseClass<object>, IDataValidatorDao
    {
        /// <summary>
        /// Validate whether record exist in APP_USER_LOG table for the given External SessionId and the user. 
        /// </summary>
        /// <param name="userSessionId"></param>
        /// <param name="externalSessionId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList IsUserSessionExists(string userSessionId, string externalSessionId, string userId)
        {
            try
            {
                IList<SelectionCriteria> lstSelectionCriteria = new List<SelectionCriteria>();
                StringBuilder sbQuery = new StringBuilder();
                sbQuery.Append(@"Select SESSION_ID as SessionId, STS as Status
                                   From APP_USER_LOG 
                                    Where 1 =1 ");
                if (!string.IsNullOrWhiteSpace(userSessionId))
                {
                    sbQuery.Append(" And SESSION_ID =:userSessionId");
                    lstSelectionCriteria.Add(GetSelectionCriteria("userSessionId", userSessionId.Trim(),
                        NHibernateUtil.AnsiString));
                }
                if (!string.IsNullOrWhiteSpace(externalSessionId))
                {
                    sbQuery.Append(" And EXT_SEC_KEY =:externalSessionId");
                    lstSelectionCriteria.Add(GetSelectionCriteria("externalSessionId", externalSessionId.Trim(), NHibernateUtil.AnsiString));
                }
                if (!string.IsNullOrWhiteSpace(userId))
                {
                    sbQuery.Append(" And USR_ID =:userId");
                    lstSelectionCriteria.Add(GetSelectionCriteria("userId", userId.Trim(), NHibernateUtil.AnsiString));
                }
                Hashtable htResultTransformField = new Hashtable
                {
                    {"SessionId", NHibernateUtil.AnsiString}, {"Status", NHibernateUtil.AnsiString}
                };
                return ExecuteSqlQuery(sbQuery.ToString(), lstSelectionCriteria, htResultTransformField, typeof(AppUserLog));

            }
            catch (Exception ex)
            {
                LogException(ex);
                throw;
            }
        }

        /// <summary>
        /// Activate the APP_USER_LOG entry
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public bool ActivateUserLog(string sessionId)
        {
            string setFields = @"LastActivityDate=:lastActivityDate, Status=:status, EndDate=:endDate";
            string whereCondition = @"SessionId=:sessionId";
            Hashtable htValues = new Hashtable();
            htValues.Add("sessionId", sessionId);
            htValues.Add("lastActivityDate", DateTime.Now);
            htValues.Add("status", current);
            htValues.Add("endDate", DateTime.Now.AddMinutes(ApplicationData.SessionTimeOut));
            return Update(setFields, whereCondition, "AppUserLog", htValues, false);
        }
    }
}
