﻿using System.Collections;

namespace IC.Green.DataValidation.DAL
{
    public interface IDataValidatorDao
    {
        void Save(object entity);
        IList IsUserSessionExists(string userSessionId, string externalSessionId, string userId);
        bool ActivateUserLog(string sessionId);
    }
}