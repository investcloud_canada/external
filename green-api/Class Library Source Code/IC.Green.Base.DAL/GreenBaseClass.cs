﻿using Babelsystem.Base.DAL;
using Babelsystem.Business.DTO.CommonUtil;
using IC.Green.Business.DTO.Constants;
using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IC.Green.Base.DAL
{
    public class GreenBaseClass<T> : BaseClass<T>
    {
        public IList ExecuteSqlQuery(string query, Dictionary<string, string> dctPagination, IList<SelectionCriteria> lstCriteria, Hashtable htResultTransformField, Type resultTransformEntity, int pageNumber,
            int pageSize, out int numberOfRecords)
        {
            try
            {
                ISQLQuery sqlQuery = Session.CreateSQLQuery(query);
                if (lstCriteria != null)
                    SetSelectionCriteria(lstCriteria, ref sqlQuery);
                SetResultTransformProperties(ref htResultTransformField, ref sqlQuery);
                sqlQuery.SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean(resultTransformEntity));

                numberOfRecords = sqlQuery.List().Count;
                GetMaximumPageNumber(numberOfRecords, pageSize, ref pageNumber);
                //custom pagination for P-tier


                if (dctPagination.TryGetValue(PaginationConstants.Top, out var top) && dctPagination.TryGetValue(PaginationConstants.Skip, out var skip))
                {
                    sqlQuery.SetFirstResult(Convert.ToInt32(skip));
                    sqlQuery.SetMaxResults(Convert.ToInt32(top));
                }
                else if (ApplicationData.PageSize != null)
                {
                    sqlQuery.SetFirstResult(ApplicationData.FirstResultSet);
                    sqlQuery.SetMaxResults(Convert.ToInt32(ApplicationData.PageSize));
                }
                else
                {
                    sqlQuery.SetFirstResult(pageNumber * pageSize);
                    sqlQuery.SetMaxResults(pageSize);
                }

                return (IList)sqlQuery.List();
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw;
            }
        }

        /// <summary>
        /// This method used to set selection Critera by considering Field Type
        /// </summary>
        /// <param name="lstCriteria"></param>
        /// <param name="executeQuery"></param>
        private void SetSelectionCriteria(IEnumerable<SelectionCriteria> lstCriteria, ref NHibernate.ISQLQuery executeQuery)
        {
            try
            {
                if (lstCriteria != null)
                {
                    foreach (SelectionCriteria objSelectionCriteria in lstCriteria)
                    {
                        if (executeQuery.NamedParameters.Contains(objSelectionCriteria.ColumnName))
                        {
                            executeQuery.SetParameter(objSelectionCriteria.ColumnName, objSelectionCriteria.FieldValue,
                                objSelectionCriteria.Type);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw;
            }
        }
        /// <summary>
        /// This method will fetch the pagenumber based on the pagesize and number of records
        /// </summary>
        /// <param name="numberOfRecords"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        private void GetMaximumPageNumber(int numberOfRecords, int pageSize, ref int pageNumber)
        {
            if (numberOfRecords > 0 && pageSize > 0 && pageNumber > 0)
            {
                int maxPageNumber = Convert.ToInt32(Math.Ceiling(Convert.ToDouble((numberOfRecords) / Convert.ToDouble(pageSize)))) - 1;
                if (pageNumber > maxPageNumber)
                    pageNumber = maxPageNumber;
            }
        }
    }
}
