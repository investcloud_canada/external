﻿using Babelsystem.Base.BL;

namespace IC.Green.StaticData.BL
{
    public class StaticDataBase : BaseClass
    {
        public enum StaticDataTables
        {
            PARTY_AGREEMENT_TP
        }
        public enum StaticDataFields
        {
            PartyAgreementType
        }
    }
}
