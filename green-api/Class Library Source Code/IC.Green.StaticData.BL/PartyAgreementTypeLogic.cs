﻿using Babelsystem.ApplicationDataStore;
using Babelsystem.Business.DTO.CommonUtil;
using Babelsystem.Business.Service.DTO.Base;
using Babelsystem.Validator;
using IC.Green.Business.DTO;
using IC.Green.Business.DTO.Common;
using IC.Green.Business.DTO.Constants;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Babelsystem.Business.DTO.StaticData;
using IC.Green.Business.DTO.StaticData;

namespace IC.Green.StaticData.BL
{
    public class PartyAgreementTypeLogic : StaticDataBase, IPartyAgreementTypeLogic
    {
        public IC.Green.StaticData.DAL.IStaticDataDao StaticDataDao { set; get; }
        /// <summary>
        /// This method will convert the errored data in the dictionary to string array
        /// </summary>
        /// <param name="dctError"></param>
        /// <returns></returns>
        private string[] ConvertDictionaryToString(Dictionary<string, int> dctError)
        {
            if (dctError != null && dctError.Count > 0)
            {
                StringBuilder responseBuilder;
                List<string> lstErrorMessages = new List<string>();
                String errorMessages = string.Join(",", dctError.GroupBy(x => x.Value).ToList().Select(x => x.Key));
                IList<AppMessage> lstMessageList = Util.GetMessageList(errorMessages, ApplicationData.FirmPartyId).OfType<AppMessage>().ToList();
                foreach (var item in dctError)
                {
                    responseBuilder = new StringBuilder();
                    responseBuilder.Append(item.Value).Append("|").Append(lstMessageList?.Where(x => x.MessageId.Trim() == item.Value.ToString())?.FirstOrDefault()?.MessageNarration).Append("|").Append(item.Key);
                    lstErrorMessages.Add(responseBuilder.ToString());
                }

                return lstErrorMessages.ToArray();
            }
            return null;
        }

        #region PartyAgreementTypeServices
        /// <summary>
        /// This method will create a new row to the party Agreement Type table
        /// </summary>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        public Result ExecuteCreateRequest(CommandRequest objRequest)
        {
            Dictionary<string, int> dctError = new Dictionary<string, int>();
            PartyAgreementTypeDetail objPartyAgreementTypeRequest = new PartyAgreementTypeDetail();
            dctError = objPartyAgreementTypeRequest.ExtractData(objRequest.CommandParameters, BasicValidatorApiConstants.RequestMethod.C.ToString());
            Result objpartyAgreementTypeResponse = new Result();
            PartyAgreementType objPartyAgreementType = new PartyAgreementType();
            PartyAgreementTypeTimeDimension objTimeDimension = new PartyAgreementTypeTimeDimension();
            IList lstRecords = new ArrayList();
            try
            {
                if (objPartyAgreementTypeRequest != null)
                {
                    if (dctError != null && dctError.Count == 0)
                    {
                        IApplicationData objApplicationData = SetApplicationData(objRequest.SessionToken);
                        objPartyAgreementType = GetExistingAgreementType(objPartyAgreementTypeRequest.PartyAgreementType);
                        ValidatePartyAgreementType(objPartyAgreementTypeRequest, objApplicationData, BasicValidatorApiConstants.RequestMethod.C.ToString(), dctError, objPartyAgreementType);

                        if (dctError != null && dctError.Count == 0)
                        {
                            if (objPartyAgreementType.StandardFields.Status == GetConfiguredData(nameof(DataBaseStatus), DataBaseStatus.Archived.ToString()) && objPartyAgreementType.PartyAgreementTypeId > 0)
                            {
                                objTimeDimension = UpdatePartyAgreementTypeTimeDimension(objPartyAgreementType, objPartyAgreementType.PartyAgreementTypeId);
                                lstRecords.Add(objTimeDimension);
                                objPartyAgreementType.StandardFields.Version = objPartyAgreementType.StandardFields.Version + 1;
                            }
                            else
                                objPartyAgreementType.StandardFields.Version = 1;

                            objPartyAgreementType.PartyAgreementDocType = objPartyAgreementTypeRequest.PartyAgreementType;
                            objPartyAgreementType.Firm = Convert.ToInt32(objPartyAgreementTypeRequest.Firm);
                            objPartyAgreementType.PartyAgreementTypeName = objPartyAgreementTypeRequest.PartyAgreementTypeName;
                            objPartyAgreementType.AgreementVersion = Convert.ToInt32(objPartyAgreementTypeRequest.AgreementVersion);
                            objPartyAgreementType.Issuer = Convert.ToInt32(objPartyAgreementTypeRequest.Issuer);
                            objPartyAgreementType.AgreementLevel = objPartyAgreementTypeRequest.AgreementLevel;
                            objPartyAgreementType.EntityType = objPartyAgreementTypeRequest.EntityType;
                            objPartyAgreementType.RequiredFlag = objPartyAgreementTypeRequest.RequiredFlag;
                            objPartyAgreementType.AgreementStatus = objPartyAgreementTypeRequest.AgreementStatus;
                            objPartyAgreementType.AgreementReference = objPartyAgreementTypeRequest.AgreementReference;


                            objPartyAgreementType.StandardFields.StartDate = DateTime.Now;
                            objPartyAgreementType.StandardFields.Status = GetConfiguredData(nameof(DataBaseStatus), DataBaseStatus.Current.ToString());
                            objPartyAgreementType.StandardFields.LastUpdatedAt = DateTime.Now;
                            objPartyAgreementType.StandardFields.LastUpdatedFrom = ApplicationData.LastUpdatedFrom;
                            objPartyAgreementType.StandardFields.LastUpdateUser = ApplicationData.LastUpdateUser;
                            objPartyAgreementType.StandardFields.ApplicationId = ApplicationData.ApplicationId;


                            lstRecords.Add(objPartyAgreementType);
                            StaticDataDao.SaveOrUpdateAll(lstRecords);
                            objpartyAgreementTypeResponse.Identifier = objPartyAgreementType.PartyAgreementTypeId.ToString();
                            objpartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Pass;
                        }
                        else
                        {
                            objpartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                            objpartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                        }
                    }
                    else
                    {
                        objpartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                        objpartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                    }
                }
                objpartyAgreementTypeResponse.MessageId = objRequest.MessageId;
                return objpartyAgreementTypeResponse;
            }
            catch (Exception ex)
            {
                LogException(ex);
                dctError.TryAdd(StaticDataFields.PartyAgreementType.ToString(), 20800);
                objpartyAgreementTypeResponse.MessageId = objRequest.MessageId;
                objpartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                objpartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Error;
                LogMessage(ex.Message, MessageType.Error);
                return objpartyAgreementTypeResponse;
            }
        }
        /// <summary>
        /// This method will fetch the data based on agreementType
        /// </summary>
        /// <param name="agreementType"></param>
        /// <returns></returns>
        private PartyAgreementType GetExistingAgreementType(string agreementType)
        {
            PartyAgreementType objPartyAgreementType = new PartyAgreementType();
            string PartyAgreementTypeId = Util.GetFieldValue(GetConfiguredData(nameof(DefaultCode), DefaultCode.Id.ToString()), StaticDataTables.PARTY_AGREEMENT_TP.ToString(),
                                       GetConfiguredData(nameof(FieldName), FieldName.AgreementDocumentType.ToString()), agreementType);
            if (!string.IsNullOrWhiteSpace(PartyAgreementTypeId))
            {
                IList<PartyAgreementType> lstPartyAgreementType = StaticDataDao.GetPartyAgreementType(Convert.ToInt32(PartyAgreementTypeId)).OfType<PartyAgreementType>().ToList();
                if (lstPartyAgreementType != null && lstPartyAgreementType.Count > 0)
                    objPartyAgreementType = lstPartyAgreementType.FirstOrDefault();

            }
            return objPartyAgreementType;
        }
        /// <summary>
        /// This method will Update the Existing row in the databse
        /// </summary>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        public Result ExecuteUpdateRequest(CommandRequest objRequest)
        {
            Dictionary<string, int> dctError = new Dictionary<string, int>();
            PartyAgreementTypeDetail objPartyAgreementTypeRequest = new PartyAgreementTypeDetail();
            dctError = objPartyAgreementTypeRequest.ExtractData(objRequest.CommandParameters, BasicValidatorApiConstants.RequestMethod.U.ToString());
            Result objPartyAgreementTypeResponse = new Result();
            PartyAgreementType objPartyAgreementType = new PartyAgreementType();
            PartyAgreementTypeTimeDimension objTimeDimension;
            IList lstRecords = new ArrayList();
            int count = 0;
            try
            {
                if (objPartyAgreementTypeRequest != null)
                {
                    if (dctError != null && dctError.Count == 0)
                    {
                        IApplicationData objApplicationData = SetApplicationData(objRequest.SessionToken);
                        objPartyAgreementTypeRequest.Firm = objApplicationData.FirmPartyId;
                        ValidatePartyAgreementType(objPartyAgreementTypeRequest, objApplicationData, BasicValidatorApiConstants.RequestMethod.U.ToString(), dctError, objPartyAgreementType);
                        if (dctError != null && dctError.Count == 0)
                        {
                            IList lstPartyAgreementType = StaticDataDao.GetPartyAgreementType(Convert.ToInt32(objPartyAgreementTypeRequest.PartyAgreementTypeId));

                            if (lstPartyAgreementType != null && lstPartyAgreementType.Count > 0)
                            {
                                objPartyAgreementType = lstPartyAgreementType.OfType<PartyAgreementType>().FirstOrDefault();

                                objTimeDimension = UpdatePartyAgreementTypeTimeDimension(objPartyAgreementType, objPartyAgreementType.PartyAgreementTypeId);
                                SetModifiedPartyAgreementType(objPartyAgreementTypeRequest, objPartyAgreementType, out count);

                                if (count > 0)
                                {
                                    objPartyAgreementType.StandardFields.Version = objPartyAgreementType.StandardFields.Version + 1;
                                    objPartyAgreementType.StandardFields.LastUpdatedAt = System.DateTime.Now;
                                    objPartyAgreementType.StandardFields.ApplicationId = objPartyAgreementType.StandardFields.ApplicationId;
                                    objPartyAgreementType.StandardFields.LastUpdateUser = objPartyAgreementType.StandardFields.LastUpdateUser;
                                    objPartyAgreementType.StandardFields.LastUpdatedFrom = objPartyAgreementType.StandardFields.LastUpdatedFrom;
                                    lstRecords.Add(objTimeDimension);
                                    lstRecords.Add(objPartyAgreementType);
                                    StaticDataDao.SaveOrUpdateAll(lstRecords);
                                }
                                objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Pass;
                            }
                            else
                            {
                                dctError.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId), 10026);
                                objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                                objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                            }
                        }
                        else
                        {

                            objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                            objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;

                        }

                    }
                    else
                    {

                        objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                        objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;

                    }

                }
                objPartyAgreementTypeResponse.MessageId = objRequest.MessageId;
                return objPartyAgreementTypeResponse;
            }
            catch (Exception ex)
            {
                LogException(ex);
                dctError.TryAdd(StaticDataFields.PartyAgreementType.ToString(), 20800);
                objPartyAgreementTypeResponse.MessageId = objRequest.MessageId;
                objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Error;
                return objPartyAgreementTypeResponse;
            }
        }
        /// <summary>
        /// This method will check the modified data from Request with the Database
        /// </summary>
        /// <param name="objPartyAgreementTypeRequest"></param>
        /// <param name="objPartyAgreementType"></param>
        /// <param name="count"></param>
        private void SetModifiedPartyAgreementType(PartyAgreementTypeDetail objPartyAgreementTypeRequest, PartyAgreementType objPartyAgreementType, out int count)
        {
            count = 0;
            BasicValidator objBasicValidator = new BasicValidator();
            if (objBasicValidator.IsDataChanged(objPartyAgreementTypeRequest.PartyAgreementTypeName, objPartyAgreementType.PartyAgreementTypeName))
            {
                objPartyAgreementType.PartyAgreementTypeName = objPartyAgreementTypeRequest.PartyAgreementTypeName;
                count++;
            }
            if (objBasicValidator.IsDataChanged(objPartyAgreementTypeRequest.AgreementVersion.ToString(), objPartyAgreementType.AgreementVersion.ToString()))
            {
                objPartyAgreementType.AgreementVersion = Convert.ToInt32(objPartyAgreementTypeRequest.AgreementVersion);
                count++;
            }
            if (objBasicValidator.IsDataChanged(objPartyAgreementTypeRequest.Issuer.ToString(), objPartyAgreementType.Issuer.ToString()))
            {
                objPartyAgreementType.Issuer = Convert.ToInt32(objPartyAgreementTypeRequest.Issuer);
                count++;
            }
            if (objBasicValidator.IsDataChanged(objPartyAgreementTypeRequest.RequiredFlag, objPartyAgreementType.RequiredFlag))
            {
                objPartyAgreementType.RequiredFlag = objPartyAgreementTypeRequest.RequiredFlag;
                count++;
            }
            if (objBasicValidator.IsDataChanged(objPartyAgreementTypeRequest.AgreementStatus, objPartyAgreementType.AgreementStatus))
            {
                objPartyAgreementType.AgreementStatus = objPartyAgreementTypeRequest.AgreementStatus;
                count++;
            }
            if (objBasicValidator.IsDataChanged(objPartyAgreementTypeRequest.AgreementReference, objPartyAgreementType.AgreementReference))
            {
                objPartyAgreementType.AgreementReference = objPartyAgreementTypeRequest.AgreementReference;
                count++;
            }
        }
        /// <summary>
        /// This method will Add the data from main table to its TD table
        /// </summary>
        /// <param name="objUpdateTimeDimension"></param>
        /// <param name="PartyAgreementTypeId"></param>
        /// <returns></returns>
        private PartyAgreementTypeTimeDimension UpdatePartyAgreementTypeTimeDimension(PartyAgreementType objUpdateTimeDimension, Int32 PartyAgreementTypeId)
        {
            PartyAgreementTypeTimeDimension objTimeDimension = new PartyAgreementTypeTimeDimension();
            objTimeDimension.CopyTo(objUpdateTimeDimension);
            objTimeDimension.PartyAgreementTypeId = PartyAgreementTypeId;
            return objTimeDimension;
        }
        /// <summary>
        /// This method will delete the existing row in the party Agreement Type table
        /// </summary>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        public Result ExecuteDeleteRequest(CommandRequest objRequest)
        {
            Dictionary<string, int> dctError = new Dictionary<string, int>();
            PartyAgreementTypeDetail objPartyAgreementTypeRequest = new PartyAgreementTypeDetail();
            dctError = objPartyAgreementTypeRequest.ExtractData(objRequest.CommandParameters, BasicValidatorApiConstants.RequestMethod.D.ToString());
            Result objPartyAgreementTypeResponse = new Result();
            List<StatusReason> lstStatusReason = new List<StatusReason>();
            PartyAgreementType objPartyAgreementType = new PartyAgreementType();
            PartyAgreementTypeTimeDimension objTimeDimension;
            IList lstRecords = new ArrayList();
            try
            {
                IApplicationData objApplicationData = SetApplicationData(objRequest.SessionToken);
                if (dctError != null && dctError.Count == 0)
                {
                    IList<PartyAgreementType> lstPartyAgreementType = StaticDataDao.GetPartyAgreementType(Convert.ToInt32(objPartyAgreementTypeRequest.PartyAgreementTypeId))
                                                                                                            .OfType<PartyAgreementType>().ToList();
                    IList<Babelsystem.Business.DTO.Portfolio.PartyAgreement> lstPartyAgreement = StaticDataDao.GetPartyAgreementDetails(lstPartyAgreementType?.FirstOrDefault().PartyAgreementDocType,
                                                                                                    lstPartyAgreementType?.FirstOrDefault().StandardFields.Status).OfType<Babelsystem.Business.DTO.Portfolio.PartyAgreement>().ToList();
                    if (lstPartyAgreement?.Count > 0)
                        dctError.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId), 22313);
                    if (lstPartyAgreementType.Where(x => x.StandardFields.Status == GetConfiguredData(nameof(RecordStatus), RecordStatus.Archived.ToString())).ToList()?.Count > 0)
                        dctError.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId), 22318);
                    if (lstPartyAgreementType.Count > 0 && lstPartyAgreementType.Where(x => x.StandardFields.Version == objPartyAgreementTypeRequest.RowVersion).ToList()?.Count == 0)
                        dctError.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId), 10202);


                    if (dctError != null && dctError.Count == 0)
                    {
                        lstPartyAgreementType = lstPartyAgreementType.Where(x => x.StandardFields.Status == GetConfiguredData(nameof(RecordStatus), RecordStatus.Current.ToString())).ToList();
                        if (lstPartyAgreementType != null && lstPartyAgreementType.Count > 0)
                        {
                            objPartyAgreementType = lstPartyAgreementType.OfType<PartyAgreementType>().FirstOrDefault();
                            objTimeDimension = UpdatePartyAgreementTypeTimeDimension(objPartyAgreementType, objPartyAgreementType.PartyAgreementTypeId);
                            objPartyAgreementType.StandardFields.Version = objPartyAgreementType.StandardFields.Version + 1;
                            objPartyAgreementType.StandardFields.LastUpdatedAt = System.DateTime.Now;
                            objPartyAgreementType.StandardFields.ApplicationId = objPartyAgreementType.StandardFields.ApplicationId;
                            objPartyAgreementType.StandardFields.LastUpdateUser = objPartyAgreementType.StandardFields.LastUpdateUser;
                            objPartyAgreementType.StandardFields.LastUpdatedFrom = objPartyAgreementType.StandardFields.LastUpdatedFrom;
                            objPartyAgreementType.StandardFields.Status = GetConfiguredData(nameof(RecordStatus), RecordStatus.Archived.ToString());
                            lstRecords.Add(objTimeDimension);
                            lstRecords.Add(objPartyAgreementType);
                            StaticDataDao.SaveOrUpdateAll(lstRecords);
                            objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Pass;
                        }
                        else
                        {
                            dctError.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId), 10213);
                            objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                            objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                        }
                    }
                    else
                    {
                        objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                        objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                    }
                }
                else
                {
                    objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                    objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                }
                objPartyAgreementTypeResponse.MessageId = objRequest.MessageId;
                return objPartyAgreementTypeResponse;
            }
            catch (Exception ex)
            {
                LogException(ex);
                dctError.TryAdd(StaticDataFields.PartyAgreementType.ToString(), 20800);
                objPartyAgreementTypeResponse.MessageId = objRequest.MessageId;
                objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Error;
                return objPartyAgreementTypeResponse;
            }
        }
        /// <summary>
        /// This method will fetch the list of party Agreement types and returns to the External service
        /// </summary>
        /// <param name="objMessageRequest"></param>
        /// <returns></returns>
        public QueryResult<PartyAgreementTypeDetail> ExecuteListRequest(QueryRequest objMessageRequest)
        {
            Dictionary<string, int> dctError = new Dictionary<string, int>();
            Dictionary<string, string> dctFilters = new Dictionary<string, string>();
            PartyAgreementTypeDetail objPartyAgreementTypeRequest = new PartyAgreementTypeDetail();
            dctError = objPartyAgreementTypeRequest.ExtractData(objMessageRequest.QueryParameters, BasicValidatorApiConstants.RequestMethod.L.ToString());
            QueryResult<PartyAgreementTypeDetail> objResponseMessage = new QueryResult<PartyAgreementTypeDetail>();
            QueryResult<PartyAgreementTypeDetail> objPartyAgreementTypeResponse;
            List<PartyAgreementTypeDetail> lstPartyAgreementTypeResponse = new List<PartyAgreementTypeDetail>();
            string paginationParams = string.Empty;

            if (objMessageRequest.QueryParameters.TryGetValue(PaginationConstants.Filter, out paginationParams))
                dctFilters = ConvertFilters(paginationParams);
            if (objMessageRequest.QueryParameters.TryGetValue(PaginationConstants.Top, out paginationParams))
                dctFilters.TryAdd(PaginationConstants.Top, paginationParams);
            if (objMessageRequest.QueryParameters.TryGetValue(PaginationConstants.Skip, out paginationParams))
                dctFilters.TryAdd(PaginationConstants.Skip, paginationParams);
            if (objMessageRequest.QueryParameters.TryGetValue(PaginationConstants.OrderBy, out paginationParams))
                dctFilters.TryAdd(PaginationConstants.OrderBy, paginationParams);

            try
            {
                if (objPartyAgreementTypeRequest != null)
                {
                    if (dctError != null && dctError.Count == 0)
                    {
                        IApplicationData objApplicationData = SetApplicationData(objMessageRequest.SessionToken);
                        ValidatePartyAgreementType(objPartyAgreementTypeRequest, objApplicationData, BasicValidatorApiConstants.RequestMethod.L.ToString(), dctError);
                        if (dctError != null && dctError.Count == 0)
                        {
                            int numberOfRecords = 0;
                            if (string.IsNullOrWhiteSpace(objPartyAgreementTypeRequest.Firm))
                                objPartyAgreementTypeRequest.Firm = ApplicationData.FirmPartyId;
                            Babelsystem.Business.DTO.Base.PaginationLookup objPaginationLookup = GetPaginationParameters(string.Equals(objPartyAgreementTypeRequest.PageNumber,PaginationConstants.NullValue) ? 0 : !string.IsNullOrWhiteSpace(objPartyAgreementTypeRequest.PageNumber)
                                                                                                ? Convert.ToInt32(objPartyAgreementTypeRequest.PageNumber) : 0);
                            IList<PartyAgreementType> lstPartyAgreementType = StaticDataDao.GetPartyAgreementType(objPartyAgreementTypeRequest, dctFilters, objPaginationLookup.LookupPageNumber,
                                                                                objPaginationLookup.LookupPageSize, out numberOfRecords).OfType<PartyAgreementType>().ToList();

                            if (lstPartyAgreementType != null && lstPartyAgreementType.Count > 0)
                            {
                                lstPartyAgreementTypeResponse = MapPartyAgreementType(lstPartyAgreementType);
                                objResponseMessage.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Pass;
                            }
                            else
                            {
                                dctError.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId), 10026);
                                objResponseMessage.Messages = ConvertDictionaryToString(dctError);
                                objResponseMessage.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                            }
                        }
                        else
                        {
                            objResponseMessage.Messages = ConvertDictionaryToString(dctError);
                            objResponseMessage.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                        }
                    }
                    else
                    {
                        objResponseMessage.Messages = ConvertDictionaryToString(dctError);
                        objResponseMessage.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                    }
                }
                objPartyAgreementTypeResponse = new QueryResult<PartyAgreementTypeDetail>(objMessageRequest.MessageId, lstPartyAgreementTypeResponse);
                objPartyAgreementTypeResponse.MessageId = objMessageRequest.MessageId;
                objPartyAgreementTypeResponse.Messages = objResponseMessage.Messages;
                objPartyAgreementTypeResponse.ExecutionStatus = objResponseMessage.ExecutionStatus;
                return objPartyAgreementTypeResponse;
            }
            catch (Exception ex)
            {
                LogException(ex);
                dctError.TryAdd(StaticDataFields.PartyAgreementType.ToString(), 20800);
                objPartyAgreementTypeResponse = new QueryResult<PartyAgreementTypeDetail>();
                objPartyAgreementTypeResponse.MessageId = objMessageRequest.MessageId;
                objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Error;
                return objPartyAgreementTypeResponse;
            }
        }
        /// <summary>
        /// This method will map the internal dto class data to the External service data
        /// </summary>
        /// <param name="lstPartyAgreementType"></param>
        /// <returns></returns>
        private List<PartyAgreementTypeDetail> MapPartyAgreementType(IList<PartyAgreementType> lstPartyAgreementType)
        {
            Hashtable htListAndValueCode = new Hashtable();
            htListAndValueCode.Add(GetConfiguredData(nameof(ListCode), ListCode.AgreementTypeLevel.ToString()), string.Empty);
            htListAndValueCode.Add(GetConfiguredData(nameof(ListCode), ListCode.AgreementReferenceType.ToString()), string.Empty);

            List<PartyAgreementTypeDetail> lstPartyAgreementTypeDetail = new List<PartyAgreementTypeDetail>();
            string firmPtyId = string.Empty;
            if (lstPartyAgreementType != null && lstPartyAgreementType.Count > 0)
            {
                firmPtyId = lstPartyAgreementType.FirstOrDefault().Firm.ToString();
                IList<AppValueList> lstAppValuesList = Util.GetApplicationValueList(htListAndValueCode, !string.IsNullOrWhiteSpace(firmPtyId) ? firmPtyId : ApplicationData.FirmPartyId);
                lstPartyAgreementTypeDetail = lstPartyAgreementType.Select(x => new PartyAgreementTypeDetail
                {
                    PartyAgreementTypeId = x.PartyAgreementTypeId,
                    AgreementLevel = x.AgreementLevel,
                    AgreementStatus = x.AgreementStatus,
                    AgreementVersion = x.AgreementVersion,
                    AgreementReference = x.AgreementReference,
                    EntityType = x.EntityType,
                    Firm = x.Firm.ToString(),
                    FirmName = x.FirmName,
                    Issuer = x.Issuer.ToString(),
                    IssuerName = x.IssuerName,
                    PartyAgreementType = x.PartyAgreementDocType,
                    PartyAgreementTypeName = x.PartyAgreementTypeName,
                    RequiredFlag = x.RequiredFlag?.Trim(),
                    RowVersion = x.StandardFields.Version,
                }).ToList();
            }

            return lstPartyAgreementTypeDetail;
        }
        /// <summary>
        /// This method will fetch the data from partyAgreementType table for GET service
        /// </summary>
        /// <param name="objMessageRequest"></param>
        /// <returns></returns>
        public QueryResult<PartyAgreementTypeDetail> ExecuteKeyRequest(QueryRequest objMessageRequest)
        {
            Dictionary<string, int> dctError = new Dictionary<string, int>();
            PartyAgreementTypeDetail objPartyAgreementTypeRequest = new PartyAgreementTypeDetail();
            dctError = objPartyAgreementTypeRequest.ExtractData(objMessageRequest.QueryParameters, BasicValidatorApiConstants.RequestMethod.R.ToString());
            PartyAgreementTypeDetail objPartyAgreementTypeDetail = new PartyAgreementTypeDetail();
            QueryResult<PartyAgreementTypeDetail> objPartyAgreementTypeResponse;
            QueryResult<PartyAgreementTypeDetail> objResponseMessage = new QueryResult<PartyAgreementTypeDetail>(objMessageRequest.MessageId, objPartyAgreementTypeDetail);
            try
            {
                if (dctError != null && dctError.Count == 0)
                {
                    IList lstPartyAgreementTypeData = StaticDataDao.GetPartyAgreementType(Convert.ToInt32(objPartyAgreementTypeRequest.PartyAgreementTypeId));

                    if (lstPartyAgreementTypeData != null && lstPartyAgreementTypeData.Count > 0)
                    {
                        List<PartyAgreementTypeDetail> lstPartyAgreementType = MapPartyAgreementType(lstPartyAgreementTypeData.OfType<PartyAgreementType>().ToList());
                        objPartyAgreementTypeDetail = lstPartyAgreementType != null ? lstPartyAgreementType.FirstOrDefault() : null;
                        objResponseMessage.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Pass;
                    }
                    else
                    {
                        dctError.TryAdd(nameof(objPartyAgreementTypeRequest.PartyAgreementTypeId), 10026);
                        objResponseMessage.Messages = ConvertDictionaryToString(dctError);
                        objResponseMessage.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                    }
                }
                else
                {
                    objResponseMessage.Messages = ConvertDictionaryToString(dctError);
                    objResponseMessage.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Fail;
                }
                objPartyAgreementTypeResponse = new QueryResult<PartyAgreementTypeDetail>(objMessageRequest.MessageId, objPartyAgreementTypeDetail);
                objPartyAgreementTypeResponse.MessageId = objMessageRequest.MessageId;
                objPartyAgreementTypeResponse.ExecutionStatus = objResponseMessage.ExecutionStatus;
                objPartyAgreementTypeResponse.Messages = objResponseMessage.Messages;
                return objPartyAgreementTypeResponse;
            }
            catch (Exception ex)
            {
                LogException(ex);
                dctError.TryAdd(StaticDataFields.PartyAgreementType.ToString(), 20800);
                objPartyAgreementTypeResponse = new QueryResult<PartyAgreementTypeDetail>();
                objPartyAgreementTypeResponse.MessageId = objMessageRequest.MessageId;
                objPartyAgreementTypeResponse.Messages = ConvertDictionaryToString(dctError);
                objPartyAgreementTypeResponse.ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Error;
                return objPartyAgreementTypeResponse;
            }
        }

        #endregion
        /// <summary>
        /// This method will do all the business level validations for PartyAgreementType Service
        /// </summary>
        /// <param name="objPartyAgreementTypeDetail"></param>
        /// <param name="objApplicationData"></param>
        /// <param name="methodType"></param>
        /// <param name="dctError"></param>
        /// <param name="objPartyAgreementType"></param>
        private void ValidatePartyAgreementType(PartyAgreementTypeDetail objPartyAgreementTypeDetail, IApplicationData objApplicationData, string methodType,
                                                            Dictionary<string, int> dctError, PartyAgreementType objPartyAgreementType = null)
        {
            Hashtable htListAndValueCode = new Hashtable();
            Babelsystem.Business.Service.DTO.Base.EventExecutionStatus objEventExecutionStatus = new Babelsystem.Business.Service.DTO.Base.EventExecutionStatus();
            try
            {
                htListAndValueCode.Add(GetConfiguredData(nameof(ListCode), ListCode.AgreementTypeLevel.ToString()), string.Empty);
                htListAndValueCode.Add(GetConfiguredData(nameof(ListCode), ListCode.AgreementReferenceType.ToString()), string.Empty);
                htListAndValueCode.Add(GetConfiguredData(nameof(ListCode), ListCode.ServicePartyBusinessFunction.ToString()), string.Empty);
                htListAndValueCode.Add(GetConfiguredData(nameof(ListCode), ListCode.BusinessFunction.ToString()), string.Empty);

                IList<AppValueList> lstAppValuesList = Util.GetApplicationValueList(htListAndValueCode, Convert.ToInt32(objPartyAgreementTypeDetail.Firm) > 0 ? objPartyAgreementTypeDetail.Firm.ToString()
                                                                : objApplicationData.FirmPartyId);

                if (methodType == BasicValidatorApiConstants.RequestMethod.C.ToString())
                    ValidateCreateRequest(objPartyAgreementTypeDetail, dctError, objPartyAgreementType);

                if (methodType == BasicValidatorApiConstants.RequestMethod.U.ToString())
                    ValidateUpdateRequest(objPartyAgreementTypeDetail, dctError);

                if (methodType == BasicValidatorApiConstants.RequestMethod.C.ToString() || methodType == BasicValidatorApiConstants.RequestMethod.L.ToString())
                    ValidateCreateListRequest(objPartyAgreementTypeDetail, objApplicationData, dctError, lstAppValuesList);

                if (methodType == BasicValidatorApiConstants.RequestMethod.C.ToString() || methodType == BasicValidatorApiConstants.RequestMethod.U.ToString())
                    ValidateCreateUpdateRequest(objPartyAgreementTypeDetail, dctError, lstAppValuesList);

            }
            catch (Exception ex)
            {
                LogException(ex);
                throw;
            }
        }

        /// <summary>
        /// This method will validates the request for Create Service
        /// </summary>
        /// <param name="objPartyAgreementTypeDetail"></param>
        /// <param name="dctError"></param>
        /// <param name="objPartyAgreementType"></param>
        private void ValidateCreateRequest(PartyAgreementTypeDetail objPartyAgreementTypeDetail, Dictionary<string, int> dctError, PartyAgreementType objPartyAgreementType)
        {
            if (objPartyAgreementType != null && !string.IsNullOrWhiteSpace(objPartyAgreementType.PartyAgreementDocType) && objPartyAgreementType.Firm > 0 && !string.IsNullOrWhiteSpace(objPartyAgreementType.StandardFields.Status)
                        && objPartyAgreementType.Firm.ToString() == objPartyAgreementTypeDetail.Firm && objPartyAgreementType.StandardFields.Status == GetConfiguredData(nameof(DataBaseStatus), DataBaseStatus.Current.ToString()))
                dctError.TryAdd(nameof(objPartyAgreementTypeDetail.PartyAgreementType), 22314);
        }
        /// <summary>
        /// This method validates the request for Update Service
        /// </summary>
        /// <param name="objPartyAgreementTypeDetail"></param>
        /// <param name="dctError"></param>
        private void ValidateUpdateRequest(PartyAgreementTypeDetail objPartyAgreementTypeDetail, Dictionary<string, int> dctError)
        {
            if (objPartyAgreementTypeDetail.PartyAgreementTypeId > 0)
            {
                IList<PartyAgreementType> lstPartyAgreementType = StaticDataDao.GetPartyAgreementType(Convert.ToInt32(objPartyAgreementTypeDetail.PartyAgreementTypeId)).OfType<PartyAgreementType>().ToList();
                if (lstPartyAgreementType != null && lstPartyAgreementType.Count > 0)
                {
                    IList<PartyAgreementType> lstPartyAgreementTypeDetail = lstPartyAgreementType.Where(x => ((x.StandardFields.Version.ToString() == objPartyAgreementTypeDetail.RowVersion.ToString())
                                                    && (x.PartyAgreementTypeId.ToString() == objPartyAgreementTypeDetail.PartyAgreementTypeId.ToString()))).ToList();
                    if (lstPartyAgreementTypeDetail != null && lstPartyAgreementTypeDetail.Count == 0)
                        dctError.TryAdd(nameof(objPartyAgreementTypeDetail.PartyAgreementTypeId), 10202);


                }
                else
                    dctError.TryAdd(nameof(objPartyAgreementTypeDetail.PartyAgreementTypeId), 10110);

            }
        }
        /// <summary>
        /// This method will validates the request for create and update services
        /// </summary>
        /// <param name="objPartyAgreementTypeDetail"></param>
        /// <param name="dctError"></param>
        /// <param name="lstAppValuesList"></param>
        private void ValidateCreateUpdateRequest(PartyAgreementTypeDetail objPartyAgreementTypeDetail, Dictionary<string, int> dctError, IList<AppValueList> lstAppValuesList)
        {
            BasicValidator objBasicValidator = new BasicValidator();

            if (lstAppValuesList != null && lstAppValuesList.Count > 0)
            {
                IList<AppValueList> lstAppValues = new List<AppValueList>();

                if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeDetail.AgreementReference))
                {
                    lstAppValues = lstAppValuesList.Where(x => x.ListCode.Trim() == GetConfiguredData(nameof(ListCode), ListCode.AgreementReferenceType.ToString())
                    && x.ValueCode.Trim() == objPartyAgreementTypeDetail.AgreementReference.Trim()).ToList<AppValueList>();
                    if (lstAppValues != null && lstAppValues.Count == 0)
                        dctError.TryAdd(nameof(objPartyAgreementTypeDetail.AgreementReference), 10110);

                }
                if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeDetail.Issuer) && Convert.ToInt32(objPartyAgreementTypeDetail.Issuer) > 0)
                {
                    lstAppValues = lstAppValuesList.Where(x => x.ListCode.Trim() == GetConfiguredData(nameof(ListCode), ListCode.ServicePartyBusinessFunction.ToString())).ToList<AppValueList>();
                    if (lstAppValues != null && lstAppValues.Count > 0)
                    {
                        ArrayList lstAppValuesDetail = new ArrayList(lstAppValues.Count);
                        lstAppValuesDetail.AddRange(lstAppValues.OfType<AppValueList>().ToList().Select(x => x.ValueCode).ToArray());
                        IList lstOfParties = Util.GetOrganisationsList(objPartyAgreementTypeDetail.Issuer.ToString(), lstAppValuesDetail);
                        if (lstOfParties == null || lstOfParties.Count == 0)
                            dctError.TryAdd(nameof(objPartyAgreementTypeDetail.Issuer), 10082);
                    }
                    else
                        dctError.TryAdd(nameof(objPartyAgreementTypeDetail.Issuer), 10082);
                }
            }
            if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeDetail.AgreementStatus))
            {
                List<StatusReason> lstListStatusReason = new List<StatusReason>();
                Util.ValidateListCode(objPartyAgreementTypeDetail.Firm.ToString(), objPartyAgreementTypeDetail.AgreementStatus, lstListStatusReason);
                if (lstListStatusReason != null && lstListStatusReason.Count > 0)
                    dctError.TryAdd(nameof(objPartyAgreementTypeDetail.AgreementStatus), 10110);

            }

            //REQUIREDFLAG
            if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeDetail.RequiredFlag) && !objBasicValidator.IsValidFlag(objPartyAgreementTypeDetail.RequiredFlag))
                dctError.TryAdd(nameof(objPartyAgreementTypeDetail.RequiredFlag), 10110);
        }
        /// <summary>
        /// This method will validates request for both create and list services
        /// </summary>
        /// <param name="objPartyAgreementTypeDetail"></param>
        /// <param name="objApplicationData"></param>
        /// <param name="dctError"></param>
        /// <param name="lstAppValuesList"></param>
        private void ValidateCreateListRequest(PartyAgreementTypeDetail objPartyAgreementTypeDetail, IApplicationData objApplicationData, Dictionary<string, int> dctError, IList<AppValueList> lstAppValuesList)
        {
            IList<AppValueList> lstAppValues = new List<AppValueList>();
            BasicValidator objBasicValidator = new BasicValidator();

            //FIRM
            if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeDetail.Firm))
            {
                if (!objBasicValidator.IsValidFirm(objPartyAgreementTypeDetail.Firm.ToString(), objApplicationData))
                    dctError.TryAdd(nameof(objPartyAgreementTypeDetail.Firm), 10119);

            }
            //PARTYAGREEMENTTYPE
            if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeDetail.PartyAgreementType) && !objBasicValidator.IsValidAlphanumeric(objPartyAgreementTypeDetail.PartyAgreementType))
                dctError.TryAdd(nameof(objPartyAgreementTypeDetail.PartyAgreementType), 20642);

            if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeDetail.AgreementLevel))
            {
                lstAppValues = lstAppValuesList.Where(x => x.ListCode.Trim() == GetConfiguredData(nameof(ListCode), ListCode.AgreementTypeLevel.ToString())
                && x.ValueCode.Trim() == objPartyAgreementTypeDetail.AgreementLevel.Trim()).ToList<AppValueList>();
                if (lstAppValues != null && lstAppValues.Count == 0)
                    dctError.TryAdd(nameof(objPartyAgreementTypeDetail.AgreementLevel), 10110);

            }
            if (!string.IsNullOrWhiteSpace(objPartyAgreementTypeDetail.EntityType))
            {
                if (objPartyAgreementTypeDetail.AgreementLevel == GetConfiguredData(nameof(ApplicationLogLevel), ApplicationLogLevel.Party.ToString()))
                {
                    lstAppValues = lstAppValuesList.Where(x => x.ListCode.Trim() == GetConfiguredData(nameof(ListCode), ListCode.BusinessFunction.ToString())
                && x.ValueCode.Trim() == objPartyAgreementTypeDetail.EntityType.Trim()).ToList<AppValueList>();
                    if (lstAppValues != null && lstAppValues.Count == 0)
                        dctError.TryAdd(nameof(objPartyAgreementTypeDetail.EntityType), 10217);

                }
                else if (objPartyAgreementTypeDetail.AgreementLevel == GetConfiguredData(nameof(ApplicationLogLevel), ApplicationLogLevel.Portfolio.ToString()))
                {
                    Hashtable htCriteria = new Hashtable();
                    htCriteria.Add(GetConfiguredData(nameof(FieldName), FieldName.PortfolioType.ToString()), objPartyAgreementTypeDetail.EntityType);
                    htCriteria.Add(GetConfiguredData(nameof(FieldName), FieldName.Status.ToString()), GetConfiguredData(nameof(DataBaseStatus), DataBaseStatus.Current.ToString()));
                    int count = Util.GetRecordCount(GetConfiguredData(nameof(FieldName), FieldName.PortfolioType.ToString()), htCriteria);
                    if (count == 0)
                        dctError.TryAdd(nameof(objPartyAgreementTypeDetail.EntityType), 10216);

                }
                else if (objPartyAgreementTypeDetail.AgreementLevel == GetConfiguredData(nameof(ApplicationLogLevel), ApplicationLogLevel.PortfolioWrapper.ToString()))
                {
                    Hashtable htCriteria = new Hashtable();
                    htCriteria.Add(GetConfiguredData(nameof(FieldName), FieldName.WrapperType.ToString()), objPartyAgreementTypeDetail.EntityType);
                    htCriteria.Add(GetConfiguredData(nameof(FieldName), FieldName.Status.ToString()), GetConfiguredData(nameof(DataBaseStatus), DataBaseStatus.Current.ToString()));
                    int count = Util.GetRecordCount(GetConfiguredData(nameof(Tables), Tables.WrapperType.ToString()), htCriteria);
                    if (count == 0)
                        dctError.TryAdd(nameof(objPartyAgreementTypeDetail.EntityType), 10215);

                }
            }
        }


    }
}
