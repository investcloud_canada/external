﻿using IC.Green.Business.DTO;
using IC.Green.Business.DTO.Common;

namespace IC.Green.StaticData.BL
{
    public interface IPartyAgreementTypeLogic
    {
        QueryResult<PartyAgreementTypeDetail> ExecuteKeyRequest(QueryRequest objMessageRequest);
        QueryResult<PartyAgreementTypeDetail> ExecuteListRequest(QueryRequest objMessageRequest);
        Result ExecuteCreateRequest(CommandRequest objRequest);
        Result ExecuteUpdateRequest(CommandRequest objRequest);
        Result ExecuteDeleteRequest(CommandRequest objRequest);
    }
}
