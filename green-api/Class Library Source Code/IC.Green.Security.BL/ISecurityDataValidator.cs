﻿using IC.Green.Business.DTO.Security;

namespace IC.Green.DataValidation.BL
{
    public interface ISecurityDataValidator
    {
        SecurityResponse ExecuteSingleSignOnRequest(SecurityRequest objRequest);
    }
}