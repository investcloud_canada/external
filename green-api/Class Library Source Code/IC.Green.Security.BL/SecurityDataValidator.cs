﻿using System;
using System.Collections;
using System.Collections.Generic;
using Babelsystem.Base.BL;
using Babelsystem.Business.DTO.UserAccessSecurity;
using IC.Green.Business.DTO.Security;
using IC.Green.DataValidation.BL;

namespace IC.Green.DataValidation.BL
{
    public class SecurityDataValidator : BaseClass, ISecurityDataValidator
    {
        #region Interface

        public DataValidation.DAL.IDataValidatorDao DataValidatorDao { get; set; }

        #endregion Interface


        /// <summary>
        /// Verify Green Session,
        /// create new session if session not exist,
        /// Activate if the session is expired.
        /// </summary>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        public SecurityResponse ExecuteSingleSignOnRequest(SecurityRequest objRequest)
        {
            SecurityResponse objSecurity = null;
            if (objRequest != null && !string.IsNullOrWhiteSpace(objRequest.ExternalSecurityKey) && !string.IsNullOrWhiteSpace(objRequest.UserId))
            {
                if (!IsValidSession(objRequest, out objSecurity))
                {
                    objSecurity = CreateUserLog(objRequest);
                }
            }
            if (objSecurity == null)
                objSecurity = new SecurityResponse(string.Empty, string.Empty, objRequest.MessageId);

            return objSecurity;

        }

        /// <summary>
        /// Validate is the session, activate if it is expired.
        /// </summary>
        /// <param name="objInput"></param>
        /// <param name="objSecurity"></param>
        /// <returns></returns>
        private bool IsValidSession(SecurityRequest objInput, out SecurityResponse objSecurity)
        {
            bool isValidSession = false;
            objSecurity = null;
            IList lstUserLog = DataValidatorDao.IsUserSessionExists(null, objInput.ExternalSecurityKey, objInput.UserId);
            if (lstUserLog != null && lstUserLog.Count > 0)
            {
                isValidSession = true;
                AppUserLog objLog = lstUserLog[0] as AppUserLog;
                if (objLog.Status != Enumerator.StatusValue.CURR.ToString())
                {
                    if (DataValidatorDao.ActivateUserLog(objLog.SessionId.Trim()))
                    {
                        objSecurity = new SecurityResponse(objInput.ExternalSecurityKey, objLog.SessionId, objInput.MessageId);
                    }
                }
                else
                {
                    objSecurity = new SecurityResponse(objInput.ExternalSecurityKey, objLog.SessionId, objInput.MessageId);
                }
            }
            return isValidSession;
        }

        /// <summary>
        /// Create App User Log entry.
        /// </summary>
        /// <param name="objInput"></param>
        /// <returns></returns>
        private SecurityResponse CreateUserLog(SecurityRequest objInput)
        {
            Random random = new Random();
            AppUserLog objAppUserLog = new AppUserLog();
            objAppUserLog.SessionId = Guid.NewGuid() + objInput.UserId.Trim() + random.Next(1, 10000);
            objAppUserLog.UserId = objInput.UserId.Trim();
            objAppUserLog.StartDate = DateTime.Now;
            objAppUserLog.LastActivityDate = DateTime.Now;
            objAppUserLog.Status = Enumerator.StatusValue.CURR.ToString();
            objAppUserLog.EndDate = DateTime.Now.AddMinutes(ApplicationData.SessionTimeOut);
            objAppUserLog.LastUpdatedFrom = ApplicationData.LastUpdatedFrom;
            objAppUserLog.BrowserVersion = ApplicationData.BrowserVersion;
            objAppUserLog.ExternalFlag = GetConfiguredData(typeof(BooleanValues).Name, BooleanValues.Yes.ToString());
            objAppUserLog.ExternalSecurityKey = objInput.ExternalSecurityKey;
            DataValidatorDao.Save(objAppUserLog);
            var objSecurity = new SecurityResponse(objInput.ExternalSecurityKey, objAppUserLog.SessionId, objInput.MessageId);
            return objSecurity;
        }
    }
}
