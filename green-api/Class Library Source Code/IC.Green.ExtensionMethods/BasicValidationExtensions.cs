﻿using System;
using System.Text.RegularExpressions;

namespace IC.Green.ExtensionMethods
{
    public static class BasicValidationExtensions
    {
        /// <summary>
        /// This method will find weather the string is populated or not
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsStringPopulated(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;
            else
                return true;
        }
        /// <summary>
        /// This method will find weather the length is valid or not
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static bool IsValidLength(this string value, int maxLength)
        {
            if(value?.Length > maxLength)
                return false;
            else
                return true;
        }
        /// <summary>
        /// This method will find weather the string is valid or not
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsValidString(this string value)
        {
            if (!IsValidAlphanumeric(value,true))
                return false;

            return true;
        }
        /// <summary>
        /// This method will find weather the string is valid
        /// </summary>
        /// <param name="character"></param>
        /// <param name="isAllowSpecialCharacter"></param>
        /// <returns></returns>
        private static bool IsValidAlphanumeric(string character, bool isAllowSpecialCharacter)
        {
            try
            {
                if (isAllowSpecialCharacter)
                    return Regex.IsMatch(character, "^[0-9a-zA-Z\\w-*\\s\\d\\.\\+\\-\\*\\@\\$\\%\\:/]*$", RegexOptions.IgnoreCase);
                else
                    return Regex.IsMatch(character, "^[a-zA-Z0-9_.-]*$", RegexOptions.IgnoreCase);
            }
            catch (Exception ex)
            {
                return false;

            }
        }
    }
}
