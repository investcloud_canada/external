﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using IC.Green.Business.DTO.Common;
using IC.Green.Business.DTO.Constants;
using Newtonsoft.Json.Linq;
using Spring.Context;
using Spring.Context.Support;

namespace IC.Green.Domain.Interface
{
    public class DomainInterface : IDomainInterface
    {
        #region Variables

        private Dictionary<string, string> _dctExecutionInstruction;
        private IApplicationContext _context;

        #endregion Variables

        /// <summary>
        /// Execute the query request by collecting the method details from the ExecutionDetailMapping file.
        /// </summary>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        public ExecutionResult ProcessQueryRequest(QueryRequest objRequest)
        {
            ExecutionResult objExecutionResult = null;
            try
            {
                MethodInfo mi = GetMethodInfo(objRequest.ServiceName, objRequest.Operation, out var objDependencyObject);
                if (mi is not null)
                {
                    var result = mi.Invoke(objDependencyObject, new Object[] { objRequest });
                    objExecutionResult = result as ExecutionResult;
                }

                return objExecutionResult;
            }
            catch (Exception e)
            {
                objExecutionResult = new ExecutionResult
                {
                    Cause = e.Message,
                    ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Error
                };
                return objExecutionResult;
            }
        }

        /// <summary>
        /// Execute the command request by collecting the method details from the ExecutionDetailMapping file.
        /// </summary>
        /// <param name="objRequest"></param>
        /// <returns></returns>
        public ExecutionResult ProcessCommandRequest(CommandRequest objRequest)
        {
            ExecutionResult objExecutionResult = null;
            try
            {
                MethodInfo mi = GetMethodInfo(objRequest.ServiceName, objRequest.Operation, out var objDependencyObject);
                if (mi != null && objDependencyObject != null)
                {
                    var result = mi.Invoke(objDependencyObject, new Object[] { objRequest });
                    objExecutionResult = result as ExecutionResult;
                }

                return objExecutionResult;
            }
            catch (Exception e)
            {
                objExecutionResult = new ExecutionResult
                {
                    Cause = e.Message, ExecutionStatus = DomainApiEnumerator.DomainExecutionStatus.Error
                };
                return objExecutionResult;
            }
        }

        /// <summary>
        /// Get the public method for the given service and operation
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="operation"></param>
        /// <param name="objDependencyObject"></param>
        /// <returns></returns>
        private MethodInfo GetMethodInfo(string serviceName, string operation, out object objDependencyObject)
        {
            objDependencyObject = null;
            string dependencyObjectName = GetDependencyObjectName(serviceName);
            if (dependencyObjectName != null)
            {
                objDependencyObject = GetDependencyObject(dependencyObjectName);
                Type type = objDependencyObject.GetType();
                return type.GetMethod(GetMethodName(operation));
            }

            return null;
        }

        /// <summary>
        /// Collect the name of the dependency object for the given service.
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        private string GetDependencyObjectName(string serviceName)
        {
            if (_dctExecutionInstruction == null)
            {
                using StreamReader fileStream = new StreamReader(Path.Combine(
                    Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty,
                    "ExecutionDetailMapping.json"));
                JObject jsonObj = JObject.Parse(fileStream.ReadToEnd());
                _dctExecutionInstruction = (from item in jsonObj["ExecutionDetail"]
                    select new KeyValuePair<string, string>(item["ServiceName"].ToString(),
                        item["DependencyObjectName"].ToString())).ToDictionary(x => x.Key, x => x.Value);
            }
            return _dctExecutionInstruction[serviceName];

        }

        private object GetDependencyObject(string dependencyObjectName)
        {
            _context ??= ContextRegistry.GetContext();
            return _context[dependencyObjectName];
        }

        private string GetMethodName(string operationName)
        {
            return string.Concat("Execute", operationName, "Request");
        }
    }
}
