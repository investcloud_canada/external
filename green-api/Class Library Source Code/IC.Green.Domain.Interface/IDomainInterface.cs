﻿using IC.Green.Business.DTO.Common;


namespace IC.Green.Domain.Interface
{
    public interface IDomainInterface
    {
        public ExecutionResult ProcessQueryRequest(QueryRequest objRequest);
        public ExecutionResult ProcessCommandRequest(CommandRequest objRequest);
    }
}
