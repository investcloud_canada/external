﻿using System;
using System.Collections.Generic;
using IC.Green.Business.DTO;
using IC.Green.Business.DTO.Common;


namespace IC.Green.Domain.Interface
{
    public class PortfolioWrapperDomain 
    {

        public QueryResult<PortfolioWrapperDetail> ExecuteKeyRequest(QueryRequest objMessageRequest)
        {
            QueryResult<PortfolioWrapperDetail> objResponseMessage =
                new QueryResult<PortfolioWrapperDetail>(objMessageRequest.MessageId,
                    new PortfolioWrapperDetail
                    {
                        PortfolioId = "11095",
                        PortfolioWrapperId = "11095_WW_FIRM_1",
                        PortfolioWrapperName = "WWMGT Dealing Errors"
                    });
            
            return objResponseMessage;
        }

        public QueryResult<PortfolioWrapperDetail> ExecuteListRequest(QueryRequest objMessageRequest)
        {
            QueryResult<PortfolioWrapperDetail> objResponseMessage =
                new QueryResult<PortfolioWrapperDetail>(objMessageRequest.MessageId,
                     new List<PortfolioWrapperDetail>
            {
                new PortfolioWrapperDetail
                {
                    PortfolioId = "11095",
                    PortfolioWrapperId = "11095_WW_FIRM_1",
                    PortfolioWrapperName = "WWMGT Dealing Errors"
                },
                new PortfolioWrapperDetail
                {
                    PortfolioId = "11294",
                    PortfolioWrapperId = "11294_WW_RB_1",
                    PortfolioWrapperName = "4625-Alabama-West Florida United Methodist Foundation-Equity"
                }
            });

            return objResponseMessage;
        }

        public Result ExecuteCreateRequest(CommandRequest objRequest)
        {
            Result objResult = new Result { MessageId = objRequest.MessageId, Messages = new String[] { "Record created successfully" } };
            return objResult;
        }
    }
}
